using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    private float timeLeft;
    public ParticleSystem system;
    private void Awake()
    {
        var main = system.main;
        timeLeft = main.startLifetimeMultiplier + main.duration;
    }
    public void ActivateExplosion()
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
        StartCoroutine(DeactivateExplosion());
    }

    IEnumerator DeactivateExplosion()
    {
        yield return new WaitForSeconds(timeLeft/2);
        GridManager.instance.active_bomb--;
        yield return new WaitForSeconds(timeLeft/2);
        GridManager.instance.bomb_pool.Add(this);
        gameObject.SetActive(false);
    }
}
