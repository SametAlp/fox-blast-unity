using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameAnalyticsSDK;

public class Boost : MonoBehaviour
{
    public int boost_id;
    public int count = 0;
    public TextMeshProUGUI count_text;
    public GameObject boost_go;
    public GameObject plus_go;
    public GameObject counter_go;
    public GameObject locked_go;
    public GameObject activated_go;

    private void Start()
    {
        if(!CheckLevel())
        {
            return;
        }
        CheckValue();
    }

    bool CheckLevel()
    {
        int level = GridManager.instance.level;
        if (level != 0 && level < 10 && boost_id == 0)
        {
            DisableBoost();
            return false;
        }
        if (level != 0 && level < 13 && boost_id == 1)
        {
            DisableBoost();
            return false;
        }
        if (level != 0 && level < 17 && boost_id == 2)
        {
            DisableBoost();
            return false;
        }
        if (level != 0 && level < 20 && boost_id == 3)
        {
            DisableBoost();
            return false;
        }
        return true;
    }

    void DisableBoost()
    {
        boost_go.SetActive(false);
        locked_go.SetActive(true);
        plus_go.SetActive(false);
        counter_go.SetActive(false);
    }

    public void CheckValue()
    {
        if (!CheckLevel())
        {
            return;
        }
        count = PlayerPrefs.GetInt("boost_count_" + boost_id.ToString(), 3);
        if (count <= 0)
        {
            count = 0;
            plus_go.SetActive(true);
            counter_go.SetActive(false);
        }
        else
        {
            plus_go.SetActive(false);
            counter_go.SetActive(true);
        }
        count_text.text = count.ToString();
    }

    public void StartBoost(int index)
    {
        if(count > 0)
        {
        }
        else
        {
            PopUps.instance.open_boost_panel(index);
        }
        if(GameManager.instance.isBoostActive)
        {
            activated_go.SetActive(false);
            GameManager.instance.DeActivateBoost();
        }
        else
        {
            activated_go.SetActive(true);
            GameManager.instance.ActivateBoost();
            if(boost_id == 0)
            {
                SoundManager.PlaySFX("Sling_UI");
            }
            else if (boost_id == 1)
            {
                SoundManager.PlaySFX("Lawnmower_UI");
            }
            else if (boost_id == 2)
            {
                SoundManager.PlaySFX("Leafblower_UI");
            }
            else if (boost_id == 3)
            {
                SoundManager.PlaySFX("Weathercock_UI");
            }

        }
        GameManager.instance.isBoostActive = !GameManager.instance.isBoostActive;
        GameManager.instance.boostIndex = index;
    }

    public void Used()
    {
        GameAnalytics.NewDesignEvent("Boost:" + boost_id);
        count--;
        PlayerPrefs.SetInt("boost_count_" + boost_id.ToString(), PlayerPrefs.GetInt("boost_count_" + boost_id.ToString(), 3) - 1);
        CheckValue();
    }
}
