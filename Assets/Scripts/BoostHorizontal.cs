using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoostHorizontal : MonoBehaviour
{
    bool isJobDone = false;

    private void Update()
    {
        if (isJobDone)
        {
            return;
        }
        Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
        if (viewPos.x <= 1f && viewPos.y >= -0f && viewPos.y <= 1f && viewPos.z > -0f)
        {
            Tile temp_tile = GameManager.instance.TileCloseToPoint(transform.position, false);
            if (temp_tile == null || !temp_tile.gameObject.activeSelf)
            {
                return;
            }

            if (temp_tile.boostPassed)
            {
                return;
            }

            if (temp_tile.GetTilesOfLargeObstacle() != null)
            {
                List<Tile> tiles = temp_tile.GetTilesOfLargeObstacle();
                for (int i = 0; i < tiles.Count; i++)
                {
                    tiles[i].boostPassed = true;
                }
            }
            else
            {
                temp_tile.boostPassed = true;
            }

            if (temp_tile.tile_type == Tile.TileType.COMBO)
            {
                GameManager.instance.HandleCombos(temp_tile);
                GameManager.instance.DisableTile(temp_tile, rocket: this.gameObject);
            }
            else
            {
                GameManager.instance.DisableTile(temp_tile, rocket: this.gameObject);
            }
        }
        else
        {
            isJobDone = true;
            GameManager.instance.active_rockets--;

            transform.DOBlendableLocalMoveBy(Vector3.zero, 1f).OnComplete(() =>
            {
                this.gameObject.SetActive(false);
            });
        }
    }

    public void Move(Vector3 position, Direction direction)
    {
        isJobDone = false;
        GameManager.instance.active_rockets++;
        transform.position = position;
        Vector3 move_direction;

        float amount = GameManager.instance.screenBounds.x - transform.position.x + 2;
        if (direction == Direction.UP)
        {
            move_direction = new Vector3(0, amount, 0);
        }
        else if (direction == Direction.DOWN)
        {
            move_direction = new Vector3(0, -amount, 0);
        }
        else if (direction == Direction.LEFT)
        {
            move_direction = new Vector3(-amount, 0, 0);
        }
        else
        {
            move_direction = new Vector3(amount, 0, 0);
        }

        transform.DOBlendableMoveBy(move_direction, 2f);
    }
}
