﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public static class ExtensionMethods
{
    public static int CeilOff(this int i)
    {
        return ((int)Math.Ceiling(i / 10.0)) * 10;
    }

    public static int FloorOff(this int i)
    {
        return ((int)Math.Floor(i / 10.0)) * 10;
    }

    public static int Threshold(this int i)
    {
        return i >= 20 ? 20 : i;
    }
}

public class ChestScript : MonoBehaviour
{
    public static ChestScript instance;
    public TextMeshProUGUI level_reach_text;
    public TextMeshProUGUI total_star_text;

    public GameObject left_chest_open_go;

    public GameObject right_chest_open_go;
    public Image bar_image;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        Intialize();
    }

    void Intialize()
    {
        if (Menu.instance.level < PlayerPrefs.GetInt("claimed_chest_max_level", 1) || Menu.instance.level < 10)
        {
            level_reach_text.text = "LEVEL " + (Menu.instance.level + 1).CeilOff().ToString();//LocalizationManager.GetLocalizedText("level", 1) + " " + (Menu.instance.level + 1).CeilOff().ToString();
        }
        else
        {
            level_reach_text.text = "LEVEL " + Menu.instance.level.CeilOff().ToString();// LocalizationManager.GetLocalizedText("level", 1) + " " + Menu.instance.level.CeilOff().ToString();
            enable_zombie_chest();
        }
        int total_star = PlayerPrefs.GetInt("total_star", 0);
        total_star_text.text = total_star.Threshold().ToString() + "/20";
        bar_image.DOFillAmount(((float)total_star) / 20f, 0.2f).OnComplete(() => {
            if (total_star >= 20)
            {
                enable_star_chest();
            }
        });
    }

    void enable_zombie_chest()
    {
        left_chest_open_go.SetActive(true);
    }

    public void disable_zombie_chest()
    {
        PlayerPrefs.SetInt("claimed_chest_max_level", (Menu.instance.level + 1).CeilOff());
        left_chest_open_go.SetActive(false);
        level_reach_text.text = "LEVEL " + (Menu.instance.level + 1).CeilOff().ToString(); // LocalizationManager.GetLocalizedText("level", 1) + " " + (Menu.instance.level + 1).CeilOff().ToString();
        Intialize();
    }

    void enable_star_chest()
    {
        right_chest_open_go.SetActive(true);
    }

    public void disable_star_chest()
    {
        right_chest_open_go.SetActive(false);
        PlayerPrefs.SetInt("total_star", PlayerPrefs.GetInt("total_star", 0) - 20);
        Intialize();
    }

    public GameObject ChestRewardPanel;
    public GameObject claim_button;
    public Transform chest_transform;
    public List<Sprite> rewards_sprites;
    List<string> rewards = new List<string> { "combo_start_count_0", "combo_start_count_1", "combo_start_count_2", "boost_count_0", "boost_count_1", "boost_count_2", "boost_count_3", "coins" };
    public List<string> rewards_text = new List<string> { "combo_start_count_0", "combo_start_count_1", "combo_start_count_2", "boost_count_0", "boost_count_1", "boost_count_2", "boost_count_3", "coins" };
    List<int> coin_values = new List<int> { 20, 20, 20, 20, 20, 100, 250, 100, 50, 50, 25, 50, 50, 50, 100 };
    List<int> used_indexes = new List<int>();

    public List<RewardItem> RewardItems;

    public void open_chest_reward_panel()
    {
        ChestRewardPanel.SetActive(true);
        claim_button.transform.localScale = Vector3.zero;
        enable_rewards();
    }

    public void close_chest_reward_panel()
    {
        ChestRewardPanel.SetActive(false);
    }

    public void enable_rewards()
    {
        used_indexes.Clear();

        for (int i = 0; i < RewardItems.Count; i++)
        {
            transform.DOBlendableLocalMoveBy(Vector3.zero, i * 0.5f).OnComplete(() => SoundManager.PlaySFX("RewardUnlock"));
            set_a_reward(i);
            RewardItems[i].transform.localScale = Vector3.zero;
            RewardItems[i].transform.DOScale(1, 0.3f).SetEase(Ease.OutBack).SetDelay(i * 0.5f);
            RewardItems[i].Go(chest_transform, i * 0.5f);
        }
        transform.DOBlendableLocalMoveBy(Vector3.zero, 1.5f).OnComplete(() =>
        { claim_button.transform.DOScale(1, 0.5f); });
    }

    void set_a_reward(int i)
    {
        int index = UnityEngine.Random.Range(0, rewards.Count);

        if (used_indexes.Count == rewards.Count)
        {
            used_indexes.Clear();
        }
        while (used_indexes.Contains(index))
        {
            index = UnityEngine.Random.Range(0, rewards.Count);
        }
        used_indexes.Add(index);


        if (rewards[index] == "coins")
        {
            RewardItems[i].set_reward(rewards[index], rewards_text[index], coin_values[UnityEngine.Random.Range(0, coin_values.Count)], rewards_sprites[index], 300);
        }
        else if (rewards[index][0] == 'b')
        {
            RewardItems[i].set_reward(rewards[index], rewards_text[index], 1, rewards_sprites[index], 3);
        }
        else
        {
            RewardItems[i].set_reward(rewards[index], rewards_text[index], 1, rewards_sprites[index], 3);
        }
    }
}



