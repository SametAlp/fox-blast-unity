using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class CoinPopUps : MonoBehaviour
{
    public static CoinPopUps instance;

    private const float DISAPPEAR_TIMER_MAX = 1f;
    public GameObject coin_go;
    public Transform goto_transform;
    public Transform start_transform;
    Vector3 goto_transform_position;
    public float scale_value = 1;
    public List<GameObject> gos;
    public bool isRunning;
    public float speed;
    CoinPopUps coinGemPopUps;


    private void Awake()
    {
        instance = this;
        speed = 1.1f;
        coinGemPopUps = GetComponent<CoinPopUps>();
    }
    public CoinPopUps Create(Vector3 position, Transform _goto_transform = null, float _speed = 1.1f, int count = 1)
    {
        float rand = UnityEngine.Random.Range(0.1f, 0.3f);
        float rand_2 = UnityEngine.Random.Range(-0.3f, 0f);
        float rand_3 = UnityEngine.Random.Range(-0.3f, 0.3f);
        speed = _speed;
        if (start_transform != null)
        {
            position = start_transform.position;
        }
        if (_goto_transform == null)
        {
            if (Menu.instance != null)
                goto_transform_position = Menu.instance.gold_transform.position;
        }
        else
        {
            goto_transform_position = goto_transform.position;
        }

        transform.position = new Vector3(position.x, position.y, transform.position.z) + new Vector3(rand + rand_3, rand_2 + rand_3, 0);
        for (int j = 0; j < transform.childCount; j++)
        {
            transform.GetChild(j).DOKill();
            transform.GetChild(j).localPosition = Vector3.zero;
        }
        coinGemPopUps.isRunning = true;
        coinGemPopUps.Setup(count);
        return coinGemPopUps;
    }


    public void Setup(int count)
    {
        bool gos_null = (count > gos.Count);
        if (gos == null)
        {
            gos = new List<GameObject>();
        }
        for (int i = 0; i < count; i++)
        {
            GameObject go;
            if (gos_null)
            {
                go = GameObject.Instantiate(coin_go, this.transform);
                gos.Add(go);
            }
            else
            {
                go = gos[i];
            }
            Transform trans = go.transform;
            trans.SetAsFirstSibling();
            go.SetActive(true);
            trans.localScale = Vector3.zero;
            trans.DOScale(scale_value, 0.1f).SetDelay(((i + 5) % 20) * 0.09f);
            //trans.DOScale(0f, 6.5f).SetDelay((count - i) * 0.9f);

            float rand_offset = UnityEngine.Random.Range(0.2f, 1);
            float rand_offset_2 = UnityEngine.Random.Range(-1, -0.2f);
            float rand = UnityEngine.Random.Range(-0.5f, 0.5f) + rand_offset;
            float rand_2 = UnityEngine.Random.Range(-0.5f, 0.5f) + rand_offset_2;
            if (count > 1)
            {
                trans.DOScale(scale_value, 0.1f);
                trans.DOBlendableMoveBy(new Vector3(rand, rand_2, 0), speed * 2 / 3).SetDelay(i * 0.01f);
            }
            else
            {
                trans.DOScale(scale_value, 0.1f).SetDelay(((i + 5) % 20) * 0.09f);
            }
            trans.DOScale(Vector3.zero, speed).SetEase(Ease.InBack).SetDelay(((i + 5) % 20) * 0.09f);
            trans.DOMove(goto_transform_position, speed).SetEase(Ease.InBack).SetDelay(((i + 5) % 20) * 0.06f).OnComplete(() =>
            {
                SoundManager.PlaySFX("Coin_Collect");

                if (count == (i - 1))
                {
                    isRunning = false;
                    for (int j = 0; j < gos.Count; j++)
                    {
                        gos[j].SetActive(false);
                    }
                }
            }
            );
        }
    }
}