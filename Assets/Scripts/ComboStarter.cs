﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ComboStarter : MonoBehaviour
{
    public int index = 0;

    public GameObject count_go;
    public GameObject tick_go;
    public GameObject plus_go;
    public GameObject green_go;
    public TextMeshProUGUI count_text;
    public int count;

    private void Start()
    {
        initialize();
    }

    public void initialize(bool again = false)
    {
        if(!again)
            PlayerPrefs.SetInt("combo_start_" + index.ToString(), 0);

        count = PlayerPrefs.GetInt("combo_start_count_" + index.ToString(), 3);
        count_text.text = count.ToString();
        if(count == 0)
        {
            count_go.SetActive(false);
            tick_go.SetActive(false);
            plus_go.SetActive(true);
        }
        else if(PlayerPrefs.GetInt("combo_start_" + index.ToString(), 0) == 0)
        {
            count_go.SetActive(true);
            tick_go.SetActive(false);
            plus_go.SetActive(false);
        }

        if(PlayerPrefs.GetInt("combo_start_" + index.ToString(), 0) == 1 && !tick_go.activeSelf)
        {
            start_with_combos();
        }
    }

    public void buy_combo()
    {
        if (Menu.instance != null)
        {
            Menu.instance.open_combo_panel(index);
        }
        else
        {
            PopUps.instance.open_combo_panel(index);
        }
    }

    public void start_with_combos()
    {
        if (PlayerPrefs.GetInt("combo_start_" + index.ToString(), 0) == 0)
        {
            green_go.SetActive(false);
            plus_go.SetActive(false);
            tick_go.SetActive(true);
            count_go.SetActive(false);
            PlayerPrefs.SetInt("combo_start_" + index.ToString(), 1);
            this.transform.DOShakeScale(0.4f, 0.2f, 10, 1).OnComplete(() => this.transform.DOScale(1, 0.1f));
        }
        else
        {
            green_go.SetActive(true);
            tick_go.SetActive(false);
            count_go.SetActive(true);
            PlayerPrefs.SetInt("combo_start_" + index.ToString(), 0);
            this.transform.DOShakeScale(0.4f, 0.2f, 10, 1).OnComplete(() => this.transform.DOScale(1, 0.1f));
        }
    }
}
