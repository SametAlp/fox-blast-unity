﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class DailyReward : MonoBehaviour
{
    public static DailyReward instance;
    public GameObject daily_bonus_object;
    public Button remaining_time_go;
    public TextMeshProUGUI remaining_time_text;
    public GameObject claim_button;
    public GameObject tap_text_go;

    bool isReady = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if(isReady)
        {
            return;
        }
        update_time();
    }

    public void set_ready()
    {
        if (PlayerPrefs.GetInt("next_level_popup", 0) == 1)
        {
            return;
        }
        remaining_time_go.interactable = true;
        isReady = true;
        remaining_time_text.text = "READY!";
    }

    public void open_gift()
    {
        if (PlayerPrefs.GetInt("next_level_popup", 0) == 1)
        {
            return;
        }

        daily_bonus_object.SetActive(true);
        enable_rewards();
        save_date();
        isReady = false;
        remaining_time_go.interactable = false;

        //Notifications.instance.longtime_notif();

        Menu.instance.coins_to_k();
    }


    public void close_gift()
    {
        //Sounds.instance.dailyreward_sound_play();
        Menu.instance.coins_to_k();
        daily_bonus_object.SetActive(false);
    }

    public void save_date()
    {
        DateTime date = DateTime.Now;

        int k = 1;

        if (date.Day == PlayerPrefs.GetInt("day", 0))
        {
            if (PlayerPrefs.GetInt("gift", 0) == 7)
            {
                k = 7;
            }
            else
            {
                k = PlayerPrefs.GetInt("gift", 0);
                PlayerPrefs.SetInt("gift", k + 1);
            }

        }
        else
        {
            PlayerPrefs.SetInt("gift", 1);
        }

        date = date.AddDays(1);
        PlayerPrefs.SetInt("year", date.Year);
        PlayerPrefs.SetInt("month", date.Month);
        PlayerPrefs.SetInt("day", date.Day);
        PlayerPrefs.SetInt("hour", date.Hour);
        PlayerPrefs.SetInt("minute", date.Minute);
        PlayerPrefs.SetInt("millisecond", date.Millisecond);
    }

    void save_date_bug()
    {
        DateTime date = DateTime.Now;
        date = date.AddDays(1);
        PlayerPrefs.SetInt("year", date.Year);
        PlayerPrefs.SetInt("month", date.Month);
        PlayerPrefs.SetInt("day", date.Day);
        PlayerPrefs.SetInt("hour", date.Hour);
        PlayerPrefs.SetInt("minute", date.Minute);
        PlayerPrefs.SetInt("millisecond", date.Millisecond);
    }

    void update_time()
    {
        try
        {
            DateTime date = DateTime.MinValue;
            date = date.AddYears(PlayerPrefs.GetInt("year", DateTime.Now.Year) - 1);
            date = date.AddMonths(PlayerPrefs.GetInt("month", DateTime.Now.Month) - 1);
            date = date.AddDays(PlayerPrefs.GetInt("day", DateTime.Now.Day) - 1);
            date = date.AddHours(PlayerPrefs.GetInt("hour", DateTime.Now.Hour));
            date = date.AddMinutes(PlayerPrefs.GetInt("minute", DateTime.Now.Minute));
            date = date.AddMilliseconds(PlayerPrefs.GetInt("millisecond", DateTime.Now.Millisecond));

            int h = (int)(date - DateTime.Now).TotalHours;
            int m = (int)(date - DateTime.Now).TotalMinutes % 60;
            int s = (int)(date - DateTime.Now).TotalSeconds % 60;

            if (h <= 0 && m <= 0 && s <= 0)
            {
                set_ready();
                return;
            }

            DateTime full_date_interval = DateTime.Now;
            full_date_interval = full_date_interval.AddDays(1);
            int add_value = (int)(full_date_interval - DateTime.Now).TotalSeconds;
            add_value -= (int)(date - DateTime.Now).TotalSeconds;
            remaining_time_text.text = h.ToString() + ":" + m.ToString() + ":" + (s < 10 ? "0" + s.ToString() : s.ToString());
        }
        catch
        {
            save_date_bug();
        }
    }

    public List<Sprite> rewards_sprites;
    List<string> rewards = new List<string> { "combo_start_count_0", "combo_start_count_1", "combo_start_count_2", "boost_count_0", "boost_count_1", "boost_count_2", "boost_count_3", "coins" };
    public List<string> rewards_text;
    List<int> coin_values = new List<int> { 20, 20, 20, 20, 20, 100, 250, 100, 50, 50, 25, 50, 50, 50, 100 };
    List<int> used_indexes = new List<int>();

    public List<DailyRewardItem> dailyRewardItems;
    public void enable_rewards()
    {
        disable_claims();
        used_indexes.Clear();

        for (int i = 0; i < dailyRewardItems.Count; i++)
        {
            set_a_reward(i);
            dailyRewardItems[i].transform.localScale = Vector3.zero;
            dailyRewardItems[i].transform.DOScale(1, 0.3f).SetEase(Ease.OutBack).SetDelay(i * 0.05f);
        }
        set_interectable_rewards(true);
    }

    public void enable_claims()
    {
        claim_button.transform.localScale = Vector3.zero;
        claim_button.transform.DOScale(1, 0.3f).SetDelay(0.6f).SetEase(Ease.OutBack);
        tap_text_go.SetActive(false);
        claim_button.SetActive(true);

        for (int i = 0; i < dailyRewardItems.Count; i++)
        {
            dailyRewardItems[i].set_front(0.5f);
        }
    }

    public void disable_claims()
    {
        tap_text_go.SetActive(true);
        claim_button.SetActive(false);
    }

    void set_a_reward(int i)
    {
        int index = UnityEngine.Random.Range(0, rewards.Count);

        if (used_indexes.Count == rewards.Count)
        {
            used_indexes.Clear();
        }
        while (used_indexes.Contains(index))
        {
            index = UnityEngine.Random.Range(0, rewards.Count);
        }
        used_indexes.Add(index);


        if (rewards[index] == "coins")
        {
            dailyRewardItems[i].set_reward(rewards[index], rewards_text[index], coin_values[UnityEngine.Random.Range(0, coin_values.Count)], rewards_sprites[index], 900);
        }
        else if (rewards[index][0] == 'b')
        {
            dailyRewardItems[i].set_reward(rewards[index], rewards_text[index], 1, rewards_sprites[index], 3);
        }
        else
        {
            dailyRewardItems[i].set_reward(rewards[index], rewards_text[index], 1, rewards_sprites[index], 3);
        }
    }

    public void set_interectable_rewards(bool isInterectable)
    {
        for (int i = 0; i < dailyRewardItems.Count; i++)
        {
            dailyRewardItems[i].button.interactable = isInterectable;
        }
    }
}