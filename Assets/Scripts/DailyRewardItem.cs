﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class DailyRewardItem : MonoBehaviour
{
    public string type;
    public int value;
    public Image reward_img;
    int _default_value;

    public GameObject black_cover_go;
    public GameObject cover_go;
    public GameObject bg_go;
    public Button button;
    public TextMeshProUGUI item_name_text;
    public TextMeshProUGUI reward_text;



    public void set_reward(string reward_type, string _item_name_text, int value, Sprite reward_sprite, int default_value)
    {
        bg_go.SetActive(false);
        type = reward_type;
        this.value = value;
        reward_img.sprite = reward_sprite;
        reward_text.text = value.ToString();
        _default_value = default_value;
        item_name_text.text = _item_name_text;
    }

    public void get_reward()
    {
        set_front();
        SoundManager.PlaySFX("SurpriseBox_Turn");
        DailyReward.instance.set_interectable_rewards(false);
        DailyReward.instance.enable_claims();
        PlayerPrefs.SetInt(type, PlayerPrefs.GetInt(type, _default_value) + value);
    }

    Tween tween;
    public void set_front(float delay = 0)
    {
        if((tween != null) && tween.IsActive())
        {
            return;
        }

        black_cover_go.SetActive(delay != 0);

        bg_go.transform.DOBlendableLocalMoveBy(Vector3.zero, delay).OnComplete(
            () =>
            {
                bg_go.SetActive(true);
                cover_go.SetActive(false);
            }
            );
        
        tween = this.transform.DOScaleX(0, 0.2f).SetDelay(delay).OnComplete(() =>
        {
            this.transform.DOScaleX(1, 0.2f);
        });
    }
}
