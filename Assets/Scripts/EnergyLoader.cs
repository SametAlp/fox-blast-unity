﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class EnergyLoader : MonoBehaviour
{
    public static EnergyLoader instance;

    public TextMeshProUGUI remaining_time_text;
    public TextMeshProUGUI energy_value_text;
    public int wait_time = 30;

    public int energy_count = 5;
    public bool infiniteOn = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(this != instance)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        setting_start();
    }

    private void Update()
    {
        update_time();
    }

    public void set_energies(int minutes)
    {
        minutes = minutes >= wait_time * 5 ? wait_time * 5 : minutes;
        minutes = minutes <= 0  ? 0 : minutes;
        int energy_minus;

        energy_minus = Mathf.RoundToInt(minutes / (float)wait_time);
        energy_count = 5 - energy_minus;

        if (energy_count >= 5)
        {
            energy_count = 5;
        }
        energy_value_text.text = energy_count.ToString();
    }

    public void double_coins_video_watch()
    {
        PlayerPrefs.SetInt("get_energy", 1);
    }

    public void add_energy_with_diamond()
    {
        if(PlayerPrefs.GetInt("coins", 500) >= 200)
        {
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) - 200);
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "menu")
            {
                Menu.instance.coins_to_k();
            }
            add_energy();
        }
        else
        {
            Store.instance.open_shop();
        }
    }


    public void add_energy()
    {
        String savedDate = PlayerPrefs.GetString("5_energy_date", "");
        if (savedDate == "")
        {
            savedDate = DateTime.Now.ToString();
            PlayerPrefs.SetString("5_energy_date", savedDate);
        }
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        launchDate = launchDate.AddMinutes(-wait_time);
        if((launchDate - DateTime.Now).TotalSeconds < 0)
        {
            launchDate = DateTime.Now;
        }
        PlayerPrefs.SetString("5_energy_date", launchDate.ToString());


        PlayerPrefs.SetInt("get_energy", 0);
        setting_start();
        notif();
    }

    public void notif()
    {
    }

    public void remove_energy()
    {
        if(inf_energy_control("energy_hour_date", "energy_hour"))
        {
            return;
        }
        String savedDate = PlayerPrefs.GetString("5_energy_date", "");
        if (savedDate == "")
        { 
            savedDate = DateTime.Now.ToString();
            PlayerPrefs.SetString("5_energy_date", savedDate);
        }
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);

        if ((launchDate - DateTime.Now).TotalMinutes <= 0)
        {
            savedDate = DateTime.Now.ToString();
            PlayerPrefs.SetString("5_energy_date", savedDate);
            DateTime.TryParse(savedDate, out launchDate);
        }

        launchDate = launchDate.AddMinutes(wait_time);
        if ((launchDate - DateTime.Now).TotalMinutes > wait_time * 5)
        {
            launchDate = DateTime.Now.AddMinutes(wait_time * 5);
        }

        PlayerPrefs.SetString("5_energy_date", launchDate.ToString());


        PlayerPrefs.SetInt("get_energy", 0);
        setting_start();
        notif();
    }

    public void energy_date_control()
    {
        String savedDate = PlayerPrefs.GetString("5_energy_date", "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString("5_energy_date", savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        if(DateTime.Compare(launchDate, DateTime.Now) < 0)
        {
            add_energy();
        }
        // calculate days ellapsed since launch date:
        min = (launchDate - now).Minutes % 30;
        sec = (launchDate - now).Seconds;
        remaining_time_text.text = min.ToString() + ":" + (sec < 10 ? "0" + sec.ToString() : sec.ToString());

        if (sec == 0)
        {
            sec = -1;
            setting_start();
        }
    }

    public bool inf_energy_control(string date_str, string date_hour_str)
    {
        String savedDate = PlayerPrefs.GetString(date_str, "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString(date_str, savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        // calculate days ellapsed since launch date:
        double hours = (now - launchDate).TotalHours;
        long secs = (now - launchDate).Seconds;
        int dedicated_hour = PlayerPrefs.GetInt(date_hour_str, 0);

        if (hours < dedicated_hour && hours >= 0 && secs >= 0)
        {
            return true;
        }
        return false;
    }

    int min;
    int sec;
    void update_time()
    {
        if(infiniteOn)
        {
            return;
        }
        if (energy_count == 5)
        {
            remaining_time_text.text = "FULL";
            return;
        }
        energy_date_control();
    }

    void setting_start()
    {
        String savedDate = PlayerPrefs.GetString("5_energy_date", "");
        if (savedDate == "")
        {
            savedDate = DateTime.Now.ToString();
            PlayerPrefs.SetString("5_energy_date", savedDate);
        }
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        DateTime now = DateTime.Now;

        int m = (int)(launchDate - now).TotalMinutes;
        set_energies(m);
    }
}