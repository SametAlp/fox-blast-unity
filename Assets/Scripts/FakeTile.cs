using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Spine.Unity;


public class FakeTile : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public SpriteRenderer insideSpriteRenderer;

    public SkeletonAnimation skeletonAnimation;
    public GameObject skeletonAnimation_go;

    public GameObject sprite_go;
    public GameObject insideSprite_go;

    public void SetTileData(Sprite sprite, Sprite insideSprite)
    {
        spriteRenderer.sprite = sprite;
        insideSpriteRenderer.sprite = insideSprite;
        skeletonAnimation_go.SetActive(false);
        insideSprite_go.SetActive(true);
        sprite_go.SetActive(true);
    }

    public void SetCombo(Sprite sprite)
    {
        skeletonAnimation_go.SetActive(false);
        insideSprite_go.SetActive(false);
        sprite_go.SetActive(true);
        spriteRenderer.sprite = sprite;
    }

    public void SetSprite(Sprite sprite)
    {
        skeletonAnimation_go.SetActive(false);
        insideSprite_go.SetActive(false);
        sprite_go.SetActive(true);
        spriteRenderer.sprite = sprite;
    }

    public void SetSkeleton(SkeletonDataAsset skeletonDataAsset, string animation_name, bool loop)
    {
        insideSprite_go.SetActive(false);
        sprite_go.SetActive(false);
        skeletonAnimation_go.SetActive(true);
        skeletonAnimation.ClearState();

        skeletonAnimation.skeletonDataAsset = skeletonDataAsset;
        skeletonAnimation.Initialize(true);
        skeletonAnimation.loop = loop;
        skeletonAnimation.AnimationState.SetAnimation(0, animation_name, loop);
    }

    public void GoTo(Vector3 start_position, Vector3 end_position)
    {
        transform.position = start_position;
        this.gameObject.SetActive(true);
        transform.DOMove(end_position, 0.2f).SetEase(Ease.InBack).OnComplete(() =>
        {
            GridManager.instance.fakeTile_pool.Add(this);
            GridManager.instance.active_fakeTile--;
            this.gameObject.SetActive(false);
        });
    }

    public void GoToGoal(Vector3 start_position, Vector3 end_position, Action myFunction)
    {
        transform.position = start_position;
        this.gameObject.SetActive(true);
        GridManager.instance.active_fakeTile--;

        transform.DOScale(1f, 0f);
        transform.DOScale(1.3f, 0.25f);
        transform.DOScale(0f, 1f).SetEase(Ease.InExpo);
        transform.DOMove(end_position, 1f).SetEase(Ease.InBack).OnComplete(() =>
        {
            GridManager.instance.fakeTile_pool.Add(this);
            this.transform.DOBlendableLocalMoveBy(Vector3.zero, 0.1f).OnComplete(() =>
            {
                this.gameObject.SetActive(false);
                myFunction();
                });
        });
    }
}
