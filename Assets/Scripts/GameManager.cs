using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.IO;
using Spine.Unity;
using System;
using GameAnalyticsSDK;
using UnityEngine.EventSystems;

public enum State { PLAYING, PAUSE, FINISHED };

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Tile selectedTile;
    public Tile comboCheckTile;
    public Tile.TileColor oldTileColor;

    public List<Tile> selectedTiles;
    public List<Tile> selectedComboTiles;
    public List<Tile> comboChecktiles;

    public float scale;

    public Values values;

    public GameObject lightning_prefab;
    public Transform lightning_parent;
    public List<LightningBolt> lightning_pool;
    public bool lightningActive = false;

    public GameObject rocket_prefab;
    public Transform rocket_parent;
    public List<Rocket> rocket_pool;

    public int active_rockets = 0;
    public int total_active_combo = 0;
    public int recursive_bomb_count = 0;
    public bool isEntered = false;

    public GameObject goal_prefab;
    public Transform goal_parent;
    public List<Goal> goal_list;

    public State state;
    public int move_count = 0;
    public TextMeshProUGUI move_text;

    public bool isBoostActive;
    public int boostIndex = 0;
    public GameObject sling_boost_go;
    public SkeletonAnimation sling_boost;
    public BoostHorizontal horizontal_boost;
    public BoostVertical vertical_boost;
    public Vector2 screenBounds;
    public Camera mainCamera;

    public Image grid_background_img;

    public bool spreadSlime = false;

    public GameObject RocketBomb_go;
    public GameObject BombBomb_go;

    public bool isTutorial;

    public SkeletonGraphic fox_animation;

    public bool isSkipped;
    public Transform tileMapMask_transform;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            float scale_def = 1080f / 1920f;
            float scale_screen = (float)Screen.width / Screen.height;
            scale = scale_screen / scale_def;
            if (scale > 1)
            {
                scale = 1;
            }
            goal_list = new List<Goal>();
        }
    }

    private void Start()
    {
        EnergyLoader.instance.remove_energy();
        set_sizes();
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, mainCamera.transform.position.z));
        Application.targetFrameRate = 60;
    }

    public void set_sizes()
    {
        List<GameObject> objs = new List<GameObject>(GameObject.FindGameObjectsWithTag("scale"));

        for (int i = 0; i < objs.Count; i++)
        {
            if (objs[i].transform != null)
            {
                objs[i].transform.localScale *= scale;
            }
            else
            {
                objs[i].GetComponent<RectTransform>().localScale *= scale;
            }
        }
        GridManager.instance.first_item_pos *= scale;
        GridManager.instance.GRID_FAR_X *= scale;
        GridManager.instance.GRID_FAR_Y *= scale;
        GridManager.instance.FIRST_ITEM_OFFSET_X *= scale;
        GridManager.instance.FIRST_ITEM_OFFSET_Y *= scale;
        tileMapMask_transform.position *= scale;
    }

    public Transform stars_parent;
    public GameObject star_prefab;
    public List<Tile> finished_tiles;
    void create_combo_item_for_end_game(int i = 0)
    {
        List<int> number_list = new List<int>();
        number_list = Enumerable.Range(0, GridManager.instance.ROWS * GridManager.instance.COLUMNS - 1).ToList().OrderBy(a => Guid.NewGuid()).ToList();
        int counter = 0;

        int random_index_x = number_list[counter] % 9;
        int random_index_y = number_list[counter] / 9;
        while (GridManager.instance.gridTiles[random_index_x][random_index_y].GetObstacle() != null || GridManager.instance.gridTiles[random_index_x][random_index_y].tile_type != Tile.TileType.TILE)
        {
            counter++;
            if (counter >= number_list.Count)
            {
                break;
            }
            random_index_x = number_list[counter] % 9;
            random_index_y = number_list[counter] / 9;
        }
        GameObject go = Instantiate(star_prefab, stars_parent);
        go.transform.localScale = Vector3.one;
        go.transform.position = move_text.transform.position;
        go.transform.DOMove(GridManager.instance.gridTiles[random_index_x][random_index_y].transform.position, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        {
            if (!isSkipped)
                move_text.text = (--move_count < 0 ? 0 : move_count).ToString();
            finished_tiles.Add(GridManager.instance.gridTiles[random_index_x][random_index_y]);
            create_manual_combo(GridManager.instance.gridTiles[random_index_x][random_index_y], Tile.ComboType.HORIZONTAL_ROCKET);
            SoundManager.PlaySFX("button_sound");
        });
    }

    void create_combo_item()
    {
        List<int> number_list = new List<int>();
        number_list = Enumerable.Range(0, GridManager.instance.ROWS * GridManager.instance.COLUMNS - 1).ToList().OrderBy(a => Guid.NewGuid()).ToList();
        int counter = 0;

        int random_index_x = number_list[counter] % 9;
        int random_index_y = number_list[counter] / 9;
        while (GridManager.instance.gridTiles[random_index_x][random_index_y].GetObstacle() != null || GridManager.instance.gridTiles[random_index_x][random_index_y].tile_type != Tile.TileType.TILE)
        {
            counter++;
            if (counter >= number_list.Count)
            {
                break;
            }
            random_index_x = number_list[counter] % 9;
            random_index_y = number_list[counter] / 9;
        }

        create_manual_combo(GridManager.instance.gridTiles[random_index_x][random_index_y], Tile.ComboType.HORIZONTAL_ROCKET);
        SoundManager.PlaySFX("button_sound");
        
    }

    public List<GameObject> stars;
    public Image star_bar;
    public int score = 0;
    public int total_value_star_bar = 0;
    float percentage;

    public void add_score(int value)
    {
        score += value;
        percentage = (float)score / (float)total_value_star_bar;
        star_bar.DOFillAmount(percentage, 1f).SetEase(Ease.OutCirc);

        if (percentage >= 0.3f && !stars[0].activeSelf)
        {
            stars[0].SetActive(true);
        }
        else if (percentage >= 0.75f && !stars[1].activeSelf)
        {
            stars[1].SetActive(true);
        }
        else if (percentage >= 0.92f && !stars[2].activeSelf)
        {
            stars[2].SetActive(true);
        }
    }

    public int GetStarCount()
    {
        return (stars[0].activeSelf ? 1 : 0) + (stars[1].activeSelf ? 1 : 0) + (stars[2].activeSelf ? 1 : 0);
    }

    public void GenerateLightning(Vector3 startPosition, Vector3 endPosition)
    {
        lightningActive = true;
        LightningBolt lightningBolt;
        if (lightning_pool.Count > 0)
        {
            lightningBolt = lightning_pool[0];
            lightningBolt.lineRenderer.positionCount = 0;
            lightning_pool.RemoveAt(0);
            lightningBolt.gameObject.SetActive(true);
        }
        else
        {
            GameObject lightning_pool_go = Instantiate(lightning_prefab, lightning_parent);
            lightningBolt = lightning_pool_go.GetComponent<LightningBolt>();
        }
        lightningBolt.ActivateExplosion(startPosition, endPosition);
    }

    public Rocket GetRocket(Vector3 position)
    {
        if (rocket_pool.Count > 0)
        {
            Rocket temp_rocket = rocket_pool[0];
            rocket_pool.RemoveAt(0);
            temp_rocket.transform.position = position;
            temp_rocket.gameObject.SetActive(true);
            return temp_rocket;
        }
        GameObject rocket_go = Instantiate(rocket_prefab, rocket_parent);
        rocket_go.transform.position = position;
        return rocket_go.GetComponent<Rocket>();
    }
     
    public BoostHorizontal GetHorizontalBoost(Vector3 position)
    {
        horizontal_boost.transform.position = position;
        return horizontal_boost.GetComponent<BoostHorizontal>();
    }

    public BoostVertical GetVerticalBoost(Vector3 position)
    {
        vertical_boost.transform.position = position;
        return vertical_boost.GetComponent<BoostVertical>();
    }

    Tween grid_background_tween;
    public void ActivateBoost()
    {
        if(isTutorial)
            Tutorial.instance.DisableTutorialVisuals();
        if (grid_background_tween != null && grid_background_tween.IsActive())
        {
            grid_background_tween.Kill();
        }
        grid_background_tween = grid_background_img.DOColor(new Color(0.2f, 0.2f, 0.2f), 0.2f);
    }

    public void DeActivateBoost()
    {
        if (grid_background_tween != null && grid_background_tween.IsActive())
        {
            grid_background_tween.Kill();
        }
        grid_background_tween = grid_background_img.DOColor(Color.white, 0.2f);
        for(int i = 0; i < PopUps.instance.boosts.Count; i++)
        {
            PopUps.instance.boosts[i].activated_go.SetActive(false);
        }
    }

    public void CheckCombos(bool isInit = false)
    {
        StartCoroutine(CheckCombos_routine(isInit));
    }

    IEnumerator CheckCombos_routine(bool isInit = false)
    {
        while (GridManager.instance.settingAllTiles || GridManager.instance.total_count_tile > 0)
        {
            yield return null;
        }
        CheckComboAvailabilityOnTiles();

        if (isInit)
            yield break;
        if (spreadSlime && hasObstacleAsGoal(Obstacle.ObstacleType.SLIME))
        {
            SlimeSpread();
            CheckComboAvailabilityOnTiles();
        }
        else
        {
            spreadSlime = true;
        }
    }

    void CheckComboAvailabilityOnTiles()
    {
        ResetVisits();
        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                GridManager.instance.gridTiles[i][j].CheckComboAvailability();
                HandleComboAnimForTile(GridManager.instance.gridTiles[i][j]);
            }
        }
        HintManager.instance.CheckTilesPlayable();
    }

    void HandleComboAnimForTile(Tile temp_tile)
    {
        if (!temp_tile.isActive)
            return;
        if (temp_tile.tile_type == Tile.TileType.COMBO)
        {
            if (temp_tile.NeighborCombo())
            {
                if(!temp_tile.hasComboAnim)
                {
                    temp_tile.comboAnim_go = GridManager.instance.GetComboAnim(temp_tile.transform);
                    temp_tile.hasComboAnim = true;
                }
            }
            else
            {
                if (temp_tile.hasComboAnim)
                {
                    temp_tile.comboAnim_go.SetActive(false);
                    temp_tile.comboAnim_go.transform.SetParent(GridManager.instance.comboAnim_parent);
                    GridManager.instance.comboAnim_pool.Add(temp_tile.comboAnim_go);
                    temp_tile.comboAnim_go = null;
                    temp_tile.hasComboAnim = false;
                }
            }
        }
        else if (temp_tile.hasComboAnim)
        { 
            temp_tile.comboAnim_go.SetActive(false);
            temp_tile.comboAnim_go.transform.SetParent(GridManager.instance.comboAnim_parent);
            GridManager.instance.comboAnim_pool.Add(temp_tile.comboAnim_go);
            temp_tile.comboAnim_go = null;
            temp_tile.hasComboAnim = false;
        }
    }

    #region GOALS

    public void IncrementGoal(Obstacle.ObstacleType obstacleType, int count = 1, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        if(obstacleType == Obstacle.ObstacleType.EMPTY && tileColor == Tile.TileColor.NONE)
        {
            return;
        }
        bool hasCreatedBefore = false;
        for(int i = 0; i < goal_list.Count; i++)
        {
            if(goal_list[i].GetObstacleType() == obstacleType && tileColor == Tile.TileColor.NONE)
            {
                goal_list[i].IncrementValue();
                hasCreatedBefore = true;
            }
            if (goal_list[i].GetObstacleType() == Obstacle.ObstacleType.EMPTY && tileColor != Tile.TileColor.NONE && tileColor == goal_list[i].GetTileColor())
            {
                total_value_star_bar += 50 * count;
                goal_list[i].IncrementValue();
                hasCreatedBefore = true;
            }
        }
        if(!hasCreatedBefore)
        {
            GameObject go = Instantiate(goal_prefab, goal_parent);
            Goal goal = go.GetComponent<Goal>();
            goal.GenerateGoal(obstacleType, count, tileColor);
            goal_list.Add(goal);
        }
    }

    public bool hasObstacleAsGoal(Obstacle.ObstacleType obstacleType)
    {
        for (int i = 0; i < goal_list.Count; i++)
        {
            if (goal_list[i].GetObstacleType() == obstacleType)
            {
                return true;
            }
        }
        return false;
    }

    public bool hasGoalDone(Obstacle.ObstacleType obstacleType = Obstacle.ObstacleType.EMPTY, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        for (int i = 0; i < goal_list.Count; i++)
        {
            if (obstacleType != Obstacle.ObstacleType.EMPTY && goal_list[i].GetObstacleType() == obstacleType && goal_list[i].GetValue() <= 0)
            {
                return true;
            }

            if (tileColor != Tile.TileColor.NONE && goal_list[i].GetTileColor() == tileColor && goal_list[i].GetValue() <= 0)
            {
                return true;
            }
        }
        return false;
    }

    public void DecrementGoal(Obstacle.ObstacleType obstacleType, Tile.TileColor tileColor = Tile.TileColor.NONE, Obstacle obstacle = null, Tile tile = null, bool withAnimation = false)
    {
        withAnimation = obstacleType == Obstacle.ObstacleType.BEAR || obstacleType == Obstacle.ObstacleType.COLLECTABLE || withAnimation;
        for (int i = 0; i < goal_list.Count; i++)
        {
            if (goal_list[i].GetObstacleType() == obstacleType || tileColor != Tile.TileColor.NONE && goal_list[i].GetTileColor() == tileColor)
            {
                if (obstacle != null)
                {
                    obstacle.gameObject.SetActive(false);
                    int index_value = i;
                    if(withAnimation && !hasGoalDone(obstacleType))
                    {
                        FakeTile fakeTile = GridManager.instance.GetFakeTile(obstacle.transform.position);
                        if (obstacleType == Obstacle.ObstacleType.BEAR)
                        {
                            fakeTile.SetSkeleton(GridManager.instance.obstacle_skeletonAnimations[1], "4", false);
                        }
                        else
                        {
                            fakeTile.SetSprite(obstacle.spriteRenderer.sprite);
                        }
                        GridManager.instance.active_fakeTile++;
                        GridManager.instance.CollapseGrid(0.5f, true);
                        fakeTile.GoToGoal(obstacle.transform.position, goal_list[i].transform.position, () =>
                        { SoundManager.PlaySFX("ItemCollect"); goal_list[index_value].DecrementValue(); CheckWinLost(); GridManager.instance.active_fakeTile--; });
                    }
                    else
                    {
                        goal_list[index_value].DecrementValue();
                        CheckWinLost();
                    }
                }
                else if(tileColor != Tile.TileColor.NONE && goal_list[i].GetTileColor() == tileColor)
                {
                    int index_value = i;
                    if (withAnimation && tile.tile_type == Tile.TileType.TILE && !hasGoalDone(tileColor: tileColor))
                    {
                        FakeTile fakeTile = GridManager.instance.GetFakeTile(tile.transform.position);
                        fakeTile.SetTileData(tile.spriteRenderer.sprite, tile.insideSpriteRenderer.sprite);
                        GridManager.instance.active_fakeTile++;
                        fakeTile.GoToGoal(tile.transform.position, goal_list[i].transform.position, () =>
                        { SoundManager.PlaySFX("ItemCollect"); goal_list[index_value].DecrementValue(); CheckWinLost(); GridManager.instance.active_fakeTile--; });
                    }
                    else
                    {
                        goal_list[index_value].DecrementValue();
                        CheckWinLost();
                    }
                }
            }
        }
    }

    #endregion

    #region HANDLE_BLAST
    public void ResetVisits()
    {
        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                GridManager.instance.gridTiles[i][j].visited_combo = false;
                GridManager.instance.gridTiles[i][j].visited_ingame = false;
                GridManager.instance.gridTiles[i][j].combo_available = false;
                GridManager.instance.gridTiles[i][j].boostPassed = false;
                GridManager.instance.gridTiles[i][j].rocket_passed_list.Clear();
                if (GridManager.instance.gridTiles[i][j].GetObstacle())
                {
                    GridManager.instance.gridTiles[i][j].GetObstacle().isDecremented = false;
                    GridManager.instance.gridTiles[i][j].GetObstacle().rocket_passed_list.Clear();
                    GridManager.instance.gridTiles[i][j].GetObstacle().bomb_passed_list.Clear();
                }
            }
        }
    }

    public bool PermissionToStartTouch()
    {
        return GridManager.instance.active_fakeTile > 0
            || GridManager.instance.total_count_tile > 0
            || active_rockets > 0
            || state == State.FINISHED;
    }

    void DecreaseMove()
    {
        move_count--;
        move_count = move_count < 0 ? 0 : move_count;
        move_text.text = move_count.ToString();
        if(move_count == 5)
        {
            SoundManager.PlaySFX("Last_Five_Moves");

            fox_animation.AnimationState.SetAnimation(0,UnityEngine.Random.Range(0,2) == 0 ? "tedirgin_01" : "tedirgin_02", false).Complete += delegate
            {
                //skeletonAnimation.AnimationState.SetAnimation(0, "animation_02", true);
            };
        }
    }

    bool CheckGoalsFinished()
    {
        bool isDone = true;
        for(int i = 0; i < goal_list.Count; i++)
        {
            if(!goal_list[i].tick_go.activeSelf)
            {
                isDone = false;
                return isDone;
            }
        }
        return isDone;
    }

    void ResetTilePreviews()
    {
        for(int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                GridManager.instance.gridTiles[i][j].ResetItemColorIcon();
            }
        }
    }

    Coroutine end_of_game_routine;
    public void CheckWinLost()
    {
        StartCoroutine(CheckWinLost_routine());
    }

    IEnumerator CheckWinLost_routine()
    {
        if (state == State.FINISHED)
        {
            yield break;
        }
        if (lightningActive || active_rockets > 0 || GridManager.instance.active_fakeTile > 0 || GridManager.instance.active_bomb > 0 || total_active_combo > 0)
        {
            yield break;
        }

        if (CheckGoalsFinished())
        {
            int level = GridManager.instance.level;
            if (level < GameManager.instance.values.max_level)
            {
                PlayerPrefs.SetInt("level", level + 1);
            }
            SoundManager.PlaySFX("Win_Game");
            state = State.FINISHED;
            ResetTilePreviews();
            fox_animation.AnimationState.SetAnimation(0, UnityEngine.Random.Range(0, 2) == 0 ? "cok_sevincli_01" : "cok_sevincli_02", false).Complete += delegate
            {
                //skeletonAnimation.AnimationState.SetAnimation(0, "animation_02", true);
            };
            end_of_game_routine = StartCoroutine(StartEndOfGame());
        }
        else if(move_count <= 0)
        {
            SoundManager.PlaySFX("Lose_Game");
            state = State.FINISHED;
            fox_animation.AnimationState.SetAnimation(0, UnityEngine.Random.Range(0, 2) == 0 ? "uzgun_01" : "uzgun_02", false).Complete += delegate
            {
                //skeletonAnimation.AnimationState.SetAnimation(0, "animation_02", true);
            };
            PopUps.instance.open_continue_panel();
        }
    }

    public void FirstContinue()
    {
        move_count = 5;
        move_text.text = move_count.ToString();
        PopUps.instance.close_continue_panel();
        state = State.PLAYING;
        fox_animation.AnimationState.SetAnimation(0, "heyecanli", true);
    }

    public void SecondContinue()
    {
        move_count = 5;
        move_text.text = move_count.ToString();
        create_combo_item();
        PopUps.instance.close_continue_panel();
        state = State.PLAYING;
        fox_animation.AnimationState.SetAnimation(0, "heyecanli", true);
    }

    int remaining_move_count = 0;
    IEnumerator StartEndOfGame()
    {
        yield return new WaitForSeconds(0.5f);
        level_complete_go.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        skip_go.SetActive(true);
        yield return new WaitForSeconds(0.5f);

        remaining_move_count = move_count;
        for (int i = 0; i < remaining_move_count; i++)
        {
            SoundManager.PlaySFX("SurpriseBox_Turn");

            create_combo_item_for_end_game(i);
            add_score(75);
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(1f);

        for (int i = 0; i < finished_tiles.Count; i++)
        {
            HandleCombos(finished_tiles[i]);
            yield return new WaitForSeconds(0.2f);
        }
        while(active_rockets > 0)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        skip_go.SetActive(false);
        level_complete_go.SetActive(false);
        GridManager.instance.CollapseGrid(dontAllowSpreadSlime: true);
        yield return new WaitForSeconds(1);
        PopUps.instance.open_win_panel();
    }

    public GameObject skip_go;
    public GameObject level_complete_go;
    public void skip()
    {
        StopCoroutine(end_of_game_routine);
        isSkipped = true;
        skip_go.SetActive(false);
        level_complete_go.SetActive(false);

        for (int i = 0; i < remaining_move_count; i++)
        {
            add_score(GridManager.instance.level < 5 ? 220 : 110);
        }

        remaining_move_count = 0;
        move_count = 0;
        move_text.text = "0";
        GridManager.instance.CollapseGrid(dontAllowSpreadSlime: true);
        PopUps.instance.open_win_panel();
    }

    public void HandleBlast(Vector2 touch)
    {
        if (PermissionToStartTouch() || total_active_combo > 0)
        {
            return;
        }

        active_rockets = 0;
        ResetVisits();
        selectedTiles.Clear();
        selectedTile = TileCloseToPoint(touch);

        if (!isBoostActive)
        {
            if (!selectedTile.isActive || selectedTile.GetObstacle() && !selectedTile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
            {
                return;
            }
        }

        if (isBoostActive && selectedTile != null)
        {
            if(boostIndex == 0)
            {
                sling_boost_go.transform.position = selectedTile.transform.position;
                sling_boost_go.SetActive(true);
                SoundManager.PlaySFX("Slingshot");

                sling_boost.AnimationState.SetAnimation(0, "animation", false).Complete += delegate
                {
                    DisableTile(selectedTile);
                    sling_boost_go.SetActive(false);
                    isBoostActive = false;
                    PopUps.instance.boosts[boostIndex].Used();
                    DeActivateBoost();
                    GridManager.instance.CollapseGrid(dontAllowSpreadSlime: true);
                };
                return;
            }
            else if (boostIndex == 1)
            {
                Vector3 start_pos = new Vector3(selectedTile.transform.position.x - screenBounds.x - 2, selectedTile.transform.position.y, selectedTile.transform.position.z);
                horizontal_boost = GetHorizontalBoost(start_pos);
                horizontal_boost.gameObject.SetActive(true);
                SoundManager.PlaySFX("Lawnmover");
                horizontal_boost.Move(start_pos, Direction.RIGHT);
            }
            else if (boostIndex == 2)
            {
                Vector3 start_pos = new Vector3(selectedTile.transform.position.x, selectedTile.transform.position.y - screenBounds.y - 2, selectedTile.transform.position.z);
                vertical_boost = GetVerticalBoost(start_pos);
                vertical_boost.gameObject.SetActive(true);
                SoundManager.PlaySFX("Leaf_Blower");
                vertical_boost.Move(start_pos, Direction.UP);
            }
            else if (boostIndex == 3)
            {
                SoundManager.PlaySFX("Weathercock");
                HintManager.instance.Shuffle();
            }


            isBoostActive = false;
            PopUps.instance.boosts[boostIndex].Used();
            DeActivateBoost();
            GridManager.instance.CollapseGrid(dontAllowSpreadSlime: true);
        }
        else if (selectedTile != null && (!selectedTile.GetObstacle() && selectedTile.tile_type == Tile.TileType.TILE
                                    || selectedTile.tile_type == Tile.TileType.TILE && selectedTile.GetObstacle() && selectedTile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED)))
        {
            if(isTutorial && selectedTile.spriteRenderer.sortingOrder == Tutorial.instance.default_tile_sorting)
            {
                return;
            }
            else if(isTutorial && selectedTile.spriteRenderer.sortingOrder > Tutorial.instance.default_tile_sorting)
            {
                Tutorial.instance.GetTilesBackWithSameColor();
            }

            recursive_distribution(selectedTile.column, selectedTile.row);
            selectedTiles = selectedTiles.Distinct().ToList();
            if (selectedTiles.Count <= 1)
                return;
            SoundManager.PlaySFX("Item_Pop");
            DecreaseMove();

            if (selectedTiles.Count >= values.critical1_value)
            {
                selectedTile.CollectAnimation();
            }
            else
            {
                for (int i = 0; i < selectedTiles.Count; i++)
                {
                    GridManager.instance.StartTileExplosion(selectedTiles[i]);
                }
            }

            oldTileColor = selectedTile.tile_color;
            HandleAllNeighborsOnce();

            create_combo(selectedTile, selectedTiles.Count);

            DisableSelectedTiles();
            GridManager.instance.CollapseGrid();
        }
        else if (selectedTile != null && selectedTile.tile_type == Tile.TileType.COMBO)
        {
            if (isTutorial && selectedTile.spriteRenderer.sortingOrder == Tutorial.instance.default_tile_sorting)
            {
                return;
            }
            else if (isTutorial && selectedTile.spriteRenderer.sortingOrder > Tutorial.instance.default_tile_sorting)
            {
                Tutorial.instance.GetTilesBackWithSameColor();
            }

            fox_animation.AnimationState.SetAnimation(0, UnityEngine.Random.Range(0, 2) == 0 ? "cok_sevincli_01" : "cok_sevincli_02", false).Complete += delegate
            {
                fox_animation.AnimationState.SetAnimation(0, "heyecanli", true);
            };

            DecreaseMove();
            ResetVisits();
            selectedComboTiles = new List<Tile>();
            GetTiles(selectedTile);
            selectedComboTiles = selectedComboTiles.Distinct().ToList();
            if (CheckNeighborCombo(selectedComboTiles))
            {
                selectedComboTiles.Add(selectedTile);
                HandleComboMerge(selectedTile, selectedComboTiles);
            }
            else
            {
                HandleCombos(selectedTile);
                selectedTiles.Add(selectedTile);
                DisableSelectedTiles();
                GridManager.instance.CollapseGrid();
            }
        }
    }

    void DisableSelectedTiles()
    {
        selectedTiles.Select(s => s.GetObstacle() && s.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED) || !s.GetObstacle());
        bool _withAnimation = selectedTiles.Count < values.critical1_value;
        for (int i = 0; i < selectedTiles.Count; i++)
        {
            if (selectedTiles[i].isActive && selectedTiles[i].GetObstacle() && selectedTiles[i].GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED))
            {
                HandleObstacle(selectedTiles[i].column, selectedTiles[i].row);
            }
            else
            {
                selectedTiles[i].gameObject.SetActive(false);
                DecrementGoal(Obstacle.ObstacleType.EMPTY, selectedTiles[i].tile_color, tile: selectedTiles[i], withAnimation: _withAnimation);
            }
        }
    }

    public void DisableTile(Tile temp_tile, GameObject bomb = null, GameObject rocket = null, bool joker = false)
    {
        int column = temp_tile.column;
        int row = temp_tile.row;
        GridManager.instance.StartTileExplosion(temp_tile);
        if (temp_tile.GetObstacle())
        {
            HandleObstacle(column, row, Tile.TileColor.JOKER, bomb: bomb, rocket: rocket, joker: joker);
        }
        else
        {
            temp_tile.gameObject.SetActive(false);
            DecrementGoal(Obstacle.ObstacleType.EMPTY, temp_tile.tile_color, tile: temp_tile);
        }
    }

    public void HandleBlast()
    {
        DisableSelectedTiles();
        GridManager.instance.CollapseGrid(0.5f, dontAllowSpreadSlime: true);
    }

    public Tile TileCloseToPoint(Vector2 point, bool isTouch = true)
    {
        Vector3 t;
        if (isTouch)
            t = Camera.main.ScreenToWorldPoint(point);
        else
            t = point;
        t.z = 0;

        int c = Mathf.FloorToInt((t.x - GridManager.instance.FIRST_ITEM_OFFSET_X + GridManager.instance.GRID_FAR_X * 0.5f) / GridManager.instance.GRID_FAR_X);
        if (c < 0)
            return null;
        if (c >= GridManager.instance.COLUMNS)
            return null;

        int r = Mathf.FloorToInt((t.y - GridManager.instance.FIRST_ITEM_OFFSET_Y + GridManager.instance.GRID_FAR_Y * 0.5f) / GridManager.instance.GRID_FAR_Y);
        if (r < 0)
            return null;
        if (r >= GridManager.instance.ROWS)
            return null;

        float sensitivity = 0.72f;
        if (selectedTile == null)
        {
            sensitivity = 10;
        }
        float sensitivity_x = Mathf.Abs(t.x - GridManager.instance.gridTiles[c][r].transform.position.x);
        float sensitivity_y = Mathf.Abs(t.y - GridManager.instance.gridTiles[c][r].transform.position.y);
        if (sensitivity_x >= sensitivity || sensitivity_y >= sensitivity)
        {
            return null;
        }
        return GridManager.instance.gridTiles[c][r];
    }
    #endregion

    #region HANDLE_OBSTACLES

    void HandleAllNeighborsOnce()
    {
        selectedTiles.Add(selectedTile);
        HandleSelectedTilesObstacles(selectedTiles);
        selectedObstacleListIndexes = new List<Indexes>();

        for (int i = 0; i < selectedTiles.Count; i++)
        {
            HandleNeighborObstacles(selectedTiles[i].column, selectedTiles[i].row);
        }
        selectedObstacleListIndexes = selectedObstacleListIndexes.Distinct().ToList();
        for (int i = 0; i < selectedObstacleListIndexes.Count; i++)
        {
            HandleObstacle(selectedObstacleListIndexes[i].x, selectedObstacleListIndexes[i].y, oldTileColor);
        }
        selectedTiles.Remove(selectedTile);
    }

    void HandleAllNeighborsOnce(List<Tile> tiles_to_be_handled, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        HandleSelectedTilesObstacles(tiles_to_be_handled);
        selectedObstacleListIndexes = new List<Indexes>();

        for (int i = 0; i < tiles_to_be_handled.Count; i++)
        {
            HandleNeighborObstacles(tiles_to_be_handled[i].column, tiles_to_be_handled[i].row);
        }
        selectedObstacleListIndexes = selectedObstacleListIndexes.Distinct().ToList();
        for (int i = 0; i < selectedObstacleListIndexes.Count; i++)
        {
            HandleObstacle(selectedObstacleListIndexes[i].x, selectedObstacleListIndexes[i].y, tileColor);
        }
    }

    List<Indexes> selectedObstacleListIndexes;
    public void HandleObstacle(int column, int row, Tile.TileColor tileColor = Tile.TileColor.NONE, GameObject rocket = null, GameObject bomb = null, bool joker = false)
    {
        Tile tile = GridManager.instance.gridTiles[column][row];
        
        List<Obstacle> obstacles = GridManager.instance.gridObstacles[column][row];
        if(obstacles.Count == 0)
        {
            return;
        }
        Obstacle obstacle = obstacles[0];

        if(!tile.CanDecrementCollectable())
        {
            return;
        }
        if(obstacle.obstacleType != Obstacle.ObstacleType.COLLECTABLE && obstacle.GetFeatureValue(Obstacle.ObstacleFeature.CANNOT_BE_DESTROYED))
        {
            return;
        }
        if(obstacle.GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED) && !GridManager.instance.gridTiles[column][row].isActive)
        {
            return;
        }
        if(obstacle.GetFeatureValue(Obstacle.ObstacleFeature.DESTROYED_WITH_ONLY_COMBO) && !(rocket != null || bomb != null || joker || tileColor == Tile.TileColor.JOKER))
        {
            return;
        }

        if(obstacle.GetFeatureValue(Obstacle.ObstacleFeature.ONE_DECREMENT_WHEN_PASSED))
        {
            if (rocket != null && obstacle.rocket_passed_list.Contains(rocket))
            {
                return;
            }
            if (bomb != null && obstacle.bomb_passed_list.Contains(bomb))
            {
                return;
            }
        }
        if(bomb != null)
            obstacle.bomb_passed_list.Add(bomb);
        if (rocket != null)
            obstacle.rocket_passed_list.Add(rocket);


        if (obstacle.GetFeatureValue(Obstacle.ObstacleFeature.COLOR_SENSITIVE))
        {
            if (obstacle.GetFeatureValue(Obstacle.ObstacleFeature.LARGE))
            {
                obstacle.DecrementLargeObstacleColor(tileColor);
            }
            else
            {
                obstacle.DecrementObstacleColor(tileColor);
            }
            add_score(30);
        }
        else
        {
            obstacle.step_no -= 1;
            add_score(30);
        }

        if (obstacle.GetFeatureValue(Obstacle.ObstacleFeature.WITH_STEPS) && obstacle.step_no > 0)
        {
            obstacle.SetNewStepSprite();
        }

        if (obstacle.obstacleType == Obstacle.ObstacleType.COLLECTABLE)
        {
            Tile temp_tile = GridManager.instance.gridTiles[column][row];
            RemoveObstacle(obstacles, obstacle, temp_tile);
            return;
        }

        if (obstacle.old_step_no != obstacle.step_no)
        {
            obstacle.old_step_no = obstacle.step_no;
            SetSoundOfObstacle(obstacle.obstacleType);
        }


        if (obstacle.step_no == 0)
        {
            if(obstacle.GetFeatureValue(Obstacle.ObstacleFeature.LARGE))
            {
                column = obstacle.master_tile.column;
                row = obstacle.master_tile.row;
                for (int i = 0; i < 2; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[column + i][row + j];
                        List<Obstacle> temp_obstacles = GridManager.instance.gridObstacles[column + i][row + j];
                        RemoveObstacle(temp_obstacles, obstacle, temp_tile,(i == 0 && j == 0) ? true : false);
                    }
                }
            }
            else
            {
                Tile temp_tile = GridManager.instance.gridTiles[column][row];
                RemoveObstacle(obstacles, obstacle, temp_tile);
            }
        }
    }

    public void RemoveObstacle(List<Obstacle> obstacles, Obstacle obstacle, Tile temp_tile, bool decrement = true)
    {
        if (obstacle.obstacleType == Obstacle.ObstacleType.SLIME)
        {
            spreadSlime = false;
            obstacle.hasSpine = false;
        }
        if(obstacle.obstacleType == Obstacle.ObstacleType.BEAR)
        {
            obstacle.hasSpine = false;
        }

        if (decrement)
        {
            if (obstacle == null)
                DecrementGoal(Obstacle.ObstacleType.EMPTY, temp_tile.tile_color, tile: temp_tile);
            else
                DecrementGoal(obstacle.obstacleType, obstacle: obstacle);
        }

        bool needRemove = !(obstacle.GetFeatureValue(Obstacle.ObstacleFeature.CONTAINS_TILE) && !obstacle.GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED));
        obstacles.Remove(obstacle);

        obstacle.transform.SetParent(GridManager.instance.obstacle_parent);
        if (obstacle.obstacleType != Obstacle.ObstacleType.COLLECTABLE)
        {
            obstacle.gameObject.SetActive(false);
            GridManager.instance.obstacle_pool.Add(obstacle);
        }
        temp_tile.UpdateCurrentObstacles();
        if (!temp_tile.GetObstacle() || temp_tile.GetObstacle() && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CONTAINS_TILE))
        {
            if(needRemove)
                temp_tile.gameObject.SetActive(false);
        }
    }

    public void SetSoundOfObstacle(Obstacle.ObstacleType obstacleType)
    {
        if(obstacleType == Obstacle.ObstacleType.ROCK || obstacleType == Obstacle.ObstacleType.WHITE_ROCK)
        {
            SoundManager.PlaySFX("Stone_Break");
        }
        else if(obstacleType == Obstacle.ObstacleType.ICE)
        {
            SoundManager.PlaySFX("Ice_Break");
        }
        else if (obstacleType == Obstacle.ObstacleType.BUBBLE)
        {
            SoundManager.PlaySFX("Balloon_Pop");
        }
        else if (obstacleType == Obstacle.ObstacleType.COLLECTABLE)
        {
            SoundManager.PlaySFX("Apple_Collect");
        }
        else if (obstacleType == Obstacle.ObstacleType.BEAR)
        {
            SoundManager.PlaySFX("Mushroom_Pick");
        }
        else if (obstacleType == Obstacle.ObstacleType.CHAIN)
        {
            SoundManager.PlaySFX("Chain_Break");
        }
        else if (obstacleType == Obstacle.ObstacleType.NET)
        {
            SoundManager.PlaySFX("Net_Rip");
        }
        else if (obstacleType == Obstacle.ObstacleType.MUSHROOM)
        {
            SoundManager.PlaySFX("Mushroom_Pick");
        }
        else if (obstacleType == Obstacle.ObstacleType.GRASS)
        {
            SoundManager.PlaySFX("Grass_Cut");
        }
        else if (obstacleType == Obstacle.ObstacleType.SLIME)
        {
            SoundManager.PlaySFX("Slime_Pop");
        }
        else if (obstacleType == Obstacle.ObstacleType.FLOWER_BOX)
        {
            SoundManager.PlaySFX("Flower_Cut");
        }
    }


    public void HandleSelectedTilesObstacles(List<Tile> tiles)
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            Tile temp_tile = tiles[i];
            if (temp_tile.GetObstacle())
            {
                if (temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED))
                {
                    if (temp_tile.gameObject.activeSelf)
                    {
                        HandleObstacle(temp_tile.column, temp_tile.row);
                    }
                }
            }
        }
    }

    public void HandleNeighborObstacles(int column, int row)
    {
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if (arr_i[i] == 0 && arr_j[j] == 0)
                {
                    continue;
                }
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = column + arr_i[i];
                int index_y = row + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.GetObstacle())
                        {
                            if (!temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED) && !temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CANNOT_BE_DESTROYED))
                            {
                                selectedObstacleListIndexes.Add(new Indexes(index_x, index_y));
                            }
                        }
                    }
                }
            }
        }
        
    }

    #endregion

    #region RECURSIVE_SECTION
    void recursive_distribution(int x, int y)
    {
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if (arr_i[i] == 0 && arr_j[j] == 0)
                {
                    continue;
                }
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf
                            && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle() && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
                            && temp_tile.tile_color == selectedTile.tile_color)
                        {
                            if (!temp_tile.visited_ingame && temp_tile.isActive)
                            {
                                temp_tile.visited_ingame = true;
                                selectedTiles.Add(temp_tile);
                                recursive_distribution(index_x, index_y);
                            }
                        }
                    }
                }
            }
        }
    }

    public void recursive_distribution_combo_preview(int x, int y)
    {
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf
                            && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle() && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
                            && temp_tile.tile_color == comboCheckTile.tile_color)
                        {
                            if (!temp_tile.visited_combo && temp_tile.isActive)
                            {
                                temp_tile.visited_combo = true;
                                comboChecktiles.Add(temp_tile);
                                recursive_distribution_combo_preview(index_x, index_y);
                            }
                        }
                    }
                }
            }
        }
    }

    void SlimeSpread()
    {
        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];
                List<Obstacle> obstacles = temp_tile.GetObstacleList();

                for(int k = 0; k < obstacles.Count; k++)
                {
                    if(obstacles[k].obstacleType == Obstacle.ObstacleType.SLIME)
                    {
                        if(slime_assignment_check(i, j))
                            return;
                    }
                }
            }
        }
    }

    public bool slime_assignment_check(int x, int y)
    {
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1) || (arr_i[i] == 0 && arr_j[j] == 0))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf
                            && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle()
                            && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED)))
                        {
                            SoundManager.PlaySFX("Slime_Appear");
                            GridManager.instance.CreateGenericObstacle(index_x, index_y, Obstacle.ObstacleType.SLIME);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    #endregion

    #region MERGE_COMBOS
    public bool CheckNeighborCombo(List<Tile> tiles)
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            if (tiles[i].isActive && tiles[i].tile_type == Tile.TileType.COMBO)
            {
                return true;
            }
        }
        return false;
    }

    public void GetTiles(Tile tile)
    {
        if (tile.visited_combo)
        {
            return;
        }
        tile.visited_combo = true;
        int column = tile.column;
        int row = tile.row;
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if (i == 1 && j == 1)
                {
                    continue;
                }
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = column + arr_i[i];
                int index_y = row + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.tile_type == Tile.TileType.COMBO && !temp_tile.visited_ingame)
                        {
                            selectedComboTiles.Add(temp_tile);
                            GetTiles(temp_tile);
                        }
                    }
                }
            }
        }
    }

    public void HandleComboMerge(Tile tile, List<Tile> tiles)
    {
        tiles = tiles.Where(obj => obj.tile_type == Tile.TileType.COMBO).OrderBy(obj => (int)obj.combo_type).Reverse().Distinct().ToList();
        selectedTile = tiles[0];
        selectedTiles.AddRange(tiles);
        for (int i = 0; i < tiles.Count; i++)
        {
            Tile temp_tile = tiles[i];
            if(temp_tile.isActive && temp_tile.GetObstacle() && temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_USED))
            {
                HandleObstacle(temp_tile.column, temp_tile.row);
            }
            FakeTile fakeTile = GridManager.instance.GetFakeTile(temp_tile.transform.position);
            temp_tile.visited_ingame = true;
            if (i != 0)
                temp_tile.gameObject.SetActive(false);
            fakeTile.SetCombo(temp_tile.spriteRenderer.sprite);
            fakeTile.GoTo(temp_tile.transform.position, tile.transform.position);
        }
        StartCoroutine(HandleComboMerge_routine(tile, tiles));
    }

    IEnumerator HandleComboMerge_routine(Tile tile, List<Tile> tiles)
    {
        while (GridManager.instance.active_fakeTile > 0)
        {
            yield return null;
        }
        total_active_combo++;

        if (tiles[0].combo_type == Tile.ComboType.MAGNET && tiles[1].combo_type == Tile.ComboType.MAGNET)
        {
            HandleMagnetMagnet(tiles[0]);
        }
        else if (tiles[0].combo_type == Tile.ComboType.MAGNET && tiles[1].combo_type != Tile.ComboType.MAGNET)
        {
            HandleMagnetWithOthers(tiles[0], tiles[1].combo_type);
        }
        else if (tiles[0].combo_type == Tile.ComboType.BOMB && tiles[1].combo_type == Tile.ComboType.BOMB)
        {
            recursive_bomb_count = 0;
            HandleBombBomb(tile);
        }
        else if (tiles[0].combo_type == Tile.ComboType.BOMB && tiles[1].combo_type != Tile.ComboType.BOMB)
        {
            tiles[0].DisableVisual();
            tiles[1].DisableVisual();
            HandleBombRocket(tile);
        }
        else
        {
            HandleRocketRocket(tile);
        }
    }

    void HandleMagnetMagnet(Tile tile)
    {
        StartCoroutine(HandleMagnetMagnet_routine(tile));
    }

    IEnumerator HandleMagnetMagnet_routine(Tile tile)
    {
        selectedTiles.Add(tile);
        List<Tile> tiles_added = new List<Tile>();
        SoundManager.PlaySFX("Lightning");

        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];

                    HandleObstacle(i, j, temp_tile.tile_color);
                    GenerateLightning(tile.transform.position, temp_tile.transform.position);
                    yield return new WaitForSeconds(0.01f);
            }
        }

        lightningActive = false;


        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];
                if (temp_tile != null && temp_tile.gameObject.activeSelf && temp_tile.isActive)
                {
                    if (temp_tile.GetObstacle())
                    {
                        int column = temp_tile.column;
                        int row = temp_tile.row;
                        List<Obstacle> obstacles = GridManager.instance.gridObstacles[column][row];
                        Obstacle obstacle = obstacles[0];
                        if (!obstacle.isDecremented)
                        {
                            if (obstacle.GetFeatureValue(Obstacle.ObstacleFeature.ONE_DECREMENT_WHEN_PASSED))
                            {
                                obstacle.isDecremented = true;
                            }
                            HandleObstacle(column, row, Tile.TileColor.JOKER);
                        }
                    }
                    else
                    {
                        GridManager.instance.StartTileExplosion(temp_tile);
                        tiles_added.Add(temp_tile);
                        selectedTiles.Add(temp_tile);
                    }
                }
            }
        }
        HandleBlast();
        total_active_combo--;

    }

    void HandleMagnetWithOthers(Tile tile, Tile.ComboType comboType)
    {
        selectedTiles.Add(tile);
        List<Tile> tiles_added = new List<Tile>();

        StartCoroutine(HandleMagnetWithOthers_routine(tile, tiles_added, comboType));
    }

    IEnumerator HandleMagnetWithOthers_routine(Tile tile, List<Tile> tiles_added, Tile.ComboType comboType)
    {
        SoundManager.PlaySFX("Lightning");
        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];
                if (temp_tile != null && temp_tile.gameObject.activeSelf && temp_tile.isActive
                    && (!temp_tile.GetObstacle() || !temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.DESTROYED_WITH_ONLY_COMBO) && temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CONTAINS_TILE))
                    && temp_tile.tile_type == Tile.TileType.TILE
                    && temp_tile.tile_color == tile.tile_color)
                {
                    HandleObstacle(i, j, temp_tile.tile_color);
                    GenerateLightning(tile.transform.position, temp_tile.transform.position);
                    tiles_added.Add(temp_tile);
                    create_manual_combo(temp_tile, comboType);
                    selectedTiles.Add(temp_tile);
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
        tile.visited_combo = false;
        tiles_added.Add(tile);
        create_manual_combo(tile, comboType);
        selectedTiles.Add(tile);
        yield return new WaitForSeconds(0.5f);
        lightningActive = false;

        HandleMagnetWithOthers_Waiter(tiles_added);
    }

    void HandleBombBomb(Tile tile)
    {
        BombBomb_go.transform.position = tile.transform.position;
        BombBomb_go.SetActive(true);
        StartCoroutine(HandleBombBomb_routine(tile));
    }

    IEnumerator HandleBombBomb_routine(Tile tile)
    {
        SoundManager.PlaySFX("Fuse_Burn_Loop");

        yield return new WaitForSeconds(1);
        BombBomb_go.SetActive(false);
        Bomb bomb = GridManager.instance.StartBigBombExplosion(tile.transform.position);
        SoundManager.PlaySFX("Big_Explosion");
        selectedTiles.Add(tile);

        int column = tile.column;
        int row = tile.row;
        int[] arr_i = { -3, -2, -1, 0, 1, 2, 3 };
        int[] arr_j = { -3, -2, -1, 0, 1, 2, 3 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                int index_x = column + arr_i[i];
                int index_y = row + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (!temp_tile.visited_ingame)
                        {
                            temp_tile.visited_ingame = true;
                            if (temp_tile.tile_type == Tile.TileType.COMBO)
                            {
                                HandleCombos(temp_tile);
                                DisableTile(temp_tile, bomb: bomb.gameObject);
                            }
                            else
                            {
                                DisableTile(temp_tile, bomb: bomb.gameObject);
                            }
                        }
                    }
                }
            }
        }
        HandleBlast();
        total_active_combo--;

    }
    void HandleBombRocket(Tile tile)
    {
        RocketBomb_go.SetActive(false);
        RocketBomb_go.transform.position = tile.transform.position;
        tile.DisableVisual();
        RocketBomb_go.SetActive(true);
        StartCoroutine(HandleBombRocket_routine(tile));
    }

    IEnumerator HandleBombRocket_routine(Tile tile)
    {
        SoundManager.PlaySFX("DynamiteRocket_Interact");

        yield return new WaitForSeconds(1.7f);
        HandleRocketRocket(tile);
        Vector3 temp_position;
        temp_position = tile.transform.position + new Vector3(GridManager.instance.GRID_FAR_X * -1, 0, 0);
        HandleVerticalRocket(tile, true, temp_position);
        temp_position = tile.transform.position + new Vector3(GridManager.instance.GRID_FAR_X, 0, 0);
        HandleVerticalRocket(tile, true, temp_position);

        temp_position = tile.transform.position + new Vector3(0, GridManager.instance.GRID_FAR_Y * -1, 0);
        HandleHorizontalRocket(tile, true, temp_position);
        temp_position = tile.transform.position + new Vector3(0, GridManager.instance.GRID_FAR_Y, 0);
        HandleHorizontalRocket(tile, true, temp_position);


        HandleBlast();
        yield return new WaitForSeconds(1);
        total_active_combo--;

    }

    void HandleRocketRocket(Tile tile, bool positionGiven = false, Vector3 _position = new Vector3())
    {
        HandleHorizontalRocket(tile, positionGiven, _position);
        HandleVerticalRocket(tile, positionGiven, _position);
        HandleBlast();
        total_active_combo--;
    }
    #endregion

    #region COMBO_WAITERS

    public void HandleMagnetWithOthers_Waiter(List<Tile> tiles_added)
    {
        StartCoroutine(HandleMagnetWithOthers_Waiter_routine(tiles_added));
    }

    IEnumerator HandleMagnetWithOthers_Waiter_routine(List<Tile> tiles_added)
    {
        for (int i = 0; i < tiles_added.Count; i++)
        {
            if (tiles_added[i].gameObject.activeSelf && !tiles_added[i].visited_combo && tiles_added[i].tile_type == Tile.TileType.COMBO)
            {
                HandleCombos(tiles_added[i]);
                yield return new WaitForSeconds(0.2f);
            }
        }
        HandleBlast();
        total_active_combo--;

    }
    #endregion

    #region COMBOS

    void create_combo(Tile tile, int number_of_tiles)
    {
        if (number_of_tiles < values.critical1_value)
        {
            return;
        }
        selectedTiles.Remove(tile);

        if (number_of_tiles >= values.critical3_value)
        {
            fox_animation.AnimationState.SetAnimation(0,UnityEngine.Random.Range(0, 2) == 0 ? "cok_sevincli_01" : "cok_sevincli_02", false).Complete += delegate
            {
                fox_animation.AnimationState.SetAnimation(0, "heyecanli", true);
            };
            tile.SetCombo(Tile.ComboType.MAGNET, oldTileColor);
        }
        else if (number_of_tiles >= values.critical2_value)
        {
            tile.SetCombo(Tile.ComboType.BOMB);
        }
        else if (number_of_tiles >= values.critical1_value)
        {
            tile.SetCombo(UnityEngine.Random.Range(0, 2) == 0 ? Tile.ComboType.HORIZONTAL_ROCKET : Tile.ComboType.VERTICAL_ROCKET);
        }
        tile.gameObject.SetActive(true);
    }

    public void create_manual_combo(Tile tile, Tile.ComboType comboType, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        if (comboType == Tile.ComboType.MAGNET)
        {
            tile.SetCombo(Tile.ComboType.MAGNET, tileColor == Tile.TileColor.NONE ? (Tile.TileColor)UnityEngine.Random.Range(0, GridManager.instance.number_of_color) : tileColor);
        }
        else if (comboType == Tile.ComboType.BOMB)
        {
            tile.SetCombo(Tile.ComboType.BOMB);
        }
        else
        {
            tile.SetCombo(UnityEngine.Random.Range(0, 2) == 0 ? Tile.ComboType.HORIZONTAL_ROCKET : Tile.ComboType.VERTICAL_ROCKET);
        }
        tile.gameObject.SetActive(true);
    }

    public void HandleCombos(Tile tile)
    {
        Tile.ComboType combo_type = tile.combo_type;

        if (combo_type == Tile.ComboType.VERTICAL_ROCKET)
        {
            HandleVerticalRocket(tile);
        }
        else if (combo_type == Tile.ComboType.HORIZONTAL_ROCKET)
        {
            HandleHorizontalRocket(tile);
        }
        else if (combo_type == Tile.ComboType.BOMB)
        {
            HandleBomb(tile);
        }
        else if (combo_type == Tile.ComboType.MAGNET)
        {
            HandleMagnet(tile);
        }
    }

    void HandleVerticalRocket(Tile tile, bool positionGiven = false, Vector3 _position = new Vector3())
    {
        if (!positionGiven)
        {
            _position = tile.transform.position;
            selectedTiles.Add(tile);
            tile.gameObject.SetActive(false);
        }

        Rocket rocket_1 = GetRocket(_position);
        Rocket rocket_2 = GetRocket(_position);
        rocket_1.Move(_position, Direction.UP);
        rocket_2.Move(_position, Direction.DOWN);
        SoundManager.PlaySFX("Rocket");

    }

    void HandleHorizontalRocket(Tile tile, bool positionGiven = false, Vector3 _position = new Vector3())
    {
        if (!positionGiven)
        {
            _position = tile.transform.position;
            selectedTiles.Add(tile);
            tile.gameObject.SetActive(false);
        }

        Rocket rocket_1 = GetRocket(_position);
        Rocket rocket_2 = GetRocket(_position);
        rocket_1.Move(_position, Direction.LEFT);
        rocket_2.Move(_position, Direction.RIGHT);
        SoundManager.PlaySFX("Rocket");
    }

    void HandleBomb(Tile tile)
    {
        Bomb bomb = GridManager.instance.StartBombExplosion(tile.transform.position);
        SoundManager.PlaySFX("Bomb_Small_Explode");

        int column = tile.column;
        int row = tile.row;
        tile.DisableVisual();
        tile.visited_combo = true;
        selectedTiles.Add(tile);

        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                int index_x = column + arr_i[i];
                int index_y = row + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf && !temp_tile.visited_combo)
                        {
                            if (temp_tile.tile_type == Tile.TileType.COMBO)
                            {
                                HandleCombos(temp_tile);
                                DisableTile(temp_tile, bomb: bomb.gameObject);
                            }
                            else
                            {
                                DisableTile(temp_tile, bomb: bomb.gameObject);
                            }
                        }
                    }
                }
            }
        }
        tile.gameObject.SetActive(false);
    }

    void HandleMagnet(Tile tile)
    {
        StartCoroutine(HandleMagnet_routine(tile));
    }

    IEnumerator HandleMagnet_routine(Tile tile)
    {
        SoundManager.PlaySFX("Lightning");
        selectedTiles.Add(tile);
        List<Tile> tiles_added = new List<Tile>();
        for (int i = 0; i < GridManager.instance.gridTiles.Count; i++)
        {
            for (int j = 0; j < GridManager.instance.gridTiles[i].Count; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];
                if (temp_tile != null && temp_tile.gameObject.activeSelf && temp_tile.isActive
                    && (!temp_tile.GetObstacle() || !temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.DESTROYED_WITH_ONLY_COMBO) && temp_tile.GetObstacle().GetFeatureValue(Obstacle.ObstacleFeature.CONTAINS_TILE))
                    && temp_tile.tile_type == Tile.TileType.TILE
                    && temp_tile.tile_color == tile.tile_color)
                {
                    GenerateLightning(tile.transform.position, temp_tile.transform.position);
                    tiles_added.Add(temp_tile);
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
        yield return new WaitForSeconds(0.5f);
        lightningActive = false;

        HandleAllNeighborsOnce(tiles_added, tile.tile_color);
        for (int i = 0; i < tiles_added.Count; i++)
        {
            Tile temp_tile = tiles_added[i];
            GridManager.instance.StartTileExplosion(temp_tile);

            if (!temp_tile.GetObstacle())
            { 
                temp_tile.gameObject.SetActive(false);
                selectedTiles.Add(temp_tile);
            }
        }
        tile.gameObject.SetActive(false);

        total_active_combo--;
        HandleBlast();
    }

    #endregion

}

public struct Indexes
{
    public Indexes(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
    public int x;
    public int y;
}