using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectActivator : MonoBehaviour
{
    public GameObject _gameObject;

    public void EnableObject()
    {
        _gameObject.SetActive(true);
    }

    public void DisableObject()
    {
        _gameObject.SetActive(false);
    }
}
