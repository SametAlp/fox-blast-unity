using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Goal : MonoBehaviour
{
    int count = 0;
    Obstacle.ObstacleType obstacleType;
    Tile.TileColor tileColor;
    public TextMeshProUGUI count_text;
    public Image obstacle_image;
    public GameObject tick_go;



    public int GetValue()
    {
        return count;
    }

    public Obstacle.ObstacleType GetObstacleType()
    {
        return obstacleType;
    }

    public Tile.TileColor GetTileColor()
    {
        return tileColor;
    }

    public void GenerateGoal(Obstacle.ObstacleType obstacleType, int count = 1, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        this.count = count;
        this.obstacleType = obstacleType;
        this.tileColor = tileColor;
        count_text.text = count.ToString();
        if (tileColor != Tile.TileColor.NONE)
        {
            obstacle_image.sprite = GridManager.instance.item_sprites[(int)tileColor];
        }
        else
        {
            obstacle_image.sprite = GridManager.instance.obstacle_goal_sprites[(int)obstacleType];
        }

        if(count == 0)
        {
            count_text.gameObject.SetActive(false);
            tick_go.SetActive(true);
        }
    }

    public void IncrementValue()
    {
        count++;
        count_text.text = count.ToString();
    }

    public void DecrementValue()
    {
        count--;
        count_text.text = count.ToString();
        SetTick();
    }

    public void SetTick()
    {
        if (count <= 0)
        {
            SoundManager.PlaySFX("SurpriseBox_Turn");
            count_text.gameObject.SetActive(false);
            GameManager.instance.add_score(30);
            tick_go.SetActive(true);
        }
    }
}
