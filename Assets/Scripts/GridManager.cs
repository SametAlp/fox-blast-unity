using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Tilemaps;
using System.IO;
using System;
using Spine.Unity;
using GameAnalyticsSDK;

public class GridManager : MonoBehaviour
{
    public static GridManager instance;
    public int ROWS = 9;
    public int COLUMNS = 9;

    public int number_of_color = 4;

    public float GRID_FAR_X = 0.2f;
    public float GRID_FAR_Y = 0.3f;

    public float FIRST_ITEM_OFFSET_X = 0;
    public float FIRST_ITEM_OFFSET_Y = 0;

    public Vector3 first_item_pos;
    public Transform center_item_transform;

    public int total_count_tile;

    public List<List<Tile>> gridTiles;
    public GameObject gridTileGO;

    public List<List<List<Obstacle>>> gridObstacles;

    public List<Sprite> item_sprites;
    public List<Sprite> item_inside_sprites;
    public List<Sprite> combo_sprites;
    public List<Sprite> combo_inside_sprites;
    public List<Sprite> magnet_sprites;
    public List<Sprite> obstacle_sprites;

    public List<Sprite> long_flower_sprites;
    public List<Sprite> short_flower_sprites;

    public List<Sprite> mushroom_sprites;

    public List<Sprite> rock_sprites;
    public List<Sprite> white_rock_sprites;
    public Dictionary<Obstacle.ObstacleType, List<Sprite>> obstacles_with_steps;

    public List<Sprite> obstacle_goal_sprites;

    public List<SkeletonDataAsset> obstacle_skeletonAnimations;

    public GameObject fakeTile_prefab;
    public Transform fakeTile_parent;
    public List<FakeTile> fakeTile_pool;
    public int active_fakeTile = 0;
    public bool settingAllTiles;

    public GameObject tileExplosion_prefab;
    public Transform tileExplosion_parent;
    public List<TileExplosion> tileExplosion_pool;

    public GameObject bomb_prefab;
    public Transform bomb_parent;
    public List<Bomb> bomb_pool;

    public int active_bomb;
    public GameObject bigBomb_prefab;
    public Transform bigBomb_parent;
    public List<Bomb> bigBomb_pool;

    public GameObject gridObstacleGO;
    public Transform obstacle_parent;
    public List<Obstacle> obstacle_pool;

    public GameObject gridLargeObstacleGO;
    public Transform large_obstacle_parent;
    public List<Obstacle> large_obstacle_pool;

    public GameObject comboAnim_prefab;
    public Transform comboAnim_parent;
    public List<GameObject> comboAnim_pool;

    public Tilemap tilemap;
    public Tilemap tilemap_bg;

    public TileBase tileBase;
    public TileBase tileBase_bg;

    public int level;

    private void Awake()
    {
        if(instance == null)
        {
            level = PlayerPrefs.GetInt("level", 1);
            instance = this;
            first_item_pos = new Vector3(center_item_transform.position.x - 4 * GRID_FAR_X, center_item_transform.position.y, 0);
            obstacle_pool = new List<Obstacle>();
            obstacles_with_steps = new Dictionary<Obstacle.ObstacleType, List<Sprite>>();
            obstacles_with_steps.Add(Obstacle.ObstacleType.ROCK, rock_sprites);
            obstacles_with_steps.Add(Obstacle.ObstacleType.WHITE_ROCK, white_rock_sprites);
            BuildGrid();
            SetItemFarValues();
        }
    }

    public void SetItemFarValues()
    {
        FIRST_ITEM_OFFSET_X = first_item_pos.x;
        FIRST_ITEM_OFFSET_Y = first_item_pos.y;
    }

    #region POOL_OBJECTS
    public FakeTile GetFakeTile(Vector3 position)
    {
        if (fakeTile_pool.Count > 0)
        {
            FakeTile temp_fakeTile = fakeTile_pool[0];
            fakeTile_pool.RemoveAt(0);
            temp_fakeTile.gameObject.SetActive(true);
            active_fakeTile++;
            return temp_fakeTile;
        }
        GameObject fakeTile_go = Instantiate(fakeTile_prefab, fakeTile_parent);
        fakeTile_go.transform.position = position;
        active_fakeTile++;
        return fakeTile_go.GetComponent<FakeTile>();
    }

    public GameObject GetComboAnim(Transform tile_parent)
    {
        if (comboAnim_pool.Count > 0)
        {
            GameObject temp_comboAnim_go = comboAnim_pool[0];
            comboAnim_pool.RemoveAt(0);
            temp_comboAnim_go.transform.SetParent(tile_parent);
            temp_comboAnim_go.transform.localPosition = Vector3.zero;
            temp_comboAnim_go.gameObject.SetActive(true);
            return temp_comboAnim_go;
        }
        GameObject comboAnim_go = Instantiate(comboAnim_prefab, tile_parent);
        comboAnim_go.transform.localPosition = Vector3.zero;
        comboAnim_go.SetActive(true);
        return comboAnim_go;
    }

    public void StartTileExplosion(Tile temp_tile)
    {
        Vector3 position = temp_tile.transform.position;
        Texture texture = temp_tile.spriteRenderer.sprite.texture;
        TileExplosion temp_tileExplosion;
        if (tileExplosion_pool.Count > 0)
        {
            temp_tileExplosion = tileExplosion_pool[0];
            tileExplosion_pool.RemoveAt(0);
            temp_tileExplosion.transform.position = position;
            temp_tileExplosion.gameObject.SetActive(true);
        }
        else
        {
            GameObject tileExplosion_go = Instantiate(tileExplosion_prefab, tileExplosion_parent);
            tileExplosion_go.transform.position = position;
            temp_tileExplosion = tileExplosion_go.GetComponent<TileExplosion>();
        }
        temp_tileExplosion.ActivateExplosion(texture);
    }

    public Bomb StartBombExplosion(Vector3 position)
    {
        active_bomb++;
        Bomb temp_bomb;
        if (bomb_pool.Count > 0)
        {
            temp_bomb = bomb_pool[0];
            bomb_pool.RemoveAt(0);
            temp_bomb.transform.position = position;
            temp_bomb.gameObject.SetActive(true);
        }
        else
        {
            GameObject tileExplosion_go = Instantiate(bomb_prefab, bomb_parent);
            tileExplosion_go.transform.position = position;
            temp_bomb = tileExplosion_go.GetComponent<Bomb>();
        }
        temp_bomb.ActivateExplosion();
        return temp_bomb;
    }

    public Bomb StartBigBombExplosion(Vector3 position)
    {
        Bomb temp_bomb;
        if (bigBomb_pool.Count > 0)
        {
            temp_bomb = bigBomb_pool[0];
            bigBomb_pool.RemoveAt(0);
            temp_bomb.transform.position = position;
            temp_bomb.gameObject.SetActive(true);
        }
        else
        {
            GameObject tileExplosion_go = Instantiate(bigBomb_prefab, bigBomb_parent);
            tileExplosion_go.transform.position = position;
            temp_bomb = tileExplosion_go.GetComponent<Bomb>();
        }
        temp_bomb.ActivateExplosion();
        return temp_bomb;
    }
    #endregion

    #region BUILD_GRID

    void read_level(int level_no)
    {
        string file_path = Path.Combine(Application.streamingAssetsPath, "Levels/" + level_no.ToString() + ".txt");
        string reader;
#if UNITY_EDITOR
        reader = System.IO.File.ReadAllText(file_path);
#elif UNITY_ANDROID
            WWW www_reader = new WWW(file_path);
            while (!www_reader.isDone);
            reader = www_reader.text;
#elif UNITY_IOS
        reader = System.IO.File.ReadAllText(file_path);
#endif
        List<string> readtext = reader.Split('\n').ToList();
        int index = 0;
        string readMeText = readtext[index++];
        GameManager.instance.move_count = Int32.Parse(readMeText.Split(' ')[0]);
        GameManager.instance.move_text.text = GameManager.instance.move_count.ToString();
        level = level_no;
        readMeText = readtext[index++];

        if (readMeText.Length == 1)
        {
            int row_number = 0;
            readMeText = readtext[index++];
            while (readMeText.Length > 0 && readMeText[0] != '#')
            {
                string[] line_array = readMeText.Split(' ');
                for (int i = 0; i < line_array.Length; i++)
                {
                    if (line_array[i] == "X")
                    {
                        CreateGenericObstacle(i, row_number, Obstacle.ObstacleType.COLLECTABLE);
                    }
                }
                row_number++;
                readMeText = readtext[index++];
            }
        }
        if (readMeText.Length == 2)
        {
            int row_number = 0;
            readMeText = readtext[index++];
            while (readMeText.Length > 0 && readMeText[0] != '#' && readMeText[0] != ' ')
            {
                string[] line_array = readMeText.Split(' ');
                for (int i = 0; i < line_array.Length; i++)
                {
                    CreateObstacleFromLevelReading(i, row_number, line_array[i]);
                }
                row_number++;
                readMeText = readtext[index++];
            }
        }
        if (readMeText.Length == 3)
        {
            int row_number = 0;
            readMeText = readtext[index++];
            while (readMeText.Length > 0 && readMeText[0] != '#' && readMeText[0] != 'E' && readMeText[1] != 'O' && readMeText[0] != ' ')
            {
                string[] line_array = readMeText.Split(' ');
                for (int i = 0; i < line_array.Length; i++)
                {
                    CreateObstacleFromLevelReading(i, row_number, line_array[i]);
                }
                row_number++;
                readMeText = readtext[index++];
            }
        }
        AddGoalsForNonObstacleLevels();
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, level.ToString());
        GameManager.instance.CheckCombos(true);

    }

    void AddGoalsForNonObstacleLevels()
    {
        if(level == 1)
        {
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.BLUE);
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 5, Tile.TileColor.GREEN);
        }
        else if (level == 2)
        {
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.ORANGE);
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.BLUE);
        }
        else if (level == 3)
        {
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.BLUE);
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.PURPLE);
        }
        else if (level == 4)
        {
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 20, Tile.TileColor.BLUE);
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 5, Tile.TileColor.ORANGE);
        }
        else if (level == 5)
        {
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 20, Tile.TileColor.PURPLE);
            GameManager.instance.IncrementGoal(Obstacle.ObstacleType.EMPTY, 10, Tile.TileColor.BLUE);
        }
    }

    void CreateObstacleFromLevelReading(int column, int row, string obstacle_str)
    {
        char obstacle_char = obstacle_str[0];
        char obstacle_char_2 = '0';

        if (obstacle_str.Length > 1)
        {
            obstacle_char_2 = obstacle_str[1];
        }

        if (obstacle_char == '0')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.ROCK, 3);
        }
        else if (obstacle_char == '1')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.ROCK, 2);
        }
        else if (obstacle_char == '2')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.ROCK, 1);
        }
        else if (obstacle_char == 'W' && obstacle_char_2 == '0')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.WHITE_ROCK, 2);
        }
        else if (obstacle_char == 'W' && (obstacle_char_2 == '1' || obstacle_char_2 == '2'))
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.WHITE_ROCK, 1);
        }
        else if (obstacle_char == 'I')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.ICE);
        }
        else if (obstacle_char == 'L')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.BUBBLE);
        }
        else if (obstacle_char == 'G')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.GRASS);
        }
        else if (obstacle_char == 'S')
        {
            GameManager.instance.spreadSlime = true;
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.SLIME);
        }
        else if (obstacle_char == 'C')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.CHAIN);
        }
        else if (obstacle_char == 'N')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.NET);
        }
        else if (obstacle_char == 'A')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.BEAR);
        }
        else if (obstacle_char == 'M')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.MUSHROOM, tileColor: (Tile.TileColor)Int32.Parse(obstacle_char_2.ToString()));
        }
        else if (obstacle_char == 'F')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.FLOWER_BOX, 4);
        }
        else if (obstacle_char == '*' || obstacle_char == 'B')
        {
            CreateGenericObstacle(column, row, Obstacle.ObstacleType.EMPTY);
        }
        else if (obstacle_char == 'a')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.BLUE);
        }
        else if (obstacle_char == 'b')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.GREEN);
        }
        else if (obstacle_char == 'c')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.ORANGE);
        }
        else if (obstacle_char == 'd')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.PURPLE);
        }
        else if (obstacle_char == 'e')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.RED);
        }
        else if (obstacle_char == 'f')
        {
            gridTiles[column][row].SetTileData(Tile.TileColor.YELLOW);
        }
        else if (obstacle_char == 'g')
        {
            GameManager.instance.create_manual_combo(gridTiles[column][row], Tile.ComboType.HORIZONTAL_ROCKET);
        }
        else if (obstacle_char == 'h')
        {
            GameManager.instance.create_manual_combo(gridTiles[column][row], Tile.ComboType.BOMB);
        }
        else if (obstacle_char == 'i')
        {
            GameManager.instance.create_manual_combo(gridTiles[column][row], Tile.ComboType.MAGNET, (Tile.TileColor)(int)(obstacle_char_2 - 'a'));
        }
    }

    private void BuildGrid()
    {
        number_of_color = level <= 20 ? 5 : 4;

        for (int i = -53; i < 53; i++)
        {
            for (int j = -53; j < 53; j++)
            {
                tilemap.SetTile(new Vector3Int(j, i, 0), tileBase);
            }
        }

        gridObstacles = new List<List<List<Obstacle>>>();
        for (int column = 0; column < COLUMNS; column++)
        {
            var columnObstacles = new List<List<Obstacle>>();

            for (int row = 0; row < ROWS; row++)
            {
                columnObstacles.Add(new List<Obstacle>());
            }
            gridObstacles.Add(columnObstacles);
        }

        gridTiles = new List<List<Tile>>();

        for (int column = 0; column < COLUMNS; column++)
        {
            var columnTiles = new List<Tile>();

            for (int row = 0; row < ROWS; row++)
            {
                var item = Instantiate(gridTileGO);
                var tile = item.GetComponent<Tile>();

                item.name = "Tile X:" + column.ToString() + " Y:" + row.ToString();
                tile.SetTilePosition(column, row);
                tile.transform.SetParent(this.transform);
                tile.transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y, 0);

                int rand = UnityEngine.Random.Range(0, number_of_color);
                tile.SetTileData((Tile.TileColor)rand);

                columnTiles.Add(tile);
                tilemap.SetTile(new Vector3Int(row, column, 0), null);
                tilemap_bg.SetTile(new Vector3Int(row, column, 0), tileBase_bg);

            }
            gridTiles.Add(columnTiles);
        }

        read_level(level);
        set_pre_combos();
    }

    public void set_pre_combos()
    {
        for (int i = 0; i < 3; i++)
        {
            if (PlayerPrefs.GetInt("combo_start_" + i.ToString(), 0) == 1)
            {
                GameAnalytics.NewDesignEvent("PreCombo:" + i.ToString());
                create_combo_item(i);
                PlayerPrefs.SetInt("combo_start_" + i.ToString(), 0);

            }
        }
    }

    List<Tile> finished_tiles;
    void create_combo_item(int index)
    {
        StartCoroutine(create_combo_item_routine(index));
    }

    IEnumerator create_combo_item_routine(int index)
    {
        yield return new WaitForSeconds(0.5f + 0.1f * index);

        List<int> number_list = new List<int>();
        number_list = Enumerable.Range(0, ROWS * COLUMNS - 1).ToList().OrderBy(a => Guid.NewGuid()).ToList();
        int counter = 0;

        int random_index_x = number_list[counter] % ROWS;
        int random_index_y = number_list[counter] / COLUMNS;
        Tile temp_tile = gridTiles[random_index_x][random_index_y];
        while (!temp_tile.gameObject.activeSelf || temp_tile.tile_type == Tile.TileType.COMBO ||
            temp_tile.GetObstacle() && !temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
        {
            counter++;
            if (counter >= number_list.Count)
            {
                break;
            }
            random_index_x = number_list[counter] % ROWS;
            random_index_y = number_list[counter] / COLUMNS;
            temp_tile = gridTiles[random_index_x][random_index_y];
        }
        if (counter >= number_list.Count)
        {
            yield break;
        }

        Tile combo_tile = gridTiles[random_index_x][random_index_y];
        if (index == 0)
        {
            GameManager.instance.create_manual_combo(combo_tile, Tile.ComboType.HORIZONTAL_ROCKET);
        }
        else if (index == 1)
        {
            GameManager.instance.create_manual_combo(combo_tile, Tile.ComboType.BOMB);
        }
        else if (index == 2)
        {
            GameManager.instance.create_manual_combo(combo_tile, Tile.ComboType.MAGNET);
        }
        SoundManager.PlaySFX("button_sound");
        PlayerPrefs.SetInt("combo_start_count_" + index.ToString(), PlayerPrefs.GetInt("combo_start_count_" + index.ToString(), 3) - 1);
        GameManager.instance.CheckCombos(true);
    }

    public void CreateGenericObstacle(int column, int row, Obstacle.ObstacleType obstacleType, int step_no = 1, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        GameManager.instance.IncrementGoal(obstacleType);
        if (obstacleType == Obstacle.ObstacleType.FLOWER_BOX)
        {
            CreateLargeObstacle(column, row, obstacleType, step_no);
        }
        else
        {
            CreateObstacle(column, row, obstacleType, step_no, tileColor);
        }
    }

    void CreateObstacle(int column, int row, Obstacle.ObstacleType obstacleType, int step_no = 1, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        if (obstacleType == Obstacle.ObstacleType.EMPTY)
        {
            tilemap.SetTile(new Vector3Int(column, row, 0), tileBase);
            tilemap_bg.SetTile(new Vector3Int(column, row, 0), null);
        }
        Obstacle obstacle;

        GameObject obstacle_go = Instantiate(gridObstacleGO);
        obstacle_go.transform.SetParent(obstacle_parent);
        obstacle = obstacle_go.GetComponent<Obstacle>();
        
        obstacle.gameObject.SetActive(true);
        obstacle.SetObstacleData(obstacleType, step_no, tileColor);
        Tile tile = gridTiles[column][row];
        tile.isActive = true;
        obstacle.transform.position = tile.transform.position;
        SetTileValuesForObstacle(obstacle, tile, column, row);
    }

    void SetTileValuesForObstacle(Obstacle obstacle, Tile tile, int column, int row)
    {
        if(obstacle.obstacleType == Obstacle.ObstacleType.EMPTY)
        {
            tile.isActive = false;
            tile.emptyTile = true;
            tile.DisableVisual();
        }
        if (obstacle.GetFeatureValue(Obstacle.ObstacleFeature.MOVABLE))
        {
            obstacle.transform.SetParent(tile.transform);
        }
        if (!obstacle.GetFeatureValue(Obstacle.ObstacleFeature.CONTAINS_TILE))
        {
            tile.DisableVisual();
        }
        gridObstacles[column][row].Add(obstacle);
        UpdateObstacleList(gridObstacles[column][row], column, row);
    }

    void CreateLargeObstacle(int column, int row, Obstacle.ObstacleType obstacleType, int step_no = 1)
    {
        Obstacle obstacle;

        GameObject obstacle_go = Instantiate(gridLargeObstacleGO);
        obstacle_go.transform.SetParent(large_obstacle_parent);
        obstacle = obstacle_go.GetComponent<Obstacle>();
        

        obstacle.gameObject.SetActive(true);
        obstacle.SetObstacleData(obstacleType, step_no);
        obstacle.tilesContained = new List<Tile>();

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                Tile temp_tile = gridTiles[column + i][row + j];
                SetTileValuesForObstacle(obstacle, temp_tile, column + i, row + j);

                if(i == 0 && j == 0)
                {
                    obstacle.transform.position = temp_tile.transform.position;
                    obstacle.master_tile = temp_tile;
                }
                obstacle.tilesContained.Add(temp_tile);
            }
        }
    }
    

    public void UpdateObstacleList(List<Obstacle> obstacles, int column, int row)
    {
        gridObstacles[column][row] = obstacles.OrderBy((e) => e.sorting_order).ToList();
    }
    #endregion

    #region COLLAPSING_GRID
    public void CollapseGrid(float extra_delay = 0, bool dontAllowSpreadSlime = false)
    {
        settingAllTiles = true;
        StartCoroutine(CollapseGrid_routine(extra_delay, dontAllowSpreadSlime));
    }

    int GetNextNullIndex(List<Tile> tiles)
    {
        for(int i = tiles.Count - 1; i >= 0; i--)
        {
            if(tiles[i] == null) 
            {
                return i;
            }
        }
        return 0;
    }

    int GetNearestNull(List<Tile> tiles, int index)
    {
        for (int i = index; i >= 0; i--)
        {
            if (tiles[i] == null)
            {
                return i <= 0 ? 0 : i;
            }
        }
        return 0;
    }

    IEnumerator CollapseGrid_routine(float extra_delay = 0, bool dontAllowSpreadSlime = false)
    {
        yield return new WaitForSeconds(extra_delay);
        while (GameManager.instance.lightningActive || GameManager.instance.active_rockets > 0 || active_fakeTile > 0 || active_bomb > 0)
        {
            yield return null;
        }
        for (int column = 0; column < COLUMNS; column++)
        {
            var columnList = gridTiles[column];
            var newColumn = new List<Tile>() { null, null, null, null, null, null, null, null, null};
            var row = ROWS - 1;
            var totalRemoved = 0;

            for (var i = columnList.Count - 1; i >= 0; i--)
            {
                if (columnList[i].gameObject.activeSelf && columnList[i].isActive)
                {
                    if (!columnList[i].CheckNewPositionAvailabilityUnder(row))
                    {
                        row = columnList[i].row;
                        newColumn[row] = columnList[i];
                        row--;
                        continue;
                    }
                    if (newColumn[row] != null)
                    {
                        row = GetNearestNull(newColumn, row);
                    }
                    newColumn[row] = columnList[i];
                    columnList[i].UpdateObstaclePosition(row);
                    columnList[i].row = row;
                    row--;
                }
                else if(columnList[i].emptyTile)
                {
                    newColumn[columnList[i].row] = columnList[i];
                }
                //DebugColumn(newColumn, column);

            }

            //DebugColumn(newColumn, column);

            totalRemoved = newColumn.Count((e) => e == null);
            int empty_row = 0;
            int removed_tile_counter = ROWS;
            for (var i = columnList.Count - 1; i >= 0; i--)
            {
                if (!columnList[i].emptyTile && (!columnList[i].gameObject.activeSelf || !columnList[i].isActive))
                {
                    removed_tile_counter--;
                    columnList[i].row = GetNextNullIndex(newColumn);
                    if (!columnList[i].CheckNewPositionAvailabilityUpper())
                    {
                        empty_row = GetNextNullIndex(newColumn);
                        newColumn[empty_row] = columnList[i];
                        columnList[i].isActive = false;
                        columnList[i].DisableVisual();
                        columnList[i].gameObject.SetActive(true);
                        continue;
                    }
                    columnList[i].isActive = true;
                    columnList[i].EnableVisual();
                    
                    var p = columnList[i].transform.position;
                    p.y = columnList[0].transform.position.y + removed_tile_counter * (GRID_FAR_Y) + 7f;
                    columnList[i].transform.position = p;
                    empty_row = GetNextNullIndex(newColumn);
                    newColumn[empty_row] = columnList[i];

                    int rand = UnityEngine.Random.Range(0, number_of_color);
                    columnList[i].SetTileData((Tile.TileColor)rand);
                }
            }
            //DebugColumn(newColumn, column);

            gridTiles[column] = newColumn;
        }
        settingAllTiles = false;

        for (int i = 0; i < gridTiles.Count; i++)
        {
            for (int j = 0; j < gridTiles.Count; j++)
            {
                gridTiles[i][j].UpdateData();
            }
        }
        GameManager.instance.CheckCombos(dontAllowSpreadSlime);

        if(level < 5 && GameManager.instance.isTutorial && Tutorial.instance.step == 0)
        {
            Tutorial.instance.SetTutorialPopUpSecond();
        }
        Tutorial.instance.step++;
        if (level < 5 && GameManager.instance.isTutorial && Tutorial.instance.step == 2)
        {
            Tutorial.instance.EndOfTutorial();
        }
        if ((level == 10 || level == 13 || level == 17 || level == 20) && GameManager.instance.isTutorial && Tutorial.instance.step == 1)
        {
            Tutorial.instance.EndOfTutorial();
        }

        yield return new WaitForSeconds(1);
        GameManager.instance.CheckWinLost();
    }

    public void DebugObstacles()
    {
        Debug.Log("OBSTACLES:");
        for (int i = 0; i < gridObstacles.Count; i++)
        {
            string row = "";
            for (int j = 0; j < gridObstacles[i].Count; j++)
            {
                if (gridObstacles[j][i].Count > 0)
                {
                    row += "(";
                    for (int k = 0; k < gridObstacles[j][i].Count; k++)
                    {
                        
                        row += gridObstacles[j][i][k].obstacleType + " ";
                    }
                    row += ")";
                }
                else
                {
                    row += "(_) ";
                }
            }
            Debug.Log(row);
        }
    }

    public void DebugTiles()
    {
        Debug.Log("TILES:");
        for (int i = 0; i < gridTiles.Count; i++)
        {
            string row = "";
            for (int j = 0; j < gridTiles[i].Count; j++)
            {
                row += gridTiles[j][i].name + " ";
            }
            Debug.Log(row);
        }

    }

    void DebugColumn(List<Tile> columnTiles, int col_no)
    {
        if (col_no > -1)
        {
            for (int i = 0; i < columnTiles.Count; i++)
            {
                Debug.Log("COLUMN: " + col_no + " ROW: " + i + " " + (columnTiles[i] == null ? "null" : columnTiles[i].name));
            }
        }
    }

    #endregion
}
