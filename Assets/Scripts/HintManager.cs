using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintManager : MonoBehaviour
{
    public static HintManager instance;
    public int COLUMNS;
    public int ROWS;

    Tile CheckTile;
    int length = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        COLUMNS = GridManager.instance.COLUMNS;
        ROWS = GridManager.instance.ROWS;
    }

    public void CheckTilesPlayable()
    {
        for (int i = 0; i < COLUMNS; i++)
        {
            for (int j = 0; j < ROWS; j++)
            {
                length = 0;
                GameManager.instance.ResetVisits();
                CheckTile = GridManager.instance.gridTiles[i][j];
                if (recursive_distribution_combo_preview(i, j))
                {
                    return;
                }
            }
        }
        Shuffle();
        CheckTilesPlayable();
    }

    public void Shuffle()
    {
        GameManager.instance.ResetVisits();
        for (int i = 0; i < COLUMNS; i++)
        {
            for (int j = 0; j < ROWS; j++)
            {
                Tile temp_tile = GridManager.instance.gridTiles[i][j];
                if (temp_tile.gameObject.activeSelf
                    && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle()
                    && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
                    )
                {
                    if (!temp_tile.visited_combo && temp_tile.isActive)
                    {
                        int rand = UnityEngine.Random.Range(0, GridManager.instance.number_of_color);
                        temp_tile.SetTileData((Tile.TileColor)rand);
                    }
                }
            }
        }
    }

    public bool recursive_distribution_combo_preview(int x, int y)
    {
        bool canValid = false;
        Tile temp_tile = GridManager.instance.gridTiles[x][y];
        if (temp_tile.gameObject.activeSelf
            && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle() && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
            && temp_tile.tile_color == CheckTile.tile_color)
        {
            if (!temp_tile.visited_combo && temp_tile.isActive)
            {
                temp_tile.visited_combo = true;
                canValid = true;
            }
        }
        if(!canValid)
        {
            return false;
        }

        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf
                            && (!temp_tile.GetObstacle() && temp_tile.tile_type == Tile.TileType.TILE || temp_tile.tile_type == Tile.TileType.TILE && temp_tile.GetObstacle() && temp_tile.GetObstaclesFeature(Obstacle.ObstacleFeature.CAN_BE_USED))
                            && temp_tile.tile_color == CheckTile.tile_color)
                        {
                            if (!temp_tile.visited_combo && temp_tile.isActive)
                            {
                                temp_tile.visited_combo = true;
                                length++;
                                if(length >= 1)
                                {
                                    return true;
                                }
                                recursive_distribution_combo_preview(index_x, index_y);
                                length--;
                            }
                        }
                    }
                }
            }
        }
        return length >= 1;
    }
}
