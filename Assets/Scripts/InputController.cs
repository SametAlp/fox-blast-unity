using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
	void Update()
	{
		try
		{
			if(PopUps.instance.panel.gameObject.activeSelf && CastingCanvasObject())
            {
				return;
            }
			if (Input.touches.Length > 0)
			{
				Touch touch = Input.touches[0];

				if (touch.phase == TouchPhase.Ended)
				{
					GameManager.instance.HandleBlast(touch.position);
				}
				return;
			}
			else if (Input.GetMouseButtonUp(0))
			{
				GameManager.instance.HandleBlast(Input.mousePosition);
			}
		}
		catch
		{

		}
	}

	public GraphicRaycaster m_Raycaster;
	public EventSystem m_EventSystem;
	PointerEventData m_PointerEventData;
	bool CastingCanvasObject()
	{
		m_PointerEventData = new PointerEventData(m_EventSystem);
		m_PointerEventData.position = Input.mousePosition;
		List<RaycastResult> results = new List<RaycastResult>();
		m_Raycaster.Raycast(m_PointerEventData, results);
		return results.Count != 0;
	}
}