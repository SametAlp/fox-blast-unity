using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.LightningBolt;

public class LightningBolt : MonoBehaviour
{
    public LightningBoltScript lightningBoltScript;
    public LineRenderer lineRenderer;

    public void ActivateExplosion(Vector3 startPosition, Vector3 endPosition)
    {
        lineRenderer.positionCount = 0;
        lightningBoltScript.StartPosition = startPosition;
        lightningBoltScript.EndPosition = endPosition;
        gameObject.SetActive(false);
        gameObject.SetActive(true);
        StartCoroutine(DeactivateExplosion());
    }

    IEnumerator DeactivateExplosion()
    {
        while(GameManager.instance.lightningActive)
        {
            yield return null;
        }
        GameManager.instance.lightning_pool.Add(this);
        gameObject.SetActive(false);
    }
}
