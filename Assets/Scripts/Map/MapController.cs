using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public Values values;
    public Image current_bg;
    public List<MapItem> mapItems;
    public Scrollbar vertical_scrollBar;

    private void OnEnable()
    {
        int current_level_no = 1;
        int players_level = PlayerPrefs.GetInt("level", 1);
        int level_of_a_map;
        for(int i = 0; i < mapItems.Count; i++)
        {
            level_of_a_map = values.total_level_of_a_map[i];
            mapItems[i].SetLevelText(current_level_no, level_of_a_map);

            if(current_level_no <= players_level && players_level <= level_of_a_map)
            {
                float value = (float)i / mapItems.Count;
                StartCoroutine(MapScrollBarSet(value));
                current_bg.sprite = values.map_bgs[i];
            }

            current_level_no = level_of_a_map + 1;
        }
    }

    IEnumerator MapScrollBarSet(float value)
    {
        yield return null;
        vertical_scrollBar.value = value;
    }
}
