using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MapItem : MonoBehaviour
{
    public TextMeshProUGUI level_text;

    public void SetLevelText(int first_level, int last_level)
    {
        level_text.text = "Levels: " + first_level.ToString() + "-" + last_level.ToString();
    }
}
