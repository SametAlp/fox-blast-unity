using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using GameAnalyticsSDK;
using System;

public class Menu : MonoBehaviour
{
    public static Menu instance;
    public Values values;

    public int coins;
    public TextMeshProUGUI play_button_level_text;
    public TextMeshProUGUI level_text;
    public TextMeshProUGUI coin_text;
    public TextMeshProUGUI coin_combo_text;
    public TextMeshProUGUI coin_life_text;
    public Image panel;
    public float duration = 0.5f;

    public GameObject enter_panel;
    public GameObject life_panel;
    public GameObject map_panel;

    public Transform gold_transform;
    public Transform play_button_transform;
    public GameObject next_level_animation;

    public int level;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        level = PlayerPrefs.GetInt("level", 1);
        SetBg();
        Application.targetFrameRate = 60;
        coins_to_k();
        NextLevelCheck();
    }

    public Image current_bg;
    void SetBg()
    {
        int current_level_no = 1;
        int level_of_a_map;
        for (int i = 0; i < values.total_level_of_a_map.Count; i++)
        {
            level_of_a_map = values.total_level_of_a_map[i];

            if (current_level_no <= level && level <= level_of_a_map)
            {
                int index = i;
                current_bg.sprite = values.map_bgs[index];
            }

            current_level_no = level_of_a_map;
        }
    }

    private void Update()
    {
        check_free_energy();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (map_panel.activeSelf)
            {
                close_map_panel();
            }
        }
    }

    void check_free_energy()
    {
        if (energy_date_control("energy_hour_date", "energy_hour"))
        {
            enable_energy_icon(energy_date_control_text("energy_hour_date", "energy_hour"));
        }
        else
        {
            disable_energy();
        }
    }

    public GameObject energy_icon;

    public void enable_energy_icon(string s)
    {
        EnergyLoader.instance.infiniteOn = true;
        EnergyLoader.instance.remaining_time_text.text = s;
        EnergyLoader.instance.energy_value_text.text = "";
        energy_icon.SetActive(true);
    }

    public void disable_energy()
    {
        energy_icon.SetActive(false);
        EnergyLoader.instance.infiniteOn = false;
    }

    public bool energy_date_control(string date_str, string date_hour_str)
    {
        String savedDate = PlayerPrefs.GetString(date_str, "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString(date_str, savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        // calculate days ellapsed since launch date:
        double hours = (now - launchDate).TotalHours;
        long secs = (now - launchDate).Seconds;
        int dedicated_hour = PlayerPrefs.GetInt(date_hour_str, 0);

        if (hours < dedicated_hour && hours >= 0 && secs >= 0)
        {
            return true;
        }
        return false;
    }

    public string energy_date_control_text(string date_str, string date_hour_str)
    {
        String savedDate = PlayerPrefs.GetString(date_str, "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString(date_str, savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        // calculate days ellapsed since launch date:
        int dedicated_hour = PlayerPrefs.GetInt(date_hour_str, 0);
        launchDate = launchDate.AddHours(dedicated_hour);
        double hour = (int)(launchDate - now).TotalHours;
        long min = (launchDate - now).Minutes;
        long sec = (launchDate - now).Seconds;

        string hour_str = hour < 10 ? "0" + hour.ToString() : hour.ToString();
        string min_str = min < 10 ? "0" + min.ToString() : min.ToString();
        string sec_str = sec < 10 ? "0" + sec.ToString() : sec.ToString();

        string time = hour_str + ":" + min_str + ":" + sec_str;
        return time.ToString();
    }

    void NextLevelCheck()
    {
        StartCoroutine(NextLevelCheck_routine());
    }

    IEnumerator NextLevelCheck_routine()
    {
        if (PlayerPrefs.GetInt("next_level_popup", 0) == 1)
        {
            PlayerPrefs.SetInt("next_level_popup", 0);
            play_button_level_text.text = "Level " + (level - 1).ToString();
            yield return new WaitForSeconds(1f);
            play_button_transform.DOPunchScale(1.02f * Vector3.one, 0.8f, 5).OnComplete(() => { SoundManager.PlaySFX("Particles"); play_button_transform.DOScale(1, 0.7f).SetDelay(0.1f); });
            play_button_level_text.text = "Level " + level.ToString();
            next_level_animation.SetActive(false);
            next_level_animation.SetActive(true);
            SoundManager.PlaySFX("upgrade");
        }
        else
        {
            play_button_level_text.text = "Level " + level.ToString();
        }
    }

    public void LoadGameScene()
    {
        SceneInfo.CrossSceneInformation = "Game";
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
    }

    Tween panel_tween;
    public void panel_on(float delay = 0)
    {
        panel.gameObject.SetActive(true);
        panel.DOFade(0, 0f);
        if (panel_tween != null && panel_tween.IsActive())
        {
            panel_tween.Kill();
        }
        panel_tween = panel.DOFade(0.7f, 0.5f).SetDelay(delay);
        panel.raycastTarget = true;
    }

    public void panel_off()
    {
        panel.raycastTarget = false;
        if (panel_tween != null && panel_tween.IsActive())
        {
            panel_tween.Kill();
        }
        panel_tween = panel.DOFade(0, 0.5f).OnComplete(() => panel.gameObject.SetActive(false));
    }

    public void open_enter_panel()
    {
        level_text.text = "Level " + level.ToString();
        open_panel(enter_panel);
        combostarter_initialize();
    }

    public void close_enter_panel()
    {
        close_panel(enter_panel);
    }

    public void open_life_panel()
    {
        open_panel(life_panel);
    }

    public void close_life_panel()
    {
        close_panel(life_panel);
    }

    public void open_map_panel()
    {
        map_panel.SetActive(true);
    }

    public void close_map_panel()
    {
        map_panel.SetActive(false);
    }

    public GameObject coinspopup_prefab;
    public Transform coinspopup_parent;

    public List<CoinPopUps> coin_pop_up_list;

    public void get_coins_animation()
    {
        get_coins_animation(null);
    }

    public void get_coins_animation(Transform transform)
    {
        int count = 0;
        CoinPopUps coin_pop_up = null;
        while (count < coin_pop_up_list.Count)
        {
            if (!coin_pop_up_list[count].isRunning)
            {
                coin_pop_up = coin_pop_up_list[count];
                break;
            }
            count++;
        }

        if (coin_pop_up == null)
        {
            coin_pop_up = GameObject.Instantiate(coinspopup_prefab, coinspopup_parent).GetComponent<CoinPopUps>();
        }
        coin_pop_up.Create(new Vector3(0, 0, 0), null, 0.6f, 15);
    }

    void open_panel(GameObject go)
    {
        panel_on();
        go.transform.localScale = Vector3.zero;
        go.SetActive(true);
        go.transform.DOScale(Vector3.one, duration).SetEase(Ease.OutBack);
        SoundMusicManager.instance.set_click_sound();
    }

    void close_panel(GameObject go)
    {
        panel_off();
        go.transform.DOScale(Vector3.zero, duration).SetEase(Ease.InBack).OnComplete(()=> go.SetActive(false));
    }


    public List<Sprite> combo_sprites;
    public GameObject combo_buy_panel;
    public Image combo_buy_icon;
    int current_combo_index = 0;

    public List<ComboStarter> comboStarters;
    public void open_combo_panel(int index)
    {
        current_combo_index = index;
        combo_buy_panel.SetActive(true);
        combo_buy_icon.sprite = combo_sprites[current_combo_index];
        combo_buy_panel.SetActive(true);
        SoundMusicManager.instance.set_click_sound();
    }

    public void coins_to_k()
    {
        coins = PlayerPrefs.GetInt("coins", 500);

        coin_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        coin_combo_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        coin_life_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));
    }

    public void buy_combo()
    {
        coins_to_k();
        if (coins >= 200)
        {
            PlayerPrefs.SetInt("combo_start_count_" + current_combo_index.ToString(), PlayerPrefs.GetInt("combo_start_count_" + current_combo_index.ToString(), 3) + 1);
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) - 200);
            coins_to_k();
            combostarter_initialize(current_combo_index);
        }
        else
        {
            Store.instance.open_shop();
        }
        
        close_combo_panel();
    }

    public void close_combo_panel()
    {
        combo_buy_panel.SetActive(false);
    }

    public void combostarter_initialize(int current_combo_index = -1)
    {
        for (int i = 0; i < comboStarters.Count; i++)
        {
            comboStarters[i].initialize(true);
        }
        if(current_combo_index != -1)
        {
            comboStarters[current_combo_index].start_with_combos();
        }
    }

    public void close_and_go_shop()
    {
        close_combo_panel();
        close_enter_panel();
        close_life_panel();
        ScrollValueChanger.instance.set_menu_sub(0);
    }

    public TextMeshProUGUI level_select_text;
    public void SetLevelByHand()
    {
        string selected_level_text = level_select_text.text.Remove(level_select_text.text.Length - 1, 1);
        try
        {
            PlayerPrefs.SetInt("coins", 100000);
            PlayerPrefs.SetInt("level", int.Parse(selected_level_text));
            Debug.Log("NEW LEVEL SET: " + int.Parse(selected_level_text));
        }
        catch
        {

        }
    }
}
