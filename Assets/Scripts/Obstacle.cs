using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Obstacle : MonoBehaviour
{
    public enum ObstacleType
    {
        ROCK,
        BUBBLE,
        ICE,
        CHAIN,
        NET,
        BEAR,
        SLIME,
        WHITE_ROCK,
        GRASS,
        COLLECTABLE,
        FLOWER_BOX,
        MUSHROOM,
        EMPTY
    };

    public enum ObstacleFeature
    {
        MOVABLE,
        CONTAINS_TILE,
        CAN_BE_PASSED_OVER,
        WITH_STEPS,
        ANIMATED,
        CAN_BE_USED,
        LARGE,
        COLOR_SENSITIVE,
        ONE_DECREMENT_WHEN_PASSED,
        DESTROYED_WITH_ONLY_COMBO,
        CANNOT_BE_DESTROYED
    };

    public bool hasSpine = false;
    public SkeletonAnimation skeletonAnimation;
    public GameObject skeletonAnimation_go;

    public bool isDecremented = false;
    public List<GameObject> rocket_passed_list;
    public List<GameObject> bomb_passed_list;

    public SpriteRenderer spriteRenderer;
    public GameObject spriteRenderer_go;
    public int sorting_order = 3;
    public int step_no = 1;
    public int old_step_no = -1;

    public ObstacleType obstacleType;

    List<bool> booleans = new List<bool> { false, false, false, false, false, false, false, false, false, false, false };

    public Tile master_tile;
    public List<Tile> tilesContained;
    public List<GameObject> objects_inside_go;
    public List<SpriteRenderer> objects_inside_sprite_renderers;
    List<Tile.TileColor> colors_inside;

    public void SetObstacleData(ObstacleType _obstacleType, int _step_no = 1, Tile.TileColor tileColor = Tile.TileColor.NONE)
    {
        obstacleType = _obstacleType;
        if (obstacleType != ObstacleType.FLOWER_BOX && obstacleType != ObstacleType.MUSHROOM && obstacleType != ObstacleType.EMPTY)
        {
            spriteRenderer.sprite = GridManager.instance.obstacle_sprites[(int)obstacleType];
        }
        if (obstacleType != ObstacleType.EMPTY)
        {
            spriteRenderer_go.SetActive(true);
        }
        else
        {
            spriteRenderer_go.SetActive(false);
        }
        SetScale();
        SetFeatures();
        SetSortingOrder();
        SetVisualSortingOrder();
        step_no = _step_no;
        GameManager.instance.total_value_star_bar += 50 * step_no;
        SetSteps();
        if (GetFeatureValue(ObstacleFeature.WITH_STEPS))
        {
            SetNewStepSprite();
        }
        if(obstacleType == ObstacleType.FLOWER_BOX)
        {
            SetFlowers();
        }
        if (obstacleType == ObstacleType.MUSHROOM)
        {
            SetMushroom(tileColor);
        }
        if(obstacleType == ObstacleType.SLIME)
        {
            SetSkeletonAnimation();
        }
    }

    public void SetSteps()
    {
        if(obstacleType == ObstacleType.BEAR)
        {
            step_no = 3;
        }
    }

    public void SetNewStepSprite()
    {
        if (!GetFeatureValue(ObstacleFeature.ANIMATED) && !GetFeatureValue(ObstacleFeature.LARGE) && obstacleType != ObstacleType.EMPTY)
        {
            List<Sprite> sprites;
            GridManager.instance.obstacles_with_steps.TryGetValue(obstacleType, out sprites);
            spriteRenderer.sprite = sprites[step_no - 1];
        }
        else
        {
            SetSkeletonAnimation();
            DecrementAnimatedObstacle();
        }
    }

    public void DecrementAnimatedObstacle()
    {
        if(obstacleType == ObstacleType.BEAR)
        {
            if (step_no == 3)
            {
                skeletonAnimation.AnimationState.SetAnimation(0, "1", false);
            }
            else if (step_no == 2)
            {
                skeletonAnimation.AnimationState.SetAnimation(0, "2", false);
            }
            else if (step_no == 1)
            {
                skeletonAnimation.AnimationState.SetAnimation(0, "3", false);
            }
            else if (step_no == 0)
            {
                skeletonAnimation.AnimationState.SetAnimation(0, "4", false);
            }
            if (old_step_no == -1 && step_no == 2)
            {
                SoundManager.PlaySFX("Bearbox_Open");
            }
            else if (old_step_no == 2 && step_no == 1)
            {
                SoundManager.PlaySFX("Bearbox_Grow");
            }
            else if (old_step_no == 1 && step_no == 0)
            {
                SoundManager.PlaySFX("Bearbox_Start_Collect");
            }
        }

    }

    public void SetSkeletonAnimation()
    {
        if(!hasSpine)
        {
            return;
        }
        spriteRenderer_go.SetActive(false);
        skeletonAnimation_go.SetActive(true);

        skeletonAnimation.ClearState();
        if (obstacleType == ObstacleType.SLIME)
        {
            skeletonAnimation_go.transform.localPosition = new Vector3(0, -0.066f, 0);
            skeletonAnimation_go.transform.localScale = Vector3.one * 0.18f;
            skeletonAnimation.skeletonDataAsset = GridManager.instance.obstacle_skeletonAnimations[0];
            skeletonAnimation.Initialize(true);
            skeletonAnimation.loop = true;
            skeletonAnimation.AnimationState.SetAnimation(0, "animation_01", false).Complete += delegate
            {
                skeletonAnimation.AnimationState.SetAnimation(0, "animation_02", true);
            };
        }
        else if (obstacleType == ObstacleType.BEAR)
        {
            skeletonAnimation_go.transform.localPosition = new Vector3(-0.015f, 0, 0);
            skeletonAnimation_go.transform.localScale = Vector3.one * 0.21f;
            skeletonAnimation.skeletonDataAsset = GridManager.instance.obstacle_skeletonAnimations[1];
            skeletonAnimation.Initialize(true);
            skeletonAnimation.loop = false;
            skeletonAnimation.AnimationState.SetAnimation(0, "1", false);
        }
    }

    public void SetSortingOrder()
    {
        if (obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.ICE
            || obstacleType == ObstacleType.CHAIN
            || obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.SLIME
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.MUSHROOM
            || obstacleType == ObstacleType.WHITE_ROCK)
        {
            sorting_order = 3;
        }
        else if (obstacleType == ObstacleType.BUBBLE)
        {
            sorting_order = 4;
        }
        else if (obstacleType == ObstacleType.GRASS)
        {
            sorting_order = 2;
        }
    }

    public void SetScale()
    {
        if (obstacleType == ObstacleType.BUBBLE || obstacleType == ObstacleType.NET)
        {
            this.transform.localScale = Vector3.one * 1.1f;
        }
        else if (obstacleType == ObstacleType.GRASS)
        {
            this.transform.localScale = Vector3.one * 1.2f;
        }
        else
        {
            this.transform.localScale = Vector3.one;
        }
    }

    public void SetVisualSortingOrder()
    {
        int _sorting_order = 20;
        if (obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.ICE
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.MUSHROOM
            || obstacleType == ObstacleType.WHITE_ROCK)
        {
           _sorting_order = 23;
        }
        else if (obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.SLIME)
        {
            _sorting_order = 24;
        }
        else if (obstacleType == ObstacleType.NET)
        {
            _sorting_order = 25;
        }
        else if (obstacleType == ObstacleType.BUBBLE
            || obstacleType == ObstacleType.CHAIN
            || obstacleType == ObstacleType.GRASS)
        {
            _sorting_order = 26;
        }
        spriteRenderer.sortingOrder = _sorting_order;
        if(skeletonAnimation_go != null)
            skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder = _sorting_order;
    }

    public void SetMushroom(Tile.TileColor tileColor)
    {
        int tileColor_integer = (int)tileColor;
        if(GridManager.instance.number_of_color <= tileColor_integer)
        {
            tileColor = (Tile.TileColor)(GridManager.instance.number_of_color - 1);
        }
        colors_inside = new List<Tile.TileColor>();
        colors_inside.Add(tileColor);
        spriteRenderer.sprite = GridManager.instance.mushroom_sprites[(int)tileColor];
    }

    public void SetFlowers()
    {
        colors_inside = new List<Tile.TileColor>();
        for(int i = 0; i < objects_inside_sprite_renderers.Count; i++)
        {
            colors_inside.Add((Tile.TileColor)i);
            if (i <= 1)
            {
                objects_inside_sprite_renderers[i].sprite = GridManager.instance.long_flower_sprites[i];
            }
            else
            {
                objects_inside_sprite_renderers[i].sprite = GridManager.instance.short_flower_sprites[i];
            }
        }
    }

    public void DecrementLargeObstacleColor(Tile.TileColor tileColor)
    {
        if(step_no == 0)
        {
            return;
        }
        bool hasColor = colors_inside.Contains(tileColor) || tileColor == Tile.TileColor.JOKER;

        if(hasColor)
        {
            GameManager.instance.add_score(20);
            step_no--;
            int index_of_color = 0;
            if (tileColor != Tile.TileColor.JOKER)
            {
                index_of_color = colors_inside.IndexOf(tileColor);
            }
            objects_inside_go[index_of_color].SetActive(false);
            objects_inside_go.RemoveAt(index_of_color);
            colors_inside.RemoveAt(index_of_color);
        }
    }

    public void DecrementObstacleColor(Tile.TileColor tileColor)
    {
        if (step_no == 0)
        {
            return;
        }
        bool hasColor = colors_inside.Contains(tileColor) || tileColor == Tile.TileColor.JOKER;

        if (hasColor)
        {
            GameManager.instance.add_score(20);
            step_no--;
            colors_inside.RemoveAt(0);
        }
    }

    public bool GetFeatureValue(ObstacleFeature obstacleFeature)
    {
        return booleans[(int)obstacleFeature];
    }

    public void SetFeatures()
    {
        if(obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.ICE
            || obstacleType == ObstacleType.CHAIN
            || obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.SLIME
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.WHITE_ROCK)
        {
            booleans[(int)ObstacleFeature.CAN_BE_PASSED_OVER] = false;
        }
        else
        {
            booleans[(int)ObstacleFeature.CAN_BE_PASSED_OVER] = true;
        }

        if (obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.SLIME
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.MUSHROOM
            || obstacleType == ObstacleType.EMPTY
            || obstacleType == ObstacleType.COLLECTABLE
            || obstacleType == ObstacleType.WHITE_ROCK)
        {
            booleans[(int)ObstacleFeature.CONTAINS_TILE] = false;
        }
        else
        {
            booleans[(int)ObstacleFeature.CONTAINS_TILE] = true;
        }

        if (obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.BUBBLE
            || obstacleType == ObstacleType.ICE
            || obstacleType == ObstacleType.CHAIN
            || obstacleType == ObstacleType.SLIME
            || obstacleType == ObstacleType.WHITE_ROCK
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.EMPTY
            || obstacleType == ObstacleType.GRASS)
        {
            booleans[(int)ObstacleFeature.MOVABLE] = false;
        }
        else
        {
            booleans[(int)ObstacleFeature.MOVABLE] = true;
        }

        if (obstacleType == ObstacleType.ROCK
            || obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.WHITE_ROCK)
        {
            booleans[(int)ObstacleFeature.WITH_STEPS] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.WITH_STEPS] = false;
        }

        if (obstacleType == ObstacleType.BEAR
            || obstacleType == ObstacleType.SLIME)
        {
            booleans[(int)ObstacleFeature.ANIMATED] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.ANIMATED] = false;
        }

        if (obstacleType == ObstacleType.BUBBLE)
        {
            booleans[(int)ObstacleFeature.CAN_BE_USED] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.CAN_BE_USED] = false;
        }

        if (obstacleType == ObstacleType.FLOWER_BOX)
        {
            booleans[(int)ObstacleFeature.LARGE] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.LARGE] = false;
        }

        if (obstacleType == ObstacleType.FLOWER_BOX
            || obstacleType == ObstacleType.MUSHROOM)
        {
            booleans[(int)ObstacleFeature.COLOR_SENSITIVE] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.COLOR_SENSITIVE] = false;
        }

        if (obstacleType == ObstacleType.FLOWER_BOX)
        {
            booleans[(int)ObstacleFeature.ONE_DECREMENT_WHEN_PASSED] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.ONE_DECREMENT_WHEN_PASSED] = false;
        }

        if (obstacleType == ObstacleType.NET || obstacleType == ObstacleType.CHAIN)
        {
            booleans[(int)ObstacleFeature.DESTROYED_WITH_ONLY_COMBO] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.DESTROYED_WITH_ONLY_COMBO] = false;
        }

        if (obstacleType == ObstacleType.EMPTY
            || obstacleType == ObstacleType.COLLECTABLE)
        {
            booleans[(int)ObstacleFeature.CANNOT_BE_DESTROYED] = true;
        }
        else
        {
            booleans[(int)ObstacleFeature.CANNOT_BE_DESTROYED] = false;
        }

        if(obstacleType == ObstacleType.SLIME || obstacleType == ObstacleType.BEAR)
        {
            hasSpine = true;
        }
        else
        {
            hasSpine = false;
        }
    }
}
