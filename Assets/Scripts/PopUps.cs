﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using GameAnalyticsSDK;

public class PopUps : MonoBehaviour
{
    public static PopUps instance;
    public Values values;

    public Image panel;

    public GameObject win_panel;
    public GameObject lost_panel;
    public GameObject continue_panel;
    public GameObject life_panel;
    public GameObject enter_panel;

    public GameObject goal_prefab;
    public Transform win_goal_parent;

    public float duration = 0.5f;

    public int coins;
    public TextMeshProUGUI level_text;
    public TextMeshProUGUI win_level_text;
    public TextMeshProUGUI win_score_text;
    public TextMeshProUGUI lost_level_text;
    public TextMeshProUGUI coin_boost_text;
    public TextMeshProUGUI coin_combo_text;
    public TextMeshProUGUI coin_life_text;
    public TextMeshProUGUI coin_continue_text;

    public List<Sprite> combo_sprites;
    public List<Sprite> boost_sprites;

    public Transform settings_transform;
    public GameObject settings_close_object;
    public List<Tween> pause_icon_tweens;
    public List<Transform> pause_transforms;

    int continue_value = 0;
    public TextMeshProUGUI continue_value_text;
    public GameObject first_offer_go;
    public GameObject second_offer_go;
    public bool is_first_try;
    int level = 1;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        pause_icon_tweens = new List<Tween>() { null, null, null };
    }

    private void Start()
    {
        level = GridManager.instance.level;
        //SetBg();
        level_text.text = "Level " + level.ToString();
        win_level_text.text = "Level " + level.ToString();
        lost_level_text.text = "Level " + level.ToString();
        continue_value = 0;
        coins_to_k();
        SoundMusicManager.instance.set_click_sound();
    }

    public Image current_bg;
    void SetBg()
    {
        int current_level_no = 1;
        int level_of_a_map;
        for (int i = 0; i < values.total_level_of_a_map.Count; i++)
        {
            level_of_a_map = values.total_level_of_a_map[i];

            if (current_level_no <= level && level <= level_of_a_map)
            {
                current_bg.sprite = values.map_bgs[i];
            }

            current_level_no += level_of_a_map;
        }
    }

    Tween panel_tween;
    public void panel_on(float delay = 0)
    {
        panel.gameObject.SetActive(true);
        panel.DOFade(0, 0f);
        if (panel_tween != null && panel_tween.IsActive())
        {
            panel_tween.Kill();
        }
        panel_tween = panel.DOFade(0.7f, 0.5f).SetDelay(delay);
        panel.raycastTarget = true;
    }

    public void panel_off()
    {
        panel.raycastTarget = false;
        if (panel_tween != null && panel_tween.IsActive())
        {
            panel_tween.Kill();
        }
        panel_tween = panel.DOFade(0, 0.5f).OnComplete(() => panel.gameObject.SetActive(false));
    }

    bool isPauseOpened = false;

    public void open_close_pause_panel()
    {
        if(isPauseOpened)
        {
            close_pause_panel();
        }
        else
        {
            open_pause_panel();
        }
    }
    public void open_pause_panel()
    {
        SoundManager.PlaySFX("Settings_Button_On");
        panel_on();
        settings_close_object.SetActive(true);
        for(int i = 0; i < pause_transforms.Count; i++)
        {
            if (pause_icon_tweens[i] != null && pause_icon_tweens[i].IsActive())
            {
                pause_icon_tweens[i].Kill();
            }
            else
            {
                pause_transforms[i].DOMoveX(settings_transform.position.x - 1, 0);
            }
            pause_transforms[i].DOMoveX(settings_transform.position.x, 0.5f).SetDelay(i * 0.1f);
        }
        isPauseOpened = true;
        settings_close_object.SetActive(true);
        SoundMusicManager.instance.set_click_sound();
    }

    public void close_pause_panel()
    {
        SoundManager.PlaySFX("Settings_Button_Off");
        panel_off();
        for (int i = 0; i < pause_transforms.Count; i++)
        {
            if (pause_icon_tweens[i] != null && pause_icon_tweens[i].IsActive())
            {
                pause_icon_tweens[i].Kill();
            }
            pause_transforms[i].DOMoveX(settings_transform.position.x - 1, 0.5f).SetDelay(i * 0.1f);
        }
        isPauseOpened = false;
        settings_close_object.SetActive(false);
    }

    public void LoadGameScene()
    {
        SceneInfo.CrossSceneInformation = "Game";
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
    }

    public void LoadMenuScene()
    {
        SceneInfo.CrossSceneInformation = "Menu";
        UnityEngine.SceneManagement.SceneManager.LoadScene("LoadingScene");
    }

    public void coins_to_k()
    {
        coins = PlayerPrefs.GetInt("coins", 500);

        coin_boost_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        coin_combo_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        coin_life_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));

        coin_continue_text.text = coins.ToString("N0",
        System.Globalization.CultureInfo.GetCultureInfo("en-US"));
    }

    public List<Boost> boosts;
    public GameObject boost_buy_panel;
    public Image boost_buy_icon;
    int current_boost_index = 0;

    public void CheckNewValues()
    {
        for(int i = 0; i < boosts.Count; i++)
        {
            boosts[i].CheckValue();
        }
    }

    public void open_boost_panel(int index)
    {
        panel_on();
        coins_to_k();
        current_boost_index = index;
        boost_buy_icon.sprite = boost_sprites[index];
        boost_buy_panel.SetActive(true);
        SoundMusicManager.instance.set_click_sound();
    }

    public void buy_boost()
    {
        coins_to_k();
        if (coins >= 200)
        {
            PlayerPrefs.SetInt("boost_count_" + current_boost_index.ToString(), PlayerPrefs.GetInt("boost_count_" + current_boost_index.ToString(), 3) + 1);
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) - 200);
            coins_to_k();
        }
        else
        {
            Store.instance.open_shop();
        }
        CheckNewValues();
        close_boost_panel();
    }

    public void close_boost_panel()
    {
        panel_off();
        boost_buy_panel.SetActive(false);
    }

    public GameObject combo_buy_panel;
    public Image combo_buy_icon;
    int current_combo_index = 0;

    public List<ComboStarter> comboStarters;
    public void open_combo_panel(int index)
    {
        panel_on();
        coins_to_k();
        current_combo_index = index;
        combo_buy_icon.sprite = combo_sprites[current_combo_index];
        combo_buy_panel.SetActive(true);
        SoundMusicManager.instance.set_click_sound();
    }

    public void buy_combo()
    {
        coins_to_k();
        if (coins >= 200)
        {
            PlayerPrefs.SetInt("combo_start_count_" + current_combo_index.ToString(), PlayerPrefs.GetInt("combo_start_count_" + current_combo_index.ToString(), 3) + 1);
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) - 200);
            coins_to_k();
            combostarter_initialize(current_combo_index);
        }
        else
        {
            Store.instance.open_shop();
        }
        
        close_combo_panel();
    }

    public void close_combo_panel()
    {
        panel_off();
        combo_buy_panel.SetActive(false);
    }

    public void combostarter_initialize(int current_combo_index = -1)
    {
        for (int i = 0; i < comboStarters.Count; i++)
        {
            comboStarters[i].initialize(true);
        }
        if (current_combo_index != -1)
        {
            comboStarters[current_combo_index].start_with_combos();
        }
    }

    void open_panel(GameObject go, StarFunc starFunc = null)
    {
        panel_on();
        go.transform.localScale = Vector3.zero;
        go.SetActive(true);
        go.transform.DOScale(Vector3.one, duration).SetEase(Ease.OutBack).OnComplete(() => { if (starFunc != null) starFunc(); });
        SoundMusicManager.instance.set_click_sound();
    }

    void close_panel(GameObject go)
    {
        panel_off();
        go.transform.DOScale(Vector3.zero, duration).SetEase(Ease.InBack).OnComplete(() => go.SetActive(false));
    }

    void set_goals(Transform goal_parent)
    {
        List<Goal> goal_list = GameManager.instance.goal_list;
        for (int i = 0; i < goal_list.Count; i++)
        {
            GameObject go = Instantiate(goal_prefab, goal_parent);
            Goal goal = go.GetComponent<Goal>();
            goal.GenerateGoal(goal_list[i].GetObstacleType(), goal_list[i].GetValue(), goal_list[i].GetTileColor());
            if(goal.GetValue() <= 0)
            {
                goal.SetTick();
            }
        }
    }

    public List<GameObject> stars_go;
    public List<GameObject> star_particles_go;

    public delegate void StarFunc();
    public void SetStars()
    {
        StartCoroutine(SetStars_routine());
    }

    IEnumerator SetStars_routine()
    {
        yield return new WaitForSeconds(0.3f);
        int star_count = GameManager.instance.GetStarCount();
        PlayerPrefs.SetInt("total_star", PlayerPrefs.GetInt("total_star", 0) + star_count);
        for (int i = 0; i < star_count; i++)
        {
            Vector3 init_scale = stars_go[i].transform.localScale;
            stars_go[i].transform.localScale = Vector3.one * 5;
            stars_go[i].gameObject.SetActive(true);

            stars_go[i].transform.DOBlendableMoveBy(Vector3.zero, 0.15f).OnComplete(() => SoundManager.PlaySFX("Star"));

            stars_go[i].transform.DOScale(init_scale, 0.3f).OnComplete(() =>
            {
                star_particles_go[i].gameObject.SetActive(true);
            });
            yield return new WaitForSeconds(0.3f);
        }
    }

    public void open_win_panel()
    {
        PlayerPrefs.SetInt("next_level_popup", 1);

        win_score_text.text = GameManager.instance.score.ToString();
        EnergyLoader.instance.add_energy();
        open_panel(win_panel, SetStars);
        set_goals(win_goal_parent);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, level.ToString());
    }

    public void open_lost_panel()
    {
        close_continue_panel();
        open_panel(lost_panel);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, level.ToString());
    }

    public void close_lost_panel()
    {
        close_panel(lost_panel);
    }

    public void open_life_panel()
    {
        open_panel(life_panel);
    }

    public void close_life_panel()
    {
        close_panel(life_panel);
    }

    public void open_enter_panel()
    {
        open_panel(enter_panel);
        combostarter_initialize();

    }

    public void close_enter_panel()
    {
        close_panel(enter_panel);
    }

    public void open_continue_panel()
    {
        StartCoroutine(open_continue_panel_routine());
    }

    IEnumerator open_continue_panel_routine()
    {
        yield return new WaitForSeconds(1);
        continue_value += 100;
        continue_value_text.text = continue_value.ToString() + "<sprite=0>";
        is_first_try = continue_value == 100;
        if(is_first_try)
        {
            first_offer_go.SetActive(true);
            second_offer_go.SetActive(false);
        }
        else
        {
            first_offer_go.SetActive(false);
            second_offer_go.SetActive(true);
        }
        open_panel(continue_panel);
    }

    public void TryToContinue()
    {
        if(continue_value <= PlayerPrefs.GetInt("coins", 500))
        {
            if(is_first_try)
            {
                GameManager.instance.FirstContinue();
            }
            else
            {
                GameManager.instance.SecondContinue();
            }
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) - continue_value);
            PopUps.instance.coins_to_k();
        }
        else
        {
            Store.instance.open_shop();
        }
    }

    public void close_continue_panel()
    {
        close_panel(continue_panel);
    }

    public void try_again()
    {
        if(EnergyLoader.instance.energy_count > 0)
        {
            LoadGameScene();
        }
        else
        {
            open_life_panel();
        }
    }

    public GameObject coinspopup_prefab;
    public Transform coinspopup_parent;

    public List<CoinPopUps> coin_pop_up_list;

    public void get_coins_animation()
    {
        get_coins_animation(null);
    }

    public void get_coins_animation(Transform transform)
    {
        int count = 0;
        CoinPopUps coin_pop_up = null;
        while (count < coin_pop_up_list.Count)
        {
            if (!coin_pop_up_list[count].isRunning)
            {
                coin_pop_up = coin_pop_up_list[count];
                break;
            }
            count++;
        }

        if (coin_pop_up == null)
        {
            coin_pop_up = GameObject.Instantiate(coinspopup_prefab, coinspopup_parent).GetComponent<CoinPopUps>();
        }
        coin_pop_up.Create(new Vector3(0, 0, 0), null, 0.6f, 15);
    }
}
