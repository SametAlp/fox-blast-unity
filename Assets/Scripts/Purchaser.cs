﻿
#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
//#define RECEIPT_VALIDATION
#endif

//#define DELAY_CONFIRMATION // Returns PurchaseProcessingResult.Pending from ProcessPurchase, then calls ConfirmPendingPurchase after a delay
//#define USE_PAYOUTS // Enables use of PayoutDefinitions to specify what the player should receive when a product is purchased
//#define INTERCEPT_PROMOTIONAL_PURCHASES // Enables intercepting promotional purchases that come directly from the Apple App Store
//#define SUBSCRIPTION_MANAGER //Enables subscription product manager for AppleStore and GooglePlay store
//#define AGGRESSIVE_INTERRUPT_RECOVERY_GOOGLEPLAY // Enables also using AIDL getPurchaseHistory to recover from purchase interruptions, assuming developer is deduplicating to protect against "duplicate on cancel" flow

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.Purchasing.Security;
using TMPro;

#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
	{
	private IStoreController m_Controller;

	private IAppleExtensions m_AppleExtensions;
	private ISamsungAppsExtensions m_SamsungExtensions;
	private IMicrosoftExtensions m_MicrosoftExtensions;
	private ITransactionHistoryExtensions m_TransactionHistoryExtensions;

	private IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;
	// Product identifiers for all products capable of being purchased: 
	// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
	// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
	// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	// General product identifiers for the consumable, non-consumable, and subscription products.
	// Use these handles in the code to reference which product to purchase. Also use these values 
	// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
	// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
	// specific mapping to Unity Purchasing's AddProduct, below.

	public List<string> storeItem_ids;
	public List<string> storeItem_ios_ids;

	public static Purchaser instance;

	public IStoreController m_StoreController;          // The Unity Purchasing system.
	public IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
	public CrossPlatformValidator validator;
	public ConfigurationBuilder builder;

	private void Awake()
    {
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else if (instance != this)
		{
			Destroy(this.gameObject);
		}
	}

    void Start()
	{
		try
		{
			UnityPurchasing.ClearTransactionLog();
		}
		catch { }
		// If we haven't set up the Unity Purchasing reference
		if (!IsInitialized())
			{
				// Begin to configure our connection to Purchasing
				InitializePurchasing();
			}
			else
			{
				StartCoroutine(initialize_values());
			}
	}

	public bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void InitializePurchasing()
	{
		try
		{
			// If we have already connected to Purchasing ...
			if (IsInitialized())
			{
				// ... we are done here.
				return;
			}
			// Create a builder, first passing in a suite of Unity provided stores.

			builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());


#if AGGRESSIVE_INTERRUPT_RECOVERY_GOOGLEPLAY
        // For GooglePlay, if we have access to a backend server to deduplicate purchases, query purchase history
        // when attempting to recover from a network-interruption encountered during purchasing. Strongly recommend
        // deduplicating transactions across app reinstallations because this relies upon the on-device, deletable
        // TransactionLog database.
        builder.Configure<IGooglePlayConfiguration>().aggressivelyRecoverLostPurchases = true;
#endif

#if UNITY_ANDROID
			for (int i = 0; i < storeItem_ids.Count; i++)
			{
				builder.AddProduct(storeItem_ids[i], ProductType.Consumable, new IDs {
				{ storeItem_ids[i], AppleAppStore.Name },
			});
			}
#elif UNITY_IOS
			for (int i = 0; i < storeItem_ids.Count; i++)
			{
				builder.AddProduct(storeItem_ios_ids[i], ProductType.Consumable, new IDs {
				{ storeItem_ios_ids[i], AppleAppStore.Name },
			});
			}
#endif
			// Continue adding the non-consumable product.

			// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
			// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.

			string appIdentifier;

			appIdentifier = Application.identifier;


			validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), appIdentifier);


			UnityPurchasing.Initialize(this, builder);
			StartCoroutine(initialize_values());

		}
		catch
		{

		}
	}

	void BuyProductID(string productId)
	{
		// If Purchasing has been initialized ...
		if (IsInitialized())
		{
			StartCoroutine(wait_for_iap());
			// ... look up the Product reference with the general product identifier and the Purchasing 
			// system's products collection.
			Product product = m_StoreController.products.WithID(productId);

			// If the look up found a product for this device's store and that product is ready to be sold ... 
			if (product != null && product.availableToPurchase)
			{
				//Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				m_StoreController.InitiatePurchase(product);
			}
			// Otherwise ...
			else
			{
				Store.instance.waiter_panel.SetActive(false);
				// ... report the product look-up failure situation  
				//Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		// Otherwise ...
		else
		{
			Store.instance.waiter_panel.SetActive(false);
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			//Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}


	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");
		// Overall Purchasing system, configured with products for this application.
		if (IsInitialized())
		{
			StartCoroutine(initialize_values());
			return;
		}
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
		StartCoroutine(initialize_values());
	}

	public void initialize_values_again()
    {
		StartCoroutine(initialize_values());
	}

    IEnumerator initialize_values()
    {
		yield return new WaitForEndOfFrame();
		try
		{
#if UNITY_ANDROID
			for (int i = 0; i < Store.instance.storeItems.Count; i++)
			{
				int index = i;
				Store.instance.storeItems[index].buy_button.onClick.RemoveAllListeners();
				Store.instance.storeItems[index].buy_button.onClick.AddListener(() => BuyProductID(storeItem_ids[index]));
				Store.instance.storeItems[index].cost_text.text = m_StoreController.products.WithID(storeItem_ids[i]).metadata.localizedPriceString;
			}
#elif UNITY_IOS
			for (int i = 0; i < Store.instance.storeItems.Count; i++)
			{
				int index = i;
				Store.instance.storeItems[index].buy_button.onClick.RemoveAllListeners();
				Store.instance.storeItems[index].buy_button.onClick.AddListener(() => BuyProductID(storeItem_ios_ids[index]));
				Store.instance.storeItems[index].cost_text.text = m_StoreController.products.WithID(storeItem_ios_ids[i]).metadata.localizedPriceString;
			}
#endif
			Debug.Log("Initialize Done!");
		}
		catch
        {

        }
	}

		public void OnInitializeFailed(InitializationFailureReason error)
		{
			Store.instance.waiter_panel.SetActive(false);

		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		//Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
		{
		//Debug.Log("Purchase OK: " + args.purchasedProduct.definition.id);
		//Debug.Log("Receipt: " + args.purchasedProduct.receipt);

			if (Directory.Exists(Path.Combine (Path.Combine (Application.persistentDataPath, "Unity"), "UnityPurchasing")))
				Directory.Delete (Path.Combine (Path.Combine (Application.persistentDataPath, "Unity"), "UnityPurchasing"), true);

		// A consumable product has been purchased by this user.

		// Or ... a non-consumable product has been purchased by this user.

		try
		{
			var result = validator.Validate(args.purchasedProduct.receipt);
			//Debug.Log("Receipt is valid. Contents:");
			foreach (IPurchaseReceipt productReceipt in result)
			{
				//Debug.Log(productReceipt.productID);
				//Debug.Log(productReceipt.purchaseDate);
				//Debug.Log(productReceipt.transactionID);

				GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
				if (null != google)
				{
					//Debug.Log(google.purchaseState);
					//Debug.Log(google.purchaseToken);
					for (int i = 0; i < storeItem_ids.Count; i++)
					{
						if (productReceipt.productID == storeItem_ids[i])
                        {
							GameAnalyticsSDK.GameAnalytics.NewDesignEvent("Purchase:Package_" + i.ToString() + ":AtLevel_" + PlayerPrefs.GetInt("level", 1));
							Store.instance.storeItems[i].set_values();
							Store.instance.waiter_panel.SetActive(false);
						}
					}
				}

				AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
				if (null != apple)
				{
					for (int i = 0; i < storeItem_ios_ids.Count; i++)
					{
						if (productReceipt.productID == storeItem_ios_ids[i])
						{
							GameAnalyticsSDK.GameAnalytics.NewDesignEvent("Purchase:Package_" + i.ToString() + ":AtLevel_" + PlayerPrefs.GetInt("level", 1));
							Store.instance.storeItems[i].set_values();
							Store.instance.waiter_panel.SetActive(false);
						}
					}
				}

				// For improved security, consider comparing the signed
				// IPurchaseReceipt.productId, IPurchaseReceipt.transactionID, and other data
				// embedded in the signed receipt objects to the data which the game is using
				// to make this purchase.
			}
		}
		catch (IAPSecurityException ex)
		{
			Debug.Log("Invalid receipt, not unlocking content. " + ex);
			Store.instance.waiter_panel.SetActive(false);
			return PurchaseProcessingResult.Complete;
		}

		/*
		if (String.Equals(args.purchasedProduct.definition.id, gems_1, StringComparison.Ordinal))
			{

				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            Store.instance.buy_diamond(0);
            }
            // Or ... a subscription product has been purchased by this user.
            else if (String.Equals(args.purchasedProduct.definition.id, gems_2, StringComparison.Ordinal))
			{

				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // TODO: The subscription item has been successfully purchased, grant this to the player.
            Store.instance.buy_diamond(1);
            }
            else if (String.Equals(args.purchasedProduct.definition.id, gems_3, StringComparison.Ordinal))
			{

				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				// TODO: The subscription item has been successfully purchased, grant this to the player.
            Store.instance.buy_diamond(2);
			}
			// Or ... an unknown product has been purchased by this user. Fill in additional products here....
			else 
			{
				//Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			}
		*/
		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
		}


	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Debug.Log("Purchase failed: " + item.definition.id);
		Debug.Log(r);
		Store.instance.waiter_panel.SetActive(false);
	}

	IEnumerator wait_for_iap()
    {
		Store.instance.waiter_panel.SetActive(true);
		yield return null;
	}
}