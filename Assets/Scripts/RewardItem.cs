using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class RewardItem : MonoBehaviour
{
    public string type;
    public int value;
    public Image reward_img;
    int _default_value;

    public Transform goto_transform;
    public TextMeshProUGUI item_name_text;
    public TextMeshProUGUI reward_text;


    public void set_reward(string reward_type, string _item_name_text, int value, Sprite reward_sprite, int default_value)
    {
        type = reward_type;
        this.value = value;
        reward_img.sprite = reward_sprite;
        reward_text.text = value.ToString();
        _default_value = default_value;
        item_name_text.text = _item_name_text;
    }

    public void Go(Transform start_transform, float delay)
    {
        transform.transform.position = start_transform.position;
        transform.DOBlendableMoveBy(Vector3.zero, delay).OnComplete( () => SoundManager.PlaySFX("RewardUnlock"));
        transform.DOMove(goto_transform.position, 1).SetDelay(delay);
        get_reward();
    }

    public void get_reward()
    {
        PlayerPrefs.SetInt(type, PlayerPrefs.GetInt(type, _default_value) + value);
        Menu.instance.coins_to_k();
    }
}
