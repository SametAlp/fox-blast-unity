using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum Direction
{
    UP,
    LEFT,
    DOWN,
    RIGHT
};

public class Rocket : MonoBehaviour
{
    Tween tween;
    bool isJobDone = false;

    private void Update()
    {
        if(isJobDone)
        {
            return;
        }
        Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
        if (viewPos.x >= -0f && viewPos.x <= 1f && viewPos.y >= -0f && viewPos.y <= 1f && viewPos.z > -0f)
        {
            Tile temp_tile = GameManager.instance.TileCloseToPoint(transform.position, false);
            if (temp_tile == null || !temp_tile.gameObject.activeSelf)
            {
                return;
            }
            if(temp_tile.rocket_passed_list.Contains(this))
            {
                return;
            }

            if (temp_tile.GetTilesOfLargeObstacle() != null)
            {
                List<Tile> tiles = temp_tile.GetTilesOfLargeObstacle();
                for (int i = 0; i < tiles.Count; i++)
                {
                    tiles[i].rocket_passed_list.Add(this);
                }
            }
            else
            {
                temp_tile.rocket_passed_list.Add(this);
            }

            if (temp_tile.tile_type == Tile.TileType.COMBO)
            {
                GameManager.instance.HandleCombos(temp_tile);
                GameManager.instance.DisableTile(temp_tile, rocket: this.gameObject);
            }
            else
            {
                GameManager.instance.DisableTile(temp_tile, rocket: this.gameObject);
            }
        }
        else
        {
            isJobDone = true;
            GameManager.instance.active_rockets--;
            
            transform.DOBlendableLocalMoveBy(Vector3.zero, 1f).OnComplete(() =>
            {
                GameManager.instance.rocket_pool.Add(this);
                this.gameObject.SetActive(false);
            });
        }
    }

    public void Move(Vector3 position, Direction direction)
    {
        isJobDone = false;
        GameManager.instance.active_rockets++;
        transform.position = position;
        transform.eulerAngles = Vector3.zero;
        transform.eulerAngles = new Vector3(0, 0 , 90 * (int)direction);
        Vector3 move_direction;

        float amount = 10;
        if(direction == Direction.UP)
        {
            move_direction = new Vector3(0, amount, 0);
        }
        else if(direction == Direction.DOWN)
        {
            move_direction = new Vector3(0, -amount, 0);
        }
        else if (direction == Direction.LEFT)
        {
            move_direction = new Vector3(-amount, 0, 0);
        }
        else
        {
            move_direction = new Vector3(amount, 0, 0);
        }

        SoundManager.PlaySFX("rocket_sound");
        transform.DOBlendableMoveBy(move_direction, 2f);
        /*.OnComplete(()=>
        {
            GameManager.instance.active_rockets--;
            GameManager.instance.rocket_pool.Add(this);
            this.gameObject.SetActive(false);
        });
        */
    }
}
