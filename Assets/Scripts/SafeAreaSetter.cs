using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeAreaSetter : MonoBehaviour {

    public RectTransform[] CanvasObjects;
    public Camera[] SceneCameras;

    // Start is called before the first frame update
    void Awake() {
        SetNewSize();
#if UNITY_IOS
        //Only call for iOS since Unity handles Android safe areas. Thanks Apple!
        SetNewSize();
#endif
    }

    void SetNewSize() {
        //Get safe areas
        Rect safeArea = Screen.safeArea;

        Vector2 anchorMax = safeArea.position + safeArea.size;

        float newValue = anchorMax.y / Screen.height;

        //If value is 1, we don't have to modify anything.
        if ((int)newValue == 1) return;

        //Add 0.01 for fine tuning.
        newValue += 0.015f;

        Debug.Log("NEW VALUE IS: " + newValue);

        //Only modify top of the screen. Otherwise UI messes up.
        Vector2 canvasValue = new Vector2(1, newValue);
        for (int i = 0; i < CanvasObjects.Length; i++)
            CanvasObjects[i].anchorMax = canvasValue;

        //Same with cameras, only modify top.
        Rect cameraRect = new Rect(0, 0, 1, newValue);
        for (int i = 0; i < SceneCameras.Length; i++) 
            SceneCameras[i].rect = cameraRect;

    }

}
