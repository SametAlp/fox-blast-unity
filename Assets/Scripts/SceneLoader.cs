﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using GameAnalyticsSDK;

public class SceneLoader : MonoBehaviour
{
    //public Text loading_text;
    public Image bar_front;

    AsyncOperation sceneAO;
    // the actual percentage while scene is fully loaded
    private const float LOAD_READY_PERCENTAGE = 0.9f;

    public List<Sprite> tutorial_go;
    public Image tutorial_img;
    public GameObject bg_go;

    private void Start()
    {
        GameAnalytics.Initialize();

        Application.targetFrameRate = 60;

        UpdateSceneLoadingSlider(0);
        if (!SceneInfo.first_one)
        {
            bg_go.SetActive(true);
            int index = UnityEngine.Random.Range(0, tutorial_go.Count);
            tutorial_img.sprite = tutorial_go[index];
        }
        SceneInfo.first_one = false;
        StartCoroutine(LoadingSceneRealProgress(SceneInfo.CrossSceneInformation));
    }


    IEnumerator LoadingSceneRealProgress(string sceneName)
    {
        yield return new WaitForSeconds(1);
        if (PlayerPrefs.GetInt("tutorial", 1) == 1)
        {
            sceneAO = SceneManager.LoadSceneAsync("Game");
        }
        else
        {
            sceneAO = SceneManager.LoadSceneAsync(sceneName);
        }

        // disable scene activation while loading to prevent auto load
        sceneAO.allowSceneActivation = false;

        while (!sceneAO.isDone)
        {
            UpdateSceneLoadingSlider(sceneAO.progress);

            if (sceneAO.progress >= LOAD_READY_PERCENTAGE)
            {
                UpdateSceneLoadingSlider(100);
                yield return new WaitForSeconds(1);
                UpdateSceneLoadingSlider(100);
                yield return new WaitForSeconds(1);
                sceneAO.allowSceneActivation = true;
            }
            yield return null;
        }
    }

    private void UpdateSceneLoadingSlider(float progress)
    {
        //loading_text.text = string.Format("{0}{1}", ((int)(progress * 100)), "%");
        bar_front.fillAmount = progress;
    }
}