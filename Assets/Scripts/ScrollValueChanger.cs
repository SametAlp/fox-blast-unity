﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ScrollValueChanger : MonoBehaviour, IEndDragHandler
{
    public static ScrollValueChanger instance;
    public Scrollbar scrollbar;

    public List<Button> sub_buttons;
    public List<GameObject> sub_button_gos;
    public List<GameObject> sub_icon_gos;
    public List<GameObject> sub_buttons_text_go;
    public float old_scrollbar_value;
    public int which_screen = 2;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        which_screen = 1;
        scrollbar.value = 2f / 3f;
        old_scrollbar_value = scrollbar.value;
        set_menu_sub(1);
    }

    public void close_maps()
    {
        sub_buttons[1].interactable = true;
        sub_buttons[1].transform.DOLocalMoveY(-24, 0.2f);
    }

    public void set_menu_sub(int j)
    {
        if (j > 3)
        {
            j = 3;
        }
        if(j < 0)
        {
            j = 0;
        }
        if(which_screen < j)
        {
            //Sounds.instance.leftwhoop_sound_play();
        }
        else
        {
            //Sounds.instance.rightwhoop_sound_play();
        }
        which_screen = j;
        float screen_offset = 0;
        float interval_value = 1f / 2f;


        DOTween.To(() => scrollbar.value, x => scrollbar.value = x, interval_value * j + screen_offset, 0.3f);
        
        old_scrollbar_value = interval_value * j + screen_offset;

        for (int i = 0; i < sub_buttons.Count; i++)
        {
            sub_buttons[i].interactable = true;
            sub_button_gos[i].SetActive(true);
            sub_icon_gos[i].transform.localScale = Vector3.one * 0.9f;
            sub_buttons_text_go[i].SetActive(false);
        }
        if (j == 0)
        {
        }
        else if ( j == 1)
        {

        }
        else if (j == 2)
        {
        }


        sub_buttons[j].interactable = false;
        sub_button_gos[j].SetActive(false);
        sub_icon_gos[j].transform.localScale = Vector3.one * 1.2f;
        sub_buttons_text_go[j].SetActive(true);

    }

    public void change_value()
    {
        float differ = Mathf.Abs(old_scrollbar_value - scrollbar.value);
        if (differ >= 0.02f)
        {
            differ = old_scrollbar_value - scrollbar.value;
            if (differ < 0)
            {
                which_screen++;
            }
            else
            {
                which_screen--;
            }
            set_menu_sub((int)which_screen);
        }
        else
        {
            set_menu_sub((int)which_screen);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        change_value();
    }
}
