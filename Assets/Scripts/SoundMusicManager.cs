using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundMusicManager : MonoBehaviour
{
    public static SoundMusicManager instance;
    public bool soundOn = true;
    public bool musicOn = true;

    public Image sound_img;
    public Image music_img;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        InitSoundMusic();
        set_click_sound();
    }

    public void set_click_sound()
    {
        GameObject[] click_buttons = GameObject.FindGameObjectsWithTag("Button");

        for (int i = 0; i < click_buttons.Length; i++)
        {
            Button but = click_buttons[i].GetComponent<Button>();
            click_buttons[i].gameObject.tag = "Untagged";
            but.onClick.AddListener(() => SoundManager.PlaySFX("Button_Click"));
        }
    }

    public void InitSoundMusic()
    {
        musicOn = PlayerPrefs.GetInt("music", 1) == 1;
        SoundManager.Instance.muted = !musicOn;
        music_img.color = musicOn ? Color.white : Color.red;

        soundOn = PlayerPrefs.GetInt("sound", 1) == 1;
        SoundManager.Instance.mutedSFX = !soundOn;
        sound_img.color = soundOn ? Color.white : Color.red;
    }

    public void SetSound()
    {
        soundOn = PlayerPrefs.GetInt("sound", 1) == 1;
        soundOn = !soundOn;
        PlayerPrefs.SetInt("sound", soundOn ? 1 : 0);
        sound_img.color = soundOn ? Color.white : Color.red;
        SoundManager.Instance.mutedSFX = !soundOn;
    }

    public void SetMusic()
    {
        musicOn = PlayerPrefs.GetInt("music", 1) == 1;
        musicOn = !musicOn;
        PlayerPrefs.SetInt("music", musicOn ? 1 : 0);
        music_img.color = musicOn ? Color.white : Color.red;
        SoundManager.Instance.mutedMusic = !musicOn;
    }
}
