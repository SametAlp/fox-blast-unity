﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour
{
    public static Store instance;

    public GameObject floating_store;
    public List<GameObject> store_item_gos;
    public GameObject more_go;

    public List<StoreItem> storeItems;

    public GameObject waiter_panel;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {

        Purchaser.instance.initialize_values_again();

    }

    public void open_shop()
    {
        if (GameManager.instance != null)
        {
            floating_store.SetActive(true);
        }
        else if (Menu.instance != null)
        {
            ScrollValueChanger.instance.set_menu_sub(0);
        }

        Purchaser.instance.initialize_values_again();

    }

    public void close_shop()
    {
        if(GameManager.instance != null)
            floating_store.SetActive(false);
    }

    public void more()
    {
        for(int i = 0; i < store_item_gos.Count; i++)
        {
            store_item_gos[i].SetActive(true);
        }
        more_go.SetActive(false);
    }


    public void buy_24h_energy()
    {
        if (!energy_date_control())
            PlayerPrefs.SetString("energy_hour_date", DateTime.Now.ToString());
        PlayerPrefs.SetInt("energy_hour", 24 + PlayerPrefs.GetInt("energy_hour", 0));
        //EnergyLoader.instance.close_energy_panel();
        //EnergyLoader.instance.add_one_battery_anim();
        int value = 5;
        while (value >= 0)
        {
            //EnergyLoader.instance.add_energy(true);
            value--;
        }
    }

    public bool energy_date_control()
    {
        String savedDate = PlayerPrefs.GetString("energy_hour_date", "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString("energy_hour_date", savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        // calculate days ellapsed since launch date:
        long hours = (now - launchDate).Hours;
        int dedicated_hour = PlayerPrefs.GetInt("energy_hour", 0);

        if (hours <= dedicated_hour)
        {
            return true;
        }
        return false;
    }

}
