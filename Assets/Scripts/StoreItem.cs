﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StoreItem : MonoBehaviour
{
    public bool isPackage;

    public int combo1_item = 0;
    public int combo2_item = 0;
    public int combo3_item = 0;

    public int boost1_item = 0;
    public int boost2_item = 0;
    public int boost3_item = 0;
    public int boost4_item = 0;

    public int energy_item = 0;

    public int coin = 0;

    public TextMeshProUGUI combo1_item_text;
    public TextMeshProUGUI combo2_item_text;
    public TextMeshProUGUI combo3_item_text;

    public TextMeshProUGUI boost1_item_text;
    public TextMeshProUGUI boost2_item_text;
    public TextMeshProUGUI boost3_item_text;
    public TextMeshProUGUI boost4_item_text;

    public TextMeshProUGUI energy_item_text;

    public TextMeshProUGUI coin_text;
    public TextMeshProUGUI cost_text;

    public Button buy_button;

    public Transform gold_transform;

    private void Start()
    {
        SetTexts();
    }

    public void SetTexts()
    {
        coin_text.text = coin.ToString();
        if(isPackage)
        {
            combo1_item_text.text = "x" + combo1_item.ToString();
            combo2_item_text.text = "x" + combo2_item.ToString();
            combo3_item_text.text = "x" + combo3_item.ToString();

            boost1_item_text.text = "x" + boost1_item.ToString();
            boost2_item_text.text = "x" + boost2_item.ToString();
            boost3_item_text.text = "x" + boost3_item.ToString();
            boost4_item_text.text = "x" + boost4_item.ToString();

            energy_item_text.text = energy_item.ToString() + "h";
        }
    }

    public void set_values()
    {
        SoundManager.PlaySFX("Shop_Buy");
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins", 500) + coin);
      
        PlayerPrefs.SetInt("combo_start_count_0", PlayerPrefs.GetInt("combo_start_count_0", 3) + combo1_item);
        PlayerPrefs.SetInt("combo_start_count_1", PlayerPrefs.GetInt("combo_start_count_1", 3) + combo2_item);
        PlayerPrefs.SetInt("combo_start_count_2", PlayerPrefs.GetInt("combo_start_count_2", 3) + combo3_item);

        PlayerPrefs.SetInt("boost_count_0", PlayerPrefs.GetInt("boost_count_0", 3) + boost1_item);
        PlayerPrefs.SetInt("boost_count_1", PlayerPrefs.GetInt("boost_count_1", 3) + boost2_item);
        PlayerPrefs.SetInt("boost_count_2", PlayerPrefs.GetInt("boost_count_2", 3) + boost3_item);
        PlayerPrefs.SetInt("boost_count_3", PlayerPrefs.GetInt("boost_count_3", 3) + boost4_item);

        if (energy_item >= 0)
        {
            if (!energy_date_control())
            {
                PlayerPrefs.SetString("energy_hour_date", DateTime.Now.ToString());
                PlayerPrefs.SetInt("energy_hour", 0);
            }
            PlayerPrefs.SetInt("energy_hour", energy_item + PlayerPrefs.GetInt("energy_hour", 0));
        }

        if (Menu.instance != null)
        {
            Menu.instance.coins_to_k();
            Menu.instance.combostarter_initialize();
            Menu.instance.get_coins_animation(coin_text.transform);
        }
        if (PopUps.instance != null)
        {
            PopUps.instance.coins_to_k();
            PopUps.instance.combostarter_initialize();
            PopUps.instance.CheckNewValues();
            PopUps.instance.get_coins_animation(coin_text.transform);
            Store.instance.close_shop();
            if(PopUps.instance.continue_panel.activeSelf)
                PopUps.instance.TryToContinue();
        }
    }

    public bool energy_date_control()
    {
        String savedDate = PlayerPrefs.GetString("energy_hour_date", "");
        if (savedDate == "")
        { // if not saved yet...
          // convert current date to string...
            savedDate = DateTime.Now.ToString();
            // and save it in PlayerPrefs as LaunchDate:
            PlayerPrefs.SetString("energy_hour_date", savedDate);
        }
        // at this point, the string savedDate contains the launch date
        // let's convert it to DateTime:
        DateTime launchDate;
        DateTime.TryParse(savedDate, out launchDate);
        // get current DateTime:
        DateTime now = DateTime.Now;
        // calculate days ellapsed since launch date:
        long hours = (now - launchDate).Hours;
        int dedicated_hour = PlayerPrefs.GetInt("energy_hour", 0);

        if (hours <= dedicated_hour)
        {
            return true;
        }
        return false;
    }
}
