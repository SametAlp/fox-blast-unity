using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using System.IO;

public class Tile : MonoBehaviour
{
    public enum TileColor
    {
        BLUE,
        GREEN,
        ORANGE,
        PURPLE,
        RED,
        YELLOW,
        JOKER,
        NONE
    };

    public enum TileType
    {
        TILE,
        COMBO
    };

    public enum ComboType
    {
        VERTICAL_ROCKET,
        HORIZONTAL_ROCKET,
        BOMB,
        MAGNET
    };

    public int column;
    public int row;
    public bool isActive = true;
    public bool emptyTile = false;
    public bool visited_ingame;
    public bool visited_combo;
    public bool combo_available;
    public List<Rocket> rocket_passed_list;
    public bool boostPassed;

    public TileColor tile_color;
    public TileType tile_type;
    public ComboType combo_type;

    public SpriteRenderer spriteRenderer;
    public GameObject spriteRenderer_go;
    public SpriteRenderer insideSpriteRenderer;
    public GameObject insideSprite_go;

    public Tween falling_tween;

    public bool hasComboAnim;
    public GameObject comboAnim_go;

    #region READ_LEVEL

    void read_level(int level_no)
    {
        string file_path = Path.Combine(Application.streamingAssetsPath, "Levels/" + level_no.ToString() + ".txt");
        string reader;
#if UNITY_EDITOR
        reader = System.IO.File.ReadAllText(file_path);
#elif UNITY_ANDROID
            WWW www_reader = new WWW(file_path);
            while (!www_reader.isDone);
            reader = www_reader.text;
#elif UNITY_IOS
        reader = System.IO.File.ReadAllText(file_path);
#endif
        List<string> readtext = reader.Split('\n').ToList();
        int index = 0;
        string readMeText = readtext[index++];
        int move_count = Int32.Parse(readMeText.Split(' ')[0]);
        readMeText = readtext[index++];
    }

    #endregion


    #region VISIBILITY
    public void EnableVisual()
    {
        if (!isActive || !GetObstaclesFeature(Obstacle.ObstacleFeature.CONTAINS_TILE))
            return;
        if (comboAnim_go)
        {
            comboAnim_go.SetActive(true);
        }

        spriteRenderer_go.SetActive(true);
        if (!GetObstacle() && tile_type == TileType.TILE)
        {
            insideSprite_go.SetActive(true);
        }
    }

    public void DisableVisual()
    {
        if(comboAnim_go)
        {
            comboAnim_go.SetActive(false);
        }
        spriteRenderer_go.SetActive(false);
        insideSprite_go.SetActive(false);
    }
    #endregion

    #region ITEM_ASSIGNMENTS

    public void SetTileData(TileColor tileColor)
    {
        if (!isActive || !GetObstaclesFeature(Obstacle.ObstacleFeature.CONTAINS_TILE))
            return;
        tile_type = TileType.TILE;
        tile_color = tileColor;
        spriteRenderer.sprite = GridManager.instance.item_sprites[(int)tileColor];
        insideSpriteRenderer.sprite = GridManager.instance.item_inside_sprites[(int)tileColor];
        insideSprite_go.SetActive(true);
        spriteRenderer_go.SetActive(true);
    }

    public void SetCombo(ComboType comboType, TileColor comboColor = TileColor.RED)
    {
        GameManager.instance.add_score(10);
        spriteRenderer_go.SetActive(true);
        insideSprite_go.SetActive(false);
        tile_type = TileType.COMBO;
        combo_type = comboType;

        if (combo_type == ComboType.MAGNET)
        {
            tile_color = comboColor;
            spriteRenderer.sprite = GridManager.instance.magnet_sprites[(int)comboColor];
        }
        else
        {
            spriteRenderer.sprite = GridManager.instance.combo_sprites[(int)comboType];
        }
        CreateJokerAnimation();
    }



    #endregion

    #region ANIMATIONS
    public void CollectAnimation()
    {
        for (int i = 0; i < GameManager.instance.selectedTiles.Count; i++)
        {
            Tile temp_tile = GameManager.instance.selectedTiles[i];
            if (temp_tile != this && temp_tile.tile_type == TileType.TILE)
            {
                FakeTile fakeTile = GridManager.instance.GetFakeTile(temp_tile.transform.position);
                fakeTile.SetTileData(temp_tile.spriteRenderer.sprite, temp_tile.insideSpriteRenderer.sprite);
                fakeTile.GoTo(temp_tile.transform.position, this.transform.position);
            }
        }
    }

    void CreateJokerAnimation()
    {
        FakeTile fakeTile = GridManager.instance.GetFakeTile(transform.position);
        fakeTile.SetSprite(spriteRenderer.sprite);
        fakeTile.transform.localScale = Vector3.one;
        fakeTile.transform.SetParent(this.transform);
        fakeTile.transform.localPosition = Vector3.zero;

        GridManager.instance.StartTileExplosion(this);
        GridManager.instance.active_fakeTile--;

        fakeTile.transform.DOShakeScale(0.4f, 0.5f, 10, 0).OnComplete(() =>
        {
            fakeTile.transform.DOShakeScale(1, new Vector3(0.1f, 0.1f, 0), 3, 0).SetEase(Ease.Linear).OnComplete(()=>
            {
                GridManager.instance.fakeTile_pool.Add(fakeTile);
                fakeTile.gameObject.SetActive(false);
            });
        });
    }
    #endregion

    #region COMBO_PREVIEW

    public void CheckComboAvailability()
    {
        if (tile_type == TileType.COMBO || visited_combo || !isActive || !GetObstaclesFeature(Obstacle.ObstacleFeature.CONTAINS_TILE))
            return;
        GameManager.instance.comboCheckTile = this;
        GameManager.instance.comboChecktiles = new List<Tile>();
        GameManager.instance.recursive_distribution_combo_preview(column, row);

        int number_of_tiles = GameManager.instance.comboChecktiles.Count;
        if (number_of_tiles >= GameManager.instance.values.critical3_value)
        {
            SetComboPreviews(GridManager.instance.combo_inside_sprites[2]);
        }
        else if (number_of_tiles >= GameManager.instance.values.critical2_value)
        {
            SetComboPreviews(GridManager.instance.combo_inside_sprites[1]);
        }
        else if (number_of_tiles >= GameManager.instance.values.critical1_value)
        {
            SetComboPreviews(GridManager.instance.combo_inside_sprites[0]);
        }
        else
        {
            SetItemColorIcon();
        }
    }

    public bool NeighborCombo()
    {
        int x = column;
        int y = row;
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1) || (arr_i[i] == 0 && arr_j[j] == 0))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf && temp_tile.tile_type == TileType.COMBO)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public void SetItemColorIcon()
    {
        for (int i = 0; i < GameManager.instance.comboChecktiles.Count; i++)
        {
            GameManager.instance.comboChecktiles[i].insideSpriteRenderer.sprite = GridManager.instance.item_inside_sprites[(int)GameManager.instance.comboChecktiles[i].tile_color];
        }
    }

    public void ResetItemColorIcon()
    {
        if (tile_type == TileType.TILE && GetObstacle() == null)
        {
            insideSpriteRenderer.sprite = GridManager.instance.item_inside_sprites[(int)tile_color];
        }
    }

    void SetComboPreviews(Sprite sprite)
    {
        for (int i = 0; i < GameManager.instance.comboChecktiles.Count; i++)
        {
            GameManager.instance.comboChecktiles[i].insideSpriteRenderer.sprite = sprite;
            GameManager.instance.comboChecktiles[i].combo_available = true;
        }
    }

    #endregion

    #region OBSTACLE_HANDLERS

    public Obstacle GetObstacle()
    {
        if(GridManager.instance.gridObstacles[column][row].Count > 0)
            return GridManager.instance.gridObstacles[column][row][0];

        return null;
    }

    public bool GetObstaclesFeature(Obstacle.ObstacleFeature obstacleFeature)
    {
        if (GridManager.instance.gridObstacles[column][row].Count > 0)
        {
            int obstacle_count = 0;
            int feature_count = 0;
            for (int i = 0; i < GridManager.instance.gridObstacles[column][row].Count; i++)
            {
                obstacle_count++;
                if (GridManager.instance.gridObstacles[column][row][i].GetFeatureValue(obstacleFeature))
                {
                    feature_count++;
                }
            }
            if (obstacleFeature == Obstacle.ObstacleFeature.CONTAINS_TILE)
            {
                return feature_count == obstacle_count;
            }
            if (obstacleFeature == Obstacle.ObstacleFeature.MOVABLE)
            {
                return feature_count == obstacle_count;
            }
            if (obstacleFeature == Obstacle.ObstacleFeature.CAN_BE_PASSED_OVER)
            {
                return feature_count > 0;
            }
            if (obstacleFeature == Obstacle.ObstacleFeature.CAN_BE_USED)
            {
                return feature_count == obstacle_count;
            }
        }
        if(obstacleFeature == Obstacle.ObstacleFeature.CONTAINS_TILE)
        {
            return true;
        }
        if (obstacleFeature == Obstacle.ObstacleFeature.MOVABLE)
        {
            return true;
        }
        if (obstacleFeature == Obstacle.ObstacleFeature.CAN_BE_PASSED_OVER)
        {
            return true;
        }
        if (obstacleFeature == Obstacle.ObstacleFeature.CAN_BE_USED)
        {
            return true;
        }
        return false;
    }

    public List<Obstacle> GetObstacleList()
    {
        return GridManager.instance.gridObstacles[column][row];
    }

    public void UpdateObstaclePosition(int new_row)
    {
        if (row == new_row || !GetObstacle())
        {
            return;
        }
        for (int i = 0; i < GridManager.instance.gridObstacles[column][row].Count; i++)
        {
            if (GridManager.instance.gridObstacles[column][row][i].GetFeatureValue(Obstacle.ObstacleFeature.MOVABLE))
            {
                Obstacle obstacle = GridManager.instance.gridObstacles[column][row][i];
                List<Obstacle> new_obstacles = GridManager.instance.gridObstacles[column][new_row];
                List<Obstacle> old_obstacles = GetObstacleList();
                new_obstacles.Add(obstacle);
                old_obstacles.Remove(obstacle);
                GridManager.instance.UpdateObstacleList(new_obstacles, column, row);
                GridManager.instance.UpdateObstacleList(old_obstacles, column, row);
            }
        }
    }

    public void UpdateCurrentObstacles()
    {
        List<Obstacle> obstacles = GetObstacleList();
        if(obstacles.Count == 0)
        {
            tile_type = TileType.TILE;
        }
        else
        {
            obstacles = obstacles.OrderBy((e) => e.sorting_order).Reverse().ToList();
        }
    }

    public List<Tile> GetTilesOfLargeObstacle()
    {
        if(GetObstaclesFeature(Obstacle.ObstacleFeature.LARGE))
        {
            return GetObstacle().tilesContained;
        }
        return null;
    }

    #endregion

    #region POSITION_CONTROLS

    public void SetTilePosition(int column, int row)
    {
        this.column = column;
        this.row = row;
        Vector3 tilePosition = new Vector3(GridManager.instance.first_item_pos.x + column * GridManager.instance.GRID_FAR_X,
                                            GridManager.instance.first_item_pos.y + row * GridManager.instance.GRID_FAR_Y, 0);
        transform.position = tilePosition;
    }

    public Ease easeCurve;
    public void UpdateData()
    {
        if(!isActive || !GetObstaclesFeature(Obstacle.ObstacleFeature.CONTAINS_TILE) && !GetObstaclesFeature(Obstacle.ObstacleFeature.MOVABLE) || emptyTile)
        {
            //return;
        }

        EnableVisual();
        GridManager.instance.total_count_tile++;
        if (Math.Abs(transform.position.y - (GridManager.instance.first_item_pos.y + (row * GridManager.instance.GRID_FAR_Y))) > 0.01f)
        {
            var target_position = new Vector3(transform.position.x, (GridManager.instance.first_item_pos.y + (row * GridManager.instance.GRID_FAR_Y)), 0);
            Sequence sequence = DOTween.Sequence();
            this.transform.DOMove(target_position, 0.5f).SetSpeedBased(false).SetEase(easeCurve).OnComplete(() =>
            {
                GridManager.instance.total_count_tile--;
                DecrementCollectable();
            });
        }
        else
        {
            GridManager.instance.total_count_tile--;
        }
        visited_ingame = false;
        gameObject.SetActive(true);
    }

    void DecrementCollectable()
    {
        if (GetObstacle() && GetObstacle().obstacleType == Obstacle.ObstacleType.COLLECTABLE)
        {
            int row_number = row + 1;
            bool canCollectable = true;
            while (row_number < GridManager.instance.ROWS)
            {
                Tile temp_tile = GridManager.instance.gridTiles[column][row_number];
                if (temp_tile.GetObstacle() == null || temp_tile.GetObstacle() && temp_tile.GetObstacle().obstacleType != Obstacle.ObstacleType.EMPTY)
                {
                    canCollectable = false;
                }
                row_number++;
            }
            if (canCollectable)
            {
                GameManager.instance.HandleObstacle(column, row);
            }
        }
    }

    public bool CanDecrementCollectable()
    {
        if (GetObstacle() && GetObstacle().obstacleType == Obstacle.ObstacleType.COLLECTABLE)
        {
            int row_number = row + 1;
            bool canCollectable = true;
            while (row_number < GridManager.instance.ROWS)
            {
                Tile temp_tile = GridManager.instance.gridTiles[column][row_number];
                if (temp_tile.GetObstacle() == null || temp_tile.GetObstacle() && temp_tile.GetObstacle().obstacleType != Obstacle.ObstacleType.EMPTY)
                {
                    canCollectable = false;
                }
                row_number++;
            }
            return canCollectable;
        }
        return true;
    }

    public bool CheckNewPositionAvailabilityUnder(int new_row)
    {
        int temp_row = row;
        while(temp_row <= new_row)
        {
            List<Obstacle> temp_obstacle_list = GridManager.instance.gridObstacles[column][temp_row];
            if (temp_obstacle_list.Count > 0)
            {
                for(int i = 0; i < temp_obstacle_list.Count; i++)
                {
                    if(!temp_obstacle_list[i].GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_PASSED_OVER))
                    {
                        return false;
                    }
                }
            }
            temp_row++;
        }
        return true;
    }

    public bool CheckNewPositionAvailabilityUpper()
    {
        int temp_row = row;
        while (temp_row >= 0)
        {
            List<Obstacle> temp_obstacle_list = GridManager.instance.gridObstacles[column][temp_row];
            if (temp_obstacle_list.Count > 0)
            {
                for (int i = 0; i < temp_obstacle_list.Count; i++)
                {
                    if (!temp_obstacle_list[i].GetFeatureValue(Obstacle.ObstacleFeature.CAN_BE_PASSED_OVER))
                    {
                        return false;
                    }
                }
            }
            temp_row--;
        }
        return true;
    }

    #endregion

}