using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileExplosion : MonoBehaviour
{
    private float timeLeft;
    public ParticleSystem system;
    public Renderer m_Renderer;
    private void Awake()
    {
        var main = system.main;
        timeLeft = main.startLifetimeMultiplier + main.duration;
    }
    public void ActivateExplosion(Texture texture)
    {
        m_Renderer.material.SetTexture("_MainTex", texture);
        gameObject.SetActive(false);
        gameObject.SetActive(true);
        StartCoroutine(DeactivateExplosion());
    }

    IEnumerator DeactivateExplosion()
    {
        yield return new WaitForSeconds(timeLeft);
        GridManager.instance.tileExplosion_pool.Add(this);
        gameObject.SetActive(false);
    }
}
