using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Tutorial : MonoBehaviour
{
    public static Tutorial instance;

    public List<List<Tile>> gridTiles;
    public List<List<List<Obstacle>>> gridObstacles;

    int level = 1;
    public int step = 0;
    Vector3 goal_position;
    float height;

    public int default_tile_sorting = 5;
    public int default_tile_inside_sorting = 6;
    public int default_obstacle_spine_sorting = 15;
    public int default_obstacle_sprite_sorting = 20;

    public int front_sorting = 50;

    public Image tutorial_panel;
    public List<Tile> front_tiles;

    public GameObject tutorial_popup_go;
    public TextMeshProUGUI tutorial_text;
    public GameObject arrow_go;
    public GameObject hand_go;

    public GameObject move_and_goal_go;
    public List<GameObject> boost_gos;
    public GameObject ok_go;

    Tile.TileColor tileColor;
    static List<List<int>> indexes_of_tiles = new List<List<int>>() {
                                new List<int>() { 0, 0},
                                new List<int>() { 4, 4},
                                new List<int>() { 4, 4},
                                new List<int>() { 4, 4},
                                new List<int>() { 4, 4},
                                new List<int>() { 4, 4, 5, 4} };

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        gridTiles = GridManager.instance.gridTiles;
        gridObstacles = GridManager.instance.gridObstacles;
        goal_position = GameManager.instance.goal_parent.position;
        height = Camera.main.ScreenToWorldPoint(GameManager.instance.goal_parent.GetComponent<RectTransform>().sizeDelta).y;

        TutorialCheck();
    }

    public void panel_off()
    {
        tutorial_panel.raycastTarget = false;
        tutorial_panel.DOFade(0, 0.5f).OnComplete(() => tutorial_panel.gameObject.SetActive(false));
    }

    public void panel_on()
    {
        tutorial_panel.raycastTarget = true;
        tutorial_panel.gameObject.SetActive(true);
        tutorial_panel.DOFade(0, 0);
        tutorial_panel.DOFade(0.8f, 0.5f);
    }

    public void DisableTutorialVisuals()
    {
        panel_off();
        arrow_go.SetActive(false);
        hand_go.SetActive(false);
        tutorial_popup_go.SetActive(false);
    }

    public void EndOfTutorial()
    {
        if (level == 1 && step < 1)
        {
            hand_go.SetActive(false);
            return;
        }
        if (level != 1 && level < 5 && step < 2)
        {
            return;
        }

        DisableTutorialVisuals();
        PlayerPrefs.SetInt("tutorial", 0);
        RemoveCanvases();
        StartCoroutine(finished_tutorial_routine());
    }

    IEnumerator finished_tutorial_routine()
    {
        yield return new WaitForSeconds(0.5f);
        GameManager.instance.isTutorial = false;
        this.gameObject.SetActive(false);
    }

    void TutorialCheck()
    {
        level = GridManager.instance.level;

        if(level == 0)
        {
            return;
        }
        if(level < 5)
        {
            SetHandPosition();
            panel_on();
            GameManager.instance.isTutorial = true;
            GetTilesFrontWithSameColor();
            SetTutorialPopUp();
        }
        if (level == 5)
        {
            SetHandPosition();
            panel_on();
            GameManager.instance.isTutorial = true;
            GetTilesFrontWithSameColorManual();
            SetTutorialPopUp();
        }
        else if (level == 10)
        {
            SetBoost(0);
        }
        else if (level == 13)
        {
            SetBoost(1);
        }
        else if (level == 17)
        {
            SetBoost(2);
        }
        else if (level == 20)
        {
            SetBoost(3);
        }
        else
        {
            ObstacleCheck();
        }
    }

    void SetBoost(int index)
    {
        panel_on();
        ok_go.SetActive(true);
        GameManager.instance.isTutorial = true;
        arrow_go.SetActive(true);
        arrow_go.transform.position = new Vector3(boost_gos[index].transform.position.x, boost_gos[index].transform.position.y - height / 3f, boost_gos[index].transform.position.z);
        boost_gos[index].AddComponent<Canvas>().GetComponent<Canvas>().overrideSorting = true;
        boost_gos[index].AddComponent<GraphicRaycaster>();
        boost_gos[index].GetComponent<Canvas>().sortingOrder = 200;
        SetTutorialPopUp();
    }

    void SetTutorialPopUp()
    {
        tutorial_popup_go.SetActive(true);
        tutorial_popup_go.transform.position = new Vector3(0, goal_position.y + height/3, goal_position.z);
        if(level == 1)
        {
            tutorial_text.text = "Tap 2 or more same colored tiles to remove them!";
        }
        else if (level == 2)
        {
            tutorial_text.text = "Tap 5 tiles to create a rocket!";
        }
        else if (level == 3)
        {
            tutorial_text.text = "Tap 7 tiles to create a dynamite!";
        }
        else if (level == 4)
        {
            tutorial_text.text = "Tap 9 tiles to create a magnet!";
        }
        else if (level == 5)
        {
            tutorial_text.text = "Tap connected combos to make a greater explosion!";
        }
        else if(level == 10)
        {
            tutorial_text.text = "Sling remove the tile you tap!";
        }
        else if (level == 13)
        {
            tutorial_text.text = "Lawn Mower removes everything in row you tap!";
        }
        else if (level == 17)
        {
            tutorial_text.text = "Leaf blower removes everything in column you tap!";
        }
        else if (level == 20)
        {
            tutorial_text.text = "Windmill shuffle all tiles!";
        }
    }

    void SetHandPosition()
    {
        hand_go.SetActive(true);
        int x = indexes_of_tiles[level][0];
        int y = indexes_of_tiles[level][1];
        hand_go.transform.position = GridManager.instance.gridTiles[x][y].transform.position;
    }

    void SetHandPositionSecond()
    {
        hand_go.SetActive(true);
        for(int i = 0; i < gridTiles.Count; i++)
        {
            for(int j = 0; j < gridTiles[i].Count; j++)
            {
                if(gridTiles[i][j].tile_type == Tile.TileType.COMBO)
                {
                    Tile first_tile = gridTiles[i][j];
                    hand_go.transform.position = first_tile.transform.position;
                    front_tiles = new List<Tile>();
                    front_tiles.Add(first_tile);
                    SetTileFront(first_tile);
                    return;
                }
            }
        }
    }
    public void SetTutorialPopUpSecond()
    {
        StartCoroutine(SetTutorialPopUpSecond_routine());
    }

    IEnumerator SetTutorialPopUpSecond_routine()
    {
        yield return new WaitForSeconds(0.5f);
        panel_on();
        if (level == 1)
        {
            tutorial_popup_go.SetActive(true);
            arrow_go.transform.position = new Vector3(goal_position.x, goal_position.y + height / 3f, goal_position.z);
            arrow_go.SetActive(true);
            arrow_go.transform.Rotate(Vector3.forward * 180);
            move_and_goal_go.AddComponent<Canvas>().GetComponent<Canvas>().overrideSorting = true;
            move_and_goal_go.GetComponent<Canvas>().sortingOrder = 200;
            tutorial_popup_go.transform.position = new Vector3(0, goal_position.y + height, goal_position.z);
            tutorial_text.text = "Collect the remaining tiles to complete goals!";
        }
        else if (level < 5)
        {
            SetHandPositionSecond();
            tutorial_popup_go.SetActive(true);
            tutorial_popup_go.transform.position = new Vector3(0, goal_position.y + height/3, goal_position.z);
            if (level == 2)
            {
                tutorial_text.text = "Tap the rocket now!";
            }
            else if (level == 3)
            {
                tutorial_text.text = "Tap the dynamite now!";
            }
            else if (level == 4)
            {
                tutorial_text.text = "Tap the magnet now!";
            }
        }
    }

    public void RemoveCanvases()
    {
        if(move_and_goal_go.GetComponent<Canvas>())
        {
            Destroy(move_and_goal_go.GetComponent<Canvas>());
        }

        for (int i = 0; i < boost_gos.Count; i++)
        {
            if (boost_gos[i].GetComponent<Canvas>())
            {
                Destroy(boost_gos[i].GetComponent<GraphicRaycaster>());
                Destroy(boost_gos[i].GetComponent<Canvas>());
            }
        }
    }

    void SetTileFront(Tile tile)
    {
        tile.spriteRenderer.sortingOrder = front_sorting;
        tile.insideSpriteRenderer.sortingOrder = front_sorting + 5;
    }

    void SetTileBack(Tile tile)
    {
        tile.spriteRenderer.sortingOrder = default_tile_sorting;
        tile.insideSpriteRenderer.sortingOrder = default_tile_inside_sorting;
    }

    public void GetTilesBackWithSameColor()
    {
        hand_go.SetActive(false);
        for(int i = 0; i < front_tiles.Count; i++)
        {
            SetTileBack(front_tiles[i]);
        }
        DisableTutorialVisuals();
        EndOfTutorial();
    }

    void GetTilesFrontWithSameColor()
    {
        int x = indexes_of_tiles[level][0];
        int y = indexes_of_tiles[level][1];
        Tile first_tile = gridTiles[x][y];
        tileColor = first_tile.tile_color;

        front_tiles = new List<Tile>();
        front_tiles.Add(first_tile);
        SetTileFront(first_tile);

        GetTilesFront(x, y);
        GameManager.instance.ResetVisits();
    }

    void GetTilesFrontWithSameColorManual()
    {
        front_tiles = new List<Tile>();
        Tile first_tile;

        int x = indexes_of_tiles[level][0];
        int y = indexes_of_tiles[level][1];

        first_tile = gridTiles[x][y];
        front_tiles.Add(first_tile);

        x = indexes_of_tiles[level][2];
        y = indexes_of_tiles[level][3];

        first_tile = gridTiles[x][y];
        front_tiles.Add(first_tile);

        for (int i = 0; i < front_tiles.Count; i++)
        {
            SetTileFront(front_tiles[i]);
        }
        GameManager.instance.ResetVisits();
    }

    void GetTilesFront(int x, int y)
    {
        int[] arr_i = { -1, 0, 1 };
        int[] arr_j = { -1, 0, 1 };
        for (int i = 0; i < arr_i.Length; i++)
        {
            for (int j = 0; j < arr_j.Length; j++)
            {
                if (arr_i[i] == 0 && arr_j[j] == 0)
                {
                    continue;
                }
                if ((arr_i[i] == -1 && arr_j[j] == -1) || (arr_i[i] == 1 && arr_j[j] == 1) || (arr_i[i] == -1 && arr_j[j] == 1) || (arr_i[i] == 1 && arr_j[j] == -1))
                {
                    continue;
                }
                int index_x = x + arr_i[i];
                int index_y = y + arr_j[j];
                if (index_x >= 0 && index_x < GridManager.instance.gridTiles.Count)
                {
                    if (index_y >= 0 && index_y < GridManager.instance.gridTiles[0].Count)
                    {
                        Tile temp_tile = GridManager.instance.gridTiles[index_x][index_y];
                        if (temp_tile.gameObject.activeSelf
                            && temp_tile.tile_color == tileColor)
                        {
                            if (!temp_tile.visited_ingame && temp_tile.isActive)
                            {
                                temp_tile.visited_ingame = true;
                                SetTileFront(temp_tile);
                                front_tiles.Add(temp_tile);
                                GetTilesFront(index_x, index_y);
                            }
                        }
                    }
                }
            }
        }
    }

    void ObstacleCheck()
    {
        if (level == 6)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.ROCK, "Tap tiles to destroy rocks");
        }
        else if (level == 11)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.ICE, "Tap tiles to destroy ice");
        }
        else if (level == 18)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.BUBBLE, "Tap tiles to pop bubbles");
        }
        else if (level == 28)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.COLLECTABLE, "Tap tiles to pop bubbles");
        }
        else if (level == 29)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.COLLECTABLE, "Make apples reach bottom to collect them");
        }
        else if (level == 31)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.MUSHROOM, "Tap same color tiles with mushrooms to remove");
        }
        else if (level == 36)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.GRASS, "Tap tiles to mow grass");
        }
        else if (level == 40)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.SLIME, "Tap tiles to clean slimes");
        }
        else if (level == 52)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.BEAR, "Tap tiles 3 times to collect bears");
        }
        else if (level == 60)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.FLOWER_BOX, "Tap same color tiles with flowers' to cut them");
        }
        else if (level == 78)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.NET, "Only a Rocket or a Bomb can destroy nets");
        }
        else if (level == 97)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.WHITE_ROCK, "Tap tiles to destroy white rocks");
        }
        else if (level == 101)
        {
            SetObstacleTutorial(Obstacle.ObstacleType.CHAIN, "Tap tiles to pop bubbles");
        }
    }

    void SetObstacleTutorial(Obstacle.ObstacleType obstacleType, string obstacle_text)
    {
        panel_on();
        GameManager.instance.isTutorial = true;
        tutorial_popup_go.SetActive(true);
        tutorial_text.text = obstacle_text;
        GetObstaclesFront(obstacleType);
    }

    void GetObstaclesFront(Obstacle.ObstacleType obstacleType)
    {
        for(int i = 0; i < gridObstacles.Count;i++)
        {
            for(int j = 0; j < gridObstacles[i].Count; j++)
            {
                List<Obstacle> temp_obstacle_list = gridObstacles[i][j];
                for(int k = 0; k < temp_obstacle_list.Count; k++)
                {
                    if(temp_obstacle_list[k].obstacleType == obstacleType)
                    {
                        temp_obstacle_list[k].skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder = front_sorting;
                        temp_obstacle_list[k].spriteRenderer.sortingOrder = front_sorting + 5;
                    }
                }
            }
        }
    }

    public void GetObstaclesBack(Obstacle.ObstacleType obstacleType)
    {
        for (int i = 0; i < gridObstacles.Count; i++)
        {
            for (int j = 0; j < gridObstacles[i].Count; j++)
            {
                List<Obstacle> temp_obstacle_list = gridObstacles[i][j];
                for (int k = 0; k < temp_obstacle_list.Count; k++)
                {
                    if (temp_obstacle_list[k].obstacleType == obstacleType)
                    {
                        temp_obstacle_list[k].skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder = default_obstacle_spine_sorting;
                        temp_obstacle_list[k].spriteRenderer.sortingOrder = default_obstacle_sprite_sorting;
                    }
                }
            }
        }
        DisableTutorialVisuals();
        EndOfTutorial();
    }
}
