// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("2taVk4SCn5CflZeCk9aGmZqflY/ccL5wAfv39/Pz9saUx/3G//D1o5qT1r+YldjH0MbS8PWj8v3l67eGhJeVgp+Vk9aFgpeCk5uTmIKF2MaSw9XjveOv60ViAQBqaDmmTDeupi/AiTdxoy9Rb0/EtA0uI4doiFekvy6AacXik1eBYj/b9PX39vdVdPeP1peFhYObk4XWl5WVk4aCl5iVk8BvutuOQRt6bSoFgW0EgCSBxrk3kXn+QtYBPVra1pmGQMn3xnpBtTnG5/D1o/L85fy3hoaak9a/mJXYx0gChW0YJJL5PY+5wi5UyA+OCZ0+gYHYl4aGmpPYlZmb2ZeGhpqTlZfz9vV09/n2xnT3/PR09/f2Emdf/8XArMaUx/3G//D1o/Lw5fSjpcflR8auGqzyxHqeRXnrKJOFCZGok0p9738oD72aA/Fd1Mb0Hu7IDqb/JdaZkNaCnpPWgp6TmNaXhoaan5WX0MbS8PWj8v3l67eGhpqT1rWThILpZy3osaYd8xuoj3LbHcBUobqjGti2UAGxu4n+qMbp8PWj69Xy7sbgQe1LZbTS5Nwx+etAu2qolT69duHy8OX0o6XH5cbn8PWj8vzl/LeGhsvQkdZ8xZwB+3Q5KB1V2Q+lnK2S/t3w9/Pz8fT34OiegoKGhczZ2YH78P/ccL5wAfv39/Pz9vV09/f2qnmFd5Yw7a3/2WREDrK+BpbOaOMDY2iM+lKxfa0i4MHFPTL5uzjinyc/74QDq/gjialtBNP1TKN5u6v7B3T39vD/3HC+cAGVkvP3xncExtzw+WvLBd2/3uw+CDhDT/gvqOogPct24t0mn7FigP8IAp172LZQAbG7idIUHSdBhin5sxfRPAebjhsRQ+Hhgp+Qn5WXgpPWlI/Wl5iP1oaXhII2lcWBAczx2qAdLPnX+CxMhe+5Q14qiNTDPNMjL/kgnSJU0tXnAVda2cZ3NfD+3fD38/Px9PTGd0Dsd0Xwxvnw9aPr5ff3CfLzxvX39wnG64aak9akmZmC1rW3xujh+8bAxsLE4Mbi8PWj8vXl+7eGhpqT1qSZmYLDxMfCxsXArOH7xcPGxMbPxMfCxulzdXPtb8uxwQRfbbZ42iJHZuQuQ8xbAvn49mT9R9fg2IIjyvstlOCvUfP/iuG2oOfogiVBfdXNsVUjmda1t8Z099TG+/D/3HC+cAH79/f38PWj6/jy4PLi3SafsWKA/wgCnXuziOm6naZgt38ygpT95nW3ccV8d8Z08k3GdPVVVvX09/T09/TG+/D/gp6ZhJ+Cj8fgxuLw9aPy9eX7t4bWl5iS1pWThIKfkJ+Vl4KfmZjWhv6oxnT35/D1o+vW8nT3/sZ09/LGmJLWlZmYkp+Cn5mYhdaZkNaDhZOfkJ+Vl4KfmZjWt4OCnpmEn4KPx4m3Xm4PJzyQatKd5yZVTRLt3DXphpqT1rWThIKfkJ+Vl4KfmZjWt4NdVYdksaWjN1nZt0UODRWGOxBVuvEai891faXWJc4yR0lsufydCd0KlJqT1oWCl5iSl4SS1oKThJuF1peMxnT3gMb48PWj6/n39wny8vX096STmp+XmJWT1pmY1oKen4XWlZOEplx8IywSCib/8cFGg4PX");
        private static int[] order = new int[] { 44,1,3,46,20,58,27,40,48,21,21,50,36,14,48,26,16,41,21,26,31,59,46,56,35,47,55,59,55,34,46,32,53,39,57,54,37,57,59,57,40,49,56,52,48,46,47,48,58,58,57,52,57,58,58,58,57,57,58,59,60 };
        private static int key = 246;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
