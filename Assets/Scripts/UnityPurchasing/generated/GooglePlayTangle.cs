// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("unNszi8fb0/MtEG+0Bss9OlqGZwqrIvYhiixMsUFHR+z81jwLn46n3gUBuYXKp6ps9OK8BTKiCO5GkcrBcgu5mufp8dKkgZgy6wwZf8TFIqJaIZO43arYBJ+nHo4MM7QLHZ7bsK27PVy/R5UkQ0bmijumcw6zT6c473gJa33ax3R1OiEd+1Yni+7K67VVlhXZ9VWXVXVVlZX4TNGXk8hoPr4GhIum0rkEpA7gWjt8qcrLfDEQQf87ZEVC1gMp73iDm5dAmmPkPWFWQh5Ti9xGqHTFViAvM5FB5wnGmlOhBPeFLcA8z3zw3ZUvQE2rq8DG7xWEs/0EbA+UlemY9l4Ai1JBiJn1VZ1Z1pRXn3RH9GgWlZWVlJXVObk7OjcbhDWEFVUVldW");
        private static int[] order = new int[] { 7,6,2,4,12,8,8,8,10,12,11,12,13,13,14 };
        private static int key = 87;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
