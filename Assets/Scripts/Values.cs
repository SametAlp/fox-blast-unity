﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[CreateAssetMenu(fileName = "Values", menuName ="Values", order = 1)]
public class Values : ScriptableObject
{
    [Header("Critical Values")]
    public int max_level;
    public List<int> total_level_of_a_map;
    public List<Sprite> map_bgs;
    [Header("Critical Values")]
    public int critical1_value = 5;
    public int critical2_value = 8;
    public int critical3_value = 10;
    public float critical_k_value = 1.2f;
    public float critical_2k_value = 1.3f;
    public float critical_3k_value = 1.4f;
}

[System.Serializable]
public class MapInfo
{
}