obstacles = []
for lev in range(151):
        lev = 1 if lev == 0 else lev
        f = open(str(lev) + ".txt", "r")
        content = f.read().split("\n")
        count = 1

        move_count = int(content[0])

        #print(count, len(content[count]), content[count])
        if len(content[count]) == 1:
            count += 1
            reach_count = count + 9
            while count != reach_count:
                line_reader = content[count].split(" ")
                for i in range(len(line_reader)):
                    if not line_reader[i] in obstacles:
                        obstacles.append(line_reader[i])
                        print(line_reader[i] + " at Level " + str(lev))
                count += 1

        if len(content[count]) == 2:
            count += 1
            reach_count = count + 9
            while count != reach_count:
                line_reader = content[count].split(" ")
                for i in range(len(line_reader)):
                    if not line_reader[i] in obstacles:
                        obstacles.append(line_reader[i])
                        print(line_reader[i] + " at Level " + str(lev))
                count += 1
                
        if len(content[count]) == 3:
            count += 1
            reach_count = count + 9
            while count != reach_count:
                line_reader = content[count].split(" ")
                for i in range(len(line_reader)):
                    if not line_reader[i] in obstacles:
                        obstacles.append(line_reader[i])
                        print(line_reader[i] + " at Level " + str(lev))
                count += 1
